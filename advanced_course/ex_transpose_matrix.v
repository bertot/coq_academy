Require Import List ZArith Bool Lia.
Import ListNotations.
Open Scope Z_scope.

(* It is possible to represent a matrix as a list of lists,
  where all lists have to be the same length.  We first
  need to define a function that tells us if a list of lists
  indeed represents a matrix of a given size (m rows, n columns) *)

Fixpoint rectangular {A : Type} (m n : nat) (l : list (list A)) : bool :=
  ....

(* Now we would like to define a function that takes as input
   a list of lists and returns the list of lists that represents
   the transposed matrix.
   this function can be defined recursively or it can be obtained
   through existing functions from the List library: map, combine,
   etc. The function should be named tp, you may have to use
   Fixpoint instead of Definition *)

Definition tp {A : Type} (l : list (list A)) : list (list A) :=
   ....


(* If your definition of tp is correct, you should be able to prove the
  following statement. *)

Lemma tp_involutive (l : list (list A)) :
  rectangular l = true -> tp (tp l) = l.     

(* Hint: use eq_from_nth from a previous exercise to express that
   two matrices are equal when they have the same dimension and the
   elements at the same position are equal.

   Find an intermediate specification of the tp function expressing
   the dimension of the result and the value at coordinates (i,j)
   using nth. *)
