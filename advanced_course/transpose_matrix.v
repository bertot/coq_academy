Require Import List ZArith Bool Lia.
Import ListNotations.
Open Scope Z_scope.

Fixpoint lists {A : Type} (l : list A) : list (list A) :=
  match l with
  | a :: l' => [a] :: lists l'
  | nil => nil
  end.


Fixpoint zip {A B C : Type} (f : A -> B -> C)
  (l1 : list A) (l2 : list B) : list C :=
  match l1, l2 with
  | a :: l1', b :: l2' => f a b :: zip f l1' l2'
  | _, _ => nil
  end.

Fixpoint tp {A : Type} (m : list (list A)) : list (list A) :=
  match m with
  | nil => nil
  | [line1] => lists line1
  | line1 :: m => zip (@cons A) line1 (tp m)
  end.

Compute tp (tp [[1;2;3];[4;5;6];[7;8;9]]).

Definition rectangular (i j : nat) (m : list (list Z)) :=
  length m = i /\
  forall k, (k < i)%nat -> length (nth k m nil) = j.

Lemma rectangular_cons i j row m :
  rectangular (S i) j (row :: m) -> rectangular i j m.
Proof.
unfold rectangular at 1; simpl; intros [nbrows nbcols].
split.
  lia.
intros k kint; apply (nbcols (S k)).
lia.
Qed.

Definition mxf (m : list (list Z)) (k l : nat): Z :=
  nth l (nth k m nil) 0.

Lemma nthP (row1 row2 : list Z) :
  length row1 = length row2 ->
  (forall k, (k < length row1)%nat -> nth k row1 0 = nth k row2 0) <->
  row1 = row2.
Proof.
revert row2.
induction row1 as [ | a tl IH]; intros row2.
  simpl.
  intros lrow2.
  split.
    now intros _; symmetry; rewrite <- length_zero_iff_nil; symmetry.
  lia.
destruct row2 as [ | b tl2].
  discriminate.
simpl; intros lq; injection lq; clear lq; intros lq.
split.
  intros index_cond.
  assert (cond0 : (0 < S (length tl))%nat) by lia.
  assert (a_eq_b := index_cond _ cond0); simpl in a_eq_b.
  rewrite a_eq_b; apply f_equal.
  rewrite <- IH; [ | easy].
  intros k condk.
  assert (condk': (S k < S (length tl))%nat).
    lia.
  now assert (tmp := index_cond _ condk'); simpl in tmp.
intros tlq; injection tlq; clear tlq; intros tlq ab.
now rewrite tlq, ab.
Qed.

Lemma mxfP (i j : nat) (m1 m2 : list (list Z)) :
  rectangular i j m1 -> rectangular i j m2 ->
  (forall k l, (k < i)%nat -> (l < j)%nat -> mxf m1 k l = mxf m2 k l)
  <-> m1 = m2.
Proof.
revert i m2; induction m1 as [ | row1 m1 IH]; intros i m2.
  intros rm1 rm2.   
  split;[ | intros m1m2; rewrite m1m2; easy].
  destruct rm1 as [rm1 _]; rewrite <- rm1 in rm2.
  destruct rm2 as [rm2 _].
  now rewrite length_zero_iff_nil in rm2; rewrite rm2.
intros [nblines nbcols].
destruct m2 as [ | row2 m2].
  rewrite <- nblines; intros [lines2 _].
  discriminate lines2.
intros [lines2 cols2].
rewrite <- nblines in lines2; simpl in lines2.
injection lines2 as lines2.
assert (cond0 : (0 < i)%nat).
  rewrite <- nblines; simpl; lia.
assert (lr1 : length row1 = j).
  now assert (tmp := nbcols _ cond0); simpl in tmp; rewrite tmp; symmetry.
assert (lr2 : length row2 = j).
  now assert (tmp := cols2 _ cond0); simpl in tmp; rewrite tmp; symmetry.
split.
  intros fq.  
  assert (rq : row1 = row2).
    apply nthP; [now rewrite lr1, lr2 | ].
    now rewrite lr1; intros l lltj; apply (fq _ _ cond0 lltj).
  rewrite rq; apply f_equal.
  rewrite <- (IH (length m1)).
      intros k l kltm1 lltj.
      assert (klti : (S k < i)%nat).
        rewrite <- nblines; simpl; lia.
      exact (fq _ _ klti lltj).
    split;[easy | ].
    intros k kltm1; 
    assert (klti : (S k < i)%nat) by now rewrite <- nblines; simpl; lia.
    exact (nbcols _ klti).
  split;[easy | ].
  intros k kltm1; 
  assert (klti : (S k < i)%nat) by now rewrite <- nblines; simpl; lia.
  exact (cols2 _ klti).
intros mq; injection mq as r1r2 m1m2; rewrite r1r2, m1m2; easy.
Qed.

Lemma length_lists {A} (l : list A) : length (lists l) = length l.
Proof.
now induction l as [ | a l IH]; simpl; auto.
Qed.

Lemma length_each_in_lists {A} (l : list A) (k : nat):
  (k < length l)%nat -> length (nth k (lists l) nil) = 1%nat.
Proof.
revert k.
induction l as [ | a l IH].
   simpl; lia.
intros [ | k].
  easy.
simpl; intros klt; assert (klt' : (k < length l)%nat) by lia.
now apply IH.
Qed.

Lemma length_zip {A B C} (i : nat) (f : A -> B -> C)
   (l1 : list A) (l2 : list B) :
   length l1 = i ->
   length l2 = i ->
   length (zip f l1 l2) = i.
Proof.
revert i l2; induction l1 as [ | a l1 IH]; [easy | ].
intros i l2; simpl.
destruct l2 as [ | b l2]; [ easy | ].
simpl.
destruct i as [ | i];[easy | ].
intros tmp; injection tmp as l1i; intros tmp; injection tmp as l2i.
now rewrite (IH _ _ l1i l2i).
Qed.

Lemma zip_length (i j : nat) (l1 : list Z) (l2 : list (list Z)) :
  length l1 = i ->
  length l2 = i ->
  (forall k, (k < i)%nat -> length (nth k l2 [] ) = j) ->
  forall k, (k < i)%nat -> length (nth k (zip cons l1 l2) []) = S j.
Proof.
revert i l2; induction l1 as [ | a l1 IH].
  simpl; lia.
simpl; intros i l2 vi; rewrite <- vi; destruct l2 as [ | lin l2];
  [discriminate | simpl; intros tmp; injection tmp as lenl2].
intros cols k kint.
destruct k as [ | k].
  now simpl; apply f_equal, (cols _ kint).
assert (kint': (k < length l1)%nat) by lia.
apply (fun h => IH (length l1) l2 eq_refl lenl2 h _ kint').
intros k' k'int; apply (cols (S k')); lia.
Qed.

Lemma nth_zip {A B C} (i : nat) (f : A -> B -> C) l1 l2 a b c k:
  length l1 = i -> length l2 = i -> (k < i)%nat ->
  nth k (zip f l1 l2) c = f (nth k l1 a) (nth k l2 b).
Proof.
revert i k l2; induction l1 as [ | e l1 IH]; intros i.
intros k [ | e' l2].
    now simpl; lia.
  simpl; intros i0; rewrite <- i0; discriminate.
simpl; intros k [ | e' l2] i_s;[rewrite <- i_s; discriminate | ].
rewrite <- i_s; simpl; intros tmp; injection tmp as lenl2.
intros kltsl; destruct k as [ | k]; [easy | ].
now apply (IH (length l1)); auto; lia.
Qed.

Lemma lists_nth {A}  (a : A) (l : list A) (k : nat) :
   nth 0 (nth k (lists l) nil) a =
   nth k l a.
Proof.
revert k; induction l as [ | e l IH].
  intros k; destruct k as [ | k]; easy.
now intros k; destruct k as [ | k]; [easy | simpl; apply IH].
Qed.

Lemma tpP (i j : nat) (m : list (list Z)) :
  (0 < i)%nat -> (0 < j)%nat ->
  rectangular i j m ->
  rectangular j i (tp m) /\
  (forall k l, 
    (k < j)%nat -> (l < i)%nat ->
    mxf (tp m) k l = mxf m l k).
Proof.
revert i.
induction m as [ | row1 m IH].
  intros i igt0 jgt0 [abs _]; rewrite <- abs in igt0; simpl in igt0; lia.
intros i igt0 jgt0.
intros rectij.
generalize rectij; unfold rectangular at 1; simpl.
intros [lmi cols].
split.
  destruct m as [ | row2 m].
    split.
      rewrite length_lists.
      now apply (cols _ igt0).
    intros k kltj; rewrite length_each_in_lists.
      now rewrite <- lmi.
    now rewrite (cols _ igt0).
  rewrite <- lmi in rectij; apply rectangular_cons in rectij.
  destruct (IH (S (length m)) (Nat.lt_0_succ _) jgt0 rectij)
     as [[IH1 IH2] IH3].
  split.
    now rewrite (length_zip j);[easy | apply (cols _ igt0) | easy].
  rewrite <- lmi; apply zip_length; auto.
  now apply (cols _ igt0).
intros k l kltj llti.
  destruct m as [ | row2 m].
  assert (l0 : l = 0%nat).
    simpl in lmi; lia.
  now rewrite l0; unfold mxf; simpl; apply lists_nth.
rewrite <- lmi in rectij; apply rectangular_cons in rectij.
destruct (IH (S (length m)) (Nat.lt_0_succ _) jgt0 rectij)
     as [[IH1 IH2] IH3].
unfold mxf.
destruct l as [ | l].
  rewrite (nth_zip j cons _ _ 0 nil); simpl; auto.
  now apply (cols _ igt0).
rewrite (nth_zip j cons _ _ 0 nil); auto.
  change (mxf  (tp (row2 :: m)) k l = mxf (row2 :: m) l k).
  apply IH3; auto.
  simpl in lmi; lia.
now apply (cols _ igt0).
Qed.

Lemma tp_involutive (i j : nat) (m : list (list Z)) :
  (0 < i)%nat -> (0 < j)%nat ->
  rectangular i j m ->
  tp (tp m) = m.
Proof.
intros igt0 jgt0 rectij.
destruct (tpP i j m igt0 jgt0 rectij) as [rectji fq].
destruct (tpP j i (tp m) jgt0 igt0 rectji) as [rect' f'q].
rewrite <- (mxfP i j); auto.
intros k l klti lltj.
rewrite f'q; auto.
Qed.
