Require Import List Arith ZArith Lia.

(* The following exercises should be solved using ring, auto, tauto,
  firstorder,
  lia, repeat, occasional apply, rewrite, and replace *)


Lemma auto_example (A : Type) (f : A -> A) (b : A -> A -> Prop)
      (e : A) :
  (forall x, b x x) ->
  (forall x y, b x y -> b x (f y)) ->
  b e (f (f (f e))).
Proof. ... Qed.

Lemma auto_example2 (A : Type) (f : A -> A) (b : A -> A -> Prop)
      (e : A) :
  (forall x, b x x) ->
  (forall x y, b x y -> b x (f y)) ->
  b e (f (f (f (f (f (f e)))))).
Proof. ... Qed.

Lemma auto_example3 (A : Type) (P Q : A -> Prop) (f : A -> A):
  (forall x, P x -> Q (f x)) -> (exists x, P x) ->
  (forall x, Q x -> Q (f x)) -> (exists y, Q (f (f y))).
Proof. ... Qed.

Lemma auto_example4 (A : Type) (P Q : A -> Prop) (f : A -> A):
  (forall x, P x -> Q (f x)) -> (exists x, P x) ->
  (forall x, Q x -> Q (f x)) -> (exists y, Q (f (f (f (f y))))).
Proof. ... Qed.

Lemma auto_example5 x y : 0 <= x < y -> 2 * x + 3 * y < 3 * x + 4 * y.
Proof. ... Qed.

Lemma auto_example6 (x y : nat) : 0 < x < y -> y <= y ^ 2.
Proof. ... Qed.

Lemma auto_example7 x y z :
  y ^ 3 = x ^ 2 ->
  (x + z) ^ 2 = y ^ 3 + 2 * x * z + z ^ 2.
Proof. ... Qed.

Open Scope Z_scope.
Lemma auto_example8 (x y z : Z) :
  y ^ 3 = x ^ 2 ->
  (x + z) ^ 2 = y ^ 3 + 2 * x * z + z ^ 2.
Proof. ... Qed.

Lemma Q1 : forall P : Prop, P -> P.
Proof. ... Qed.

Lemma Q2 : forall P Q : Prop, P -> Q -> P /\ Q.
Proof. ... Qed.

Lemma Q3 : forall P Q : Prop, P -> P \/ Q.
Proof. ... Qed.

Lemma Q4 : forall P Q : Prop, Q -> P \/ Q.
Proof. ... Qed.

Lemma Q5 : forall P Q : Prop, P /\ Q -> Q /\ P.
Proof. ... Qed.

Lemma Q6 : forall P Q : Prop, P \/ Q -> Q \/ P.
Proof. ... Qed.

Lemma Q7 : forall P Q R : Prop, (P -> Q) -> (Q -> R) -> (P -> R).
Proof. auto. Qed.

Lemma Q8 : forall P Q : Prop, ~ ((P -> Q) -> Q) -> ~ (P \/ Q).
Proof. ... Qed.

Lemma Q9 : forall P Q R : Prop, (P -> Q -> R) <-> ((P /\ Q) -> R).
Proof. ... Qed.

Require Import Classical.

Lemma Q10 : forall P Q : Prop, ((P -> Q) -> P) -> P.
Proof. 
(* destruct (classic A). permet de raisonner
   en supposant A dans un premier sous but et ~ A dans un second *)
...
Qed.

Lemma E1 : forall (T : Type) (A B C : T -> Prop),
  (exists x, A x /\ (B x \/ C x)) -> exists x, (A x /\ B x) \/ (A x /\ C x).
Proof. ... Qed.

Lemma E2 : forall (T : Type) (A B : T -> Prop),
  (exists x, A x /\ B x) -> ~ (forall x, ~ A x \/ ~ B x).
Proof. ... Qed.
