Require Import ZArith Lia.

Open Scope Z_scope.

Definition target : Z -> Type :=
  fun x => match x with 0 => bool | _ => Z end.

Definition g : forall x : Z, target x :=
  fun x => match x with 0 => true | _ => x end.

Check g 2.

Compute g 2.

Compute g 0.

Check Z.le_refl.

Lemma Z_le_add1 n m : n <= m -> n <= m + 1.
Proof. intros; lia. Qed.

Check Z.le_refl 3.

Check (Z_le_add1 3 3 (Z.le_refl 3 : 3 <= 3) : 3 <= 4).

Definition upper_bounded (k : Z) := {x : Z | x < k}.

Definition mkub {k : Z} (n : Z) (prf : n < k)
    : upper_bounded k :=
   exist _ n prf.

Definition toZ {k : Z} (n : upper_bounded k) :=
  proj1_sig n.

Coercion toZ : upper_bounded >-> Z.

Lemma Zm1lt x : x - 1 < x. Proof. lia. Qed.

Definition maxub (k : Z) : upper_bounded k :=
  mkub (k - 1) (Zm1lt k).

Check maxub.

Check maxub 3.

Compute maxub 3 + 2.

Fail Definition test_mkub (k x : Z) : upper_bounded k :=
  if x <? k then mkub x true else maxub k.

Definition test_mkub (k n : Z) : upper_bounded k.
destruct (n <? k) eqn:h.
  (* case where h has type n <? k = true *)
  apply (mkub n).
  apply Z.ltb_lt.
  exact h.
(* case where h has type n <? k = false *)
apply maxub.
Defined.

Definition inspect {A : Type}(a : A) : {b : A | a = b} :=
  exist (fun b' => a = b') a eq_refl.

Definition test_mkub2 (k x : Z) : upper_bounded k :=
  match inspect (x <? k) with
  | exist _ true h =>
    (* here h is a proof that x <? k = true *)
    mkub x (proj1 (Z.ltb_lt x k) h)
  | exist _ false _ => maxub k
  end.

Definition nat_dep_case (P : nat -> Prop) (h0 : P O)
  (hs : forall k, P (S k)) (n : nat) : P n :=
  match n with
  | O => h0 : P O
  | S p => hs p : P (S p)
  end.

Fixpoint nat_dep_rec (P : nat -> Prop) (h0 : P O)
  (hs : forall k, P k -> P (S k)) (n : nat) : P n :=
  match n with
  | O => h0
  | S p => hs p (nat_dep_rec P h0 hs p : P p) : P (S p)
  end.

Inductive t2 : nat -> Type :=
| start2 : t2 O
| step2 : forall n, t2 n -> t2 (S (S n)).

Inductive singleton3 : Z -> Prop :=
  is_3 : singleton3 3.

Definition singleton3_dep (x : Z) (prf : singleton3 x) 
  (P : Z -> Prop) (p3 : P 3) : P x :=
  match prf with is_3 => p3 end.

Inductive singleton {A : Type} (x : A) : A -> Prop :=
  single : singleton x x.

Definition singleton_dep
   {A : Type} {x y : A} (prf : singleton x y)
   (P : A -> Prop) (px : P x)  : P y :=
  match prf in singleton _ b return P b with
  | single _ => px
  end.  

Check single.

Check singleton_dep.
