\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{xcolor}
\definecolor{ttblue}{RGB}{48,48,170}
\setbeamertemplate{footline}[frame number]
\title{Advanced programming}
\author{Yves Bertot}
\date{December 2020}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Types in Coq}
\begin{itemize}
\item Types indicate what functions accept as argument and what they return
\item Information is given as types for function
\item Functions can also be arguments to other functions
\item Some types are {\em indexed} by values
\item Types also prescribe how data can be observed
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Examples of types}
\begin{itemize}
\item Some types are defined from the start
\begin{itemize}
\item {\tt nat},(natural numbers), {\tt bool}, {\tt unit}, {\tt Empty\_set},
  {\tt option}, {\tt prod}, {\tt sum}
\end{itemize}
\item Some types are available after loading libraries
\begin{itemize}
\item {\tt Require Import ZArith} gives {\tt Z} (unbounded integers),\\
  {\tt positive} (unbounded positive integers)
\item {\tt Require Import List} gives {\tt list} (similar to arrays)
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Function types}
\begin{itemize}
\item The type of a function is typically written {\tt T -> U}
\item When there are several arguments {\tt T1 -> T2 -> T3}
\item Functions with several arguments can receive just one argument
\item implicit parentheses in {\tt T1 -> T2 -> T3} are\\ {\tt T1 -> (T2 -> T3)}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Defining functions}
\begin{alltt}
Require Import ZArith.

Open Scope Z_scope.

Definition add3 (a : Z) (b : Z) (c : Z) : Z :=
  a + b + c.
\end{alltt}
\begin{itemize}
\item This defines a function with three arguments
\item The body of the function is {\tt a + b + c}
\end{itemize}
\end{frame}
\addtocounter{framenumber}{-1}
\begin{frame}[fragile]
\frametitle{Defining functions}
\begin{alltt}
Require Import ZArith.

Open Scope Z_scope.

Definition add3 (a b c : Z) : Z :=
  a + b + c.
\end{alltt}
\begin{itemize}
\item This defines a function with three arguments
\item The body of the function is {\tt a + b + c}
\end{itemize}
\end{frame}
\addtocounter{framenumber}{-1}
\begin{frame}[fragile]
\frametitle{Defining functions}
\begin{alltt}
Require Import ZArith.

Open Scope Z_scope.

Definition add3 a b c : Z :=
  a + b + c.
\end{alltt}
\begin{itemize}
\item This defines a function with three arguments
\item The body of the function is {\tt a + b + c}
\end{itemize}
\end{frame}
\addtocounter{framenumber}{-1}
\begin{frame}[fragile]
\frametitle{Defining functions}
\begin{alltt}
Require Import ZArith.

Open Scope Z_scope.

Definition add3 a b c :=
  a + b + c.
\end{alltt}
\begin{itemize}
\item This defines a function with three arguments
\item The body of the function is {\tt a + b + c}
\end{itemize}
\end{frame}
\addtocounter{framenumber}{-1}
\begin{frame}[fragile]
\frametitle{Syntax for using functions}
\begin{alltt}
Compute add3 1 2 3.
\textcolor{ttblue}{ = 6 : Z}
Compute add3 1 (add3 2 3 4) 5.
\textcolor{ttblue}{ = 15 : Z}
\end{alltt}
\begin{itemize}
\item Function written first, argument later, no parentheses
\item Parentheses needed only if one argument is itself a function call.
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Constructing a function without defining it}
\begin{alltt}
Compute (fun a b c => a + b + c) 1 2 3.
\textcolor{ttblue}{ = 6 : Z}
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Partial application}
\begin{alltt}
Check add3 2 3.
\textcolor{ttblue}{add3 2 3 : Z -> Z}
\end{alltt}
\begin{itemize}
\item Implicit parentheses in {\tt add3 2 3}:\\
{\tt (add3 2) 3}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Types of lists}
\begin{itemize}
\item {\tt list Z} is a type of lists whose elements are in {\tt Z}
\item {\tt list (list Z)} is a type of lists whose elements are lists of integers
\item The type of {\tt list} is : {\tt Type -> Type}
\item objects that have type '{\tt list ...}' have two possible shapes
\begin{itemize}
\item the empty list : {\tt nil}
\item a structure composed of two parts : the first element and the rest
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{A graphical drawing of Lists}
% trim left bottom right top
%  \framebox{
    \includegraphics[width=0.8\textwidth, trim=0 13cm 0 0, clip]{list_graph.pdf}
%  }

\end{frame}
\begin{frame}[fragile]
\frametitle{Observing and retrieving data from lists}
\begin{alltt}
Inductive list (A : Type) : Type :=
  | \textcolor{red}{nil : list A}
  | \textcolor{ttblue}{cons : A -> list A -> list A}.

Infix "::" :=
   cons (at level 60, right associativity) : list_scope.

Definition is_empty \{A : Type\} (l : list A) : bool :=
  match l with
  | \textcolor{red}{nil} => true
  | \textcolor{ttblue}{cons _ _ _} => false
  end.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Recursive programming with lists}
\begin{alltt}
Require Import List.

Fixpoint mymap \{A : Type\} (f : A -> A) (l : list A) :=
  match l with
  | nil => nil
  | e :: tail_list => f e :: mymap f tail_list
  end.
\end{alltt}
\end{frame}
\addtocounter{framenumber}{-1}
\begin{frame}[fragile]
\frametitle{Recursive programming with lists}
\begin{alltt}
Fixpoint mymap \textcolor{red}{\{A : Type\}} (f : A -> A) (l : list A) :=
  match l with
  | nil => nil
  | e :: tail_list => f e :: mymap \textcolor{red}{\framebox{\,\strut}} f tail_list
  end.
\end{alltt}
\begin{itemize}
\item implicit argument: curly or square braces in header\\
 not given at use time
\item Guessed by Coq
\end{itemize}
\end{frame}
\addtocounter{framenumber}{-1}
\begin{frame}[fragile]
\frametitle{Recursive programming with lists}
\begin{alltt}
Fixpoint mymap {\{A : Type\}} (f : A -> A) (l : list A) :=
  match l with
  | nil => nil
  | e :: tail_list => f e :: \textcolor{ttblue}{mymap f tail_list}
  end.
\end{alltt}
\begin{itemize}
\item Recursive call, only allowed on subterm
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Pattern-matching on other datatypes}
\begin{alltt}
Print option.
\textcolor{ttblue}{Inductive option (A : Type) : Type :=
  Some : A -> option A | None : option A

Arguments option _%type_scope
Arguments Some \{A\}%type_scope a : rename
Arguments None \{A\}%type_scope}
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Pattern-matching on option}
\begin{alltt}
Definition otion_to_default \{A : Type\} (def : A)
   (v : option A) : A :=
  match v with
  | Some v => v
  | None => def
  end.
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Finding existing functions}
\begin{itemize}
\item Searching by expected type
\item What should be the type of a function that gets the element
  at some index in a list?
\begin{itemize}
\item Takes as argument a list and a number
\item What type for numbers?
\item First try with {\tt Z} as the type for number
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Search attempt for a function}
\begin{alltt}
Search list Z.
\end{alltt}
\begin{itemize}
\item Too many answers, search needs to be refined
\item We want to find functions that would be provided as tools on lists
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Refined search attempt}
\begin{alltt}
Search list Z inside List.
\end{alltt}
\begin{itemize}
\item No answer
\item Maybe the type {\tt Z} is not the default for this kind of function
\item Try also {\tt nat}
\end{itemize}
\begin{alltt}
Search list nat inside List.
\end{alltt}
\begin{itemize}
\item many answers, but this can be refined.
\item Find that {\tt nth} has a similar meaning to our objective
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Collect information on an existing function}
\begin{alltt}
Search nth in List.
\textcolor{ttblue}{...
nth_middle : 
  forall [A : Type] (l l' : list A) (a d : A),
  nth (length l) (l ++ a :: l') d = a}
\end{alltt}
\begin{itemize}
\item restrict the search to the modules with basic facts about lists
\item {\tt nth\_middle} really says that nth will look inside a list
\item Other theorems also bring more information, eg. {\tt nth\_In}
\item You can then confirm that the function is what you want by reading
its definition
\end{itemize}
\begin{alltt}
Print nth.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Programming with numbers}
\begin{itemize}
\item Several types of numbers (like {\tt float} and {\tt int} in C)
\item Names are {\tt nat} or {\tt Z}
\item Recursive programming is handy with natural numbers
\item Recursion is not provided easily with integers
\item But iteration exists to repeat a function n times.
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Notations on arithmetic operations}
\begin{alltt}
Require Import ZArith Arith.

Check 3 + 6.
\textcolor{ttblue}{3 + 6 : nat}

Open Scope Z_scope.

Check 3 + 6.
\textcolor{ttblue}{3 + 6 : Z}

Check (3 + 6)%nat.
\textcolor{ttblue}{(3 + 6)%nat : nat}

Check Nat.even (3 + 6).
\textcolor{ttblue}{Nat.even (3 + 6) : bool}
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{{\tt Z.iter} at work}
\begin{alltt}
Compute Z.iter 10 (fun l => 1 :: l) nil.
\textcolor{ttblue}{= 1 :: 1 :: 1 :: 1 :: 1 :: 1 :: 1 :: 1 :: 1 :: 1 :: nil}

Compute
  Z.iter 10 (fun p => (fst p + 1, fst p :: snd p))
     (0, nil).
\textcolor{ttblue}{= (10,
     9 :: 8 :: 7 :: 6 :: 5 :: 4 :: 3 :: 2 :: 1 :: 0 :: nil)}
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Recursive programming with natural numbers}
\begin{alltt}
Print nat.
\textcolor{ttblue}{Inductive nat : Set :=  O : nat | S : nat -> nat}

Fixpoint myfact (n : nat) : nat :=
  match n with
  | O => 1
  | S p => n * myfact p
  end.

Compute myfact 6.
\textcolor{ttblue}{720}
\end{alltt}
Beware of memory limitations when computing with natural numbers
\end{frame}
\begin{frame}
\frametitle{Hands-on session}
\begin{itemize}
\item Define a list of numbers that are the length of months in a non-bissextile year
\item Define a function that takes as input a list of numbers and returns their sum
\item Define a function that computes the sum of all numbers between 0 and
a certain \(n\) (included).
\item Define a function that returns the difference between the previous
function and \(n \times (n + 1) / 2\)
\item Find an existing function that extract from a list all elements satisfying
 a boolean test
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Hands-on session (2)}
\begin{itemize}
\item Use function {\tt Z.iter} to compute large factorials
\item There is a function that takes a function and a list as input and
returns the list obtained by applying the function to all elements of the list,
find it (using the {\tt Search} command)
\item There is a function that produces a list of natural numbers
 of the form {\tt \(n\) :: \(n + 1\) :: ... :: \(n + k\) :: nil}
find it and use it to produce a list of {\bf integers} of the same form.
\item There is a function that takes two lists as inputs and produces
  a list of pairs, where the pairs contain an element of each list, find it.
\item When a matrix of integers is represented by a list of lists of integers,
design a function that transposes a matrix
\item Bigger project: compute the determinant of a square matrix
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{solutions}
\begin{verbatim}
Require Import ZArith List Bool.
Open Scope Z_scope.

Definition month_lengths :=
 31 :: 28 :: 31 :: 30 :: 31 :: 30 ::
 31 :: 31 :: 30 :: 31 :: 30 :: 31 :: nil.

Fixpoint sum_list (l : List Z) : Z :=
  match l with nil => 0 | a :: tl => a + sum_list tl end.

Fixpoint sum_n (n : nat) : nat :=
  match n with O => O | S p => (n + sum_n p)%nat end.

\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{Solutions (continued)}
\begin{verbatim}
Definition sum_z (n : Z) : Z :=
  snd (Z.iter n
         (fun p => (fst p + 1, (fst p + 1) + snd p))
         (0, 0)).

Definition test_sum_n (n : nat) : nat :=
  sum_n n - (n * (n + 1) / 2).

Search list bool headconcl:list inside List.
(* This should show at least the result "filter" *)

Search filter.
(* This will give a few lemmas about filter. *)
\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{Solutions (continued)}
\begin{verbatim}
Definition large_factorial (n : Z) : Z :=
  snd (Z.iter n
         (fun p => (fst p + 1, (fst p + 1) * snd p))
         (0, 1)).

Search headconcl:list hyp:(_ -> _).

Search headconcl:(list nat) hyp:nat.

Definition seqZ (start len : Z) :=
  map Z.of_nat (seq (Z.to_nat start) (Z.to_nat len)).
\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{Solutions (continued)}
\begin{verbatim}
Import ListNotations.

(* This solution uses a pre-existing function
   combine, but students can also define their
   own function to perform the equivalent of 
   map composed with combine. *)
Fixpoint transpose_matrix {A : Type}
  (mx : list (list A)) :=
  match mx with
  | nil => nil
  | l :: nil => map (fun x => x :: nil) l
  | l :: tl =>  map (fun '(x,  l) => x :: l)
                    (combine l (transpose_matrix tl))
  end.
\end{verbatim}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
