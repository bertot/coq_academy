From mathcomp Require Import all_ssreflect all_algebra.

Set Implicit Arguments.
Unset Strict Implicit.
Set Printing Implicit Defensive.

Import GRing.Theory.

Require Import fib.


Declare ML Module "paramcoq".

Parametricity bool.
Parametricity list.
Parametricity fastexp3.

Lemma list_bool_R_eq p1 p2 : list_R bool_R p1 p2 -> p1 = p2.
Proof.
induction 1 as [ | a a' ra l l' rel Ih].
  easy.
rewrite Ih; destruct ra; auto.
Qed.

Lemma list_bool_R_refl l : list_R bool_R l l.
Proof.
induction l as [ | a l Ih].
  constructor.
constructor; auto.
destruct a; constructor.
Qed.

Definition powers {A : Type} (op : A -> A -> A) a b :=
  exists k, b = ssrnat.iter k [eta op a] a.

Lemma iter_mul' (T : unitRingType) (e v : T) k:
  iter k [eta *%R e] v = e ^+ k * v.
Proof.
elim: k v => [ | k Ih].
  by move=> v; rewrite /= expr0 mul1r.
move=> v; rewrite /=.
by rewrite exprS Ih mulrA.
Qed.

Lemma fastexp3_genP (R : comUnitRingType) (k : nat) {A B : Type} (a acc : A)
      (op : A -> A -> A) (tomx : A -> 'M[R]_k.+1) l :
  (forall e1 e2, powers *%R (tomx a) (tomx e1) ->
      powers *%R (tomx a) (tomx e2) ->
     tomx (op e1 e2) = (tomx e1 * tomx e2)%R) ->
  (powers *%R (tomx a) (tomx acc)) ->
    tomx (fastexp3 op a l acc) = fastexp3 *%R (tomx a) l (tomx acc).
Proof.
move=> opP pow_acc.
set rel := fun (e : A) (m : 'M[R]_k.+1) =>
     powers *%R (tomx a) m /\ tomx e = m.
have relP : forall e1 m1, rel e1 m1 -> forall e2 m2, rel e2 m2 ->
  rel (op e1 e2) (m1 * m2).
  move=> e1 m1 [[k1 p1] e1m1] e2 m2 [[k2 p2] e2m2]; split.
    exists (k1 + k2 + 1)%N.  
    rewrite p1 p2 !iter_mul' !mulrA.
    by rewrite -(mulrA _ (tomx a)) -exprS -exprD addn1 addnS.
  rewrite -e2m2 -e1m1; apply: opP.
    by exists k1; rewrite e1m1.
  by exists k2; rewrite e2m2.
have rela : rel a (tomx a) by split;[exists O%N | ].
have relacc : rel acc (tomx acc) by split;[exact: pow_acc | ].
by have [relr rq ] :=fastexp3_R relP rela (list_bool_R_refl l) relacc.
Qed.

