\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{xcolor}
\newcommand{\coqand}{{\tt/\char'134}}
\newcommand{\coqor}{{\tt\char'134/}}
\newcommand{\coqnot}{\tt\raise.17ex\hbox{$\scriptstyle\mathtt{\sim}$}}
\definecolor{ttblue}{RGB}{48,48,170}
\setbeamertemplate{footline}[frame number]
\title{Automated tactics}
\author{Yves Bertot}
\date{June 2020}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Automation: a technical overview}
\begin{itemize}
\item Learn which domains are covered by automation
\begin{itemize}
\item Numeric equalities: {\tt ring} and {\tt field}
\item numeric inequalities: linear arithmetic, {\tt lia}, {\tt ria}
\item Numeric reasoning with real functions: {\tt autoderive} and {\tt interval}
\end{itemize}
\item Logical automation
\begin{itemize}
\item {\tt auto} : backward chaining
\item {\tt tauto} : complete propositional logic
\item {\tt firstorder} : first order logic
\end{itemize}
\item Programming new automation
\begin{itemize}
\item {\tt Ltac} language
\item reflection tactics
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Numeric equalities}
\begin{itemize}
\item Number structures often have +, *, 0, 1, and distributivity
\item equalities between expressions are easy to automate
\begin{itemize}
\item The tactic is {\tt ring}
\item To be used when the goal is an equality
\item Also usable with boolean expressions.
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example use of {\tt ring}}
\begin{alltt}
Require Import Arith ZArith.
Open Scope Z_scope.

Lemma ring_example x y :
   x + (y - x) * (y + x) = y ^ 2 - x ^ 2 + x.
Proof. ring. Qed.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Ring limitation on {\tt nat}}
\begin{alltt}
Close Scope Z_scope.

Lemma bad_ring_example (x y : nat) :
   (x + (y - x) * (y + x) = y ^ 2 - x ^ 2 + x)%nat.
Proof. 
Fail ring.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{But it is provable!}
\begin{alltt}
rewrite !Nat.mul_sub_distr_r, !Nat.mul_add_distr_l.
rewrite Nat.sub_add_distr,  (Nat.add_comm x).
replace (x * x) with (x ^ 2) by (simpl; ring).
rewrite (Nat.mul_comm y x), Nat.add_sub.
now replace (y * y) with (y ^ 2) by (simpl; ring).
Qed.
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Linear arithmetic}
\begin{itemize}
\item Linear formula: \(3 * x + 4 * y - 1 \leq 2 * x + 5 * y\)
\item Conjunctions of linear formulas are decidable
\item When variables are integers or natural numbers: {\tt lia}
\item When variables are real numbers: {\tt lra}
\begin{itemize}
\item Linear Integer Arithmetic (respectively Real)
\end{itemize}
\item Caveat: only quantifier free Presburger arithmetic, even though
full Presburger is also decidable
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{An example with {\tt lia}}
\begin{alltt}
Lemma toto  x y z :
   - x - 6 <= 2 * y ->
   y <= 2 * x + 5 ->
   4 * x - 3 <= y ->
   3 <= z ->
   3 * x - z <= y.
Proof. intros; lia. Qed.
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{More numbers and automation}
\begin{itemize}
\item Coq also provides libraries for real numbers
\item Mathematical functions: \(\sin\), \(\cos\), \(\ln\), \(e^x\)
\item Integrals and derivatives
\item Proofs: symbolic reasoning on functions, approximations on values
\item Users may need to install Coq extensions
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Symbolic reasoning on functions}
\begin{small}
\begin{alltt}
Require Import Reals Coquelicot.Coquelicot.
Open Scope R_scope.

Lemma derive_example x y z :
  is_derive (fun y => x ^ 2 + y ^ 3 + 2 * z * y ^ 2) y
       (3 * y ^ 2 + 4 * z * y).
Proof.
auto_derive.
(* First goal: conditions for derivability. *)
\textcolor{ttblue}{True}
(* Second goal: the automatic derivative is equal *)
\textcolor{ttblue}{1 * ((1 + 1 + 1) * (y * (y * 1))) +
  2 * z * (1 * ((1 + 1) * (y * 1))) =
 3 * y ^ 2 + 4 * z * y}
easy. ring.
Qed.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Automatic approximation}
\begin{alltt}
Require Import Reals Coquelicot.Coquelicot.
Require Import Interval.Tactic.

Lemma sqrt2_approx : 
  Rabs (sqrt 2 - (3 / 5 + PI / (7 - PI))) <= 1 / 10 ^ 5.
Proof. interval. Qed.

Lemma sin_0_1 x : 0 <= x <= 1 -> 0 <= sin x <= 0.842.
Proof. intros; interval. Qed.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Finer grain approximation}
\begin{alltt}
Lemma sin_0_1_affine x : 0 <= x <= 1 ->
   0.842 * x - 0.001 <= sin x
     <= 0.842 * x + 0.06.
Proof.
intros; split.
  unfold Rminus; rewrite Rplus_comm, <- Rle_minus_r.
  (* x occurs only on one side of the comparison. *)
  Fail interval.
  interval with (i_bisect x).
rewrite Rplus_comm, <- Rle_minus_l.
interval with (i_bisect x).
Qed.
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Collections of theorems}
\begin{itemize}
\item Repeat theorem application
\item Repeat tactics
\item Repetition as a Coq program
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Combining and repeating tactics}
\begin{itemize}
\item {\tt ;} (sequence), {\tt repeat}, {\tt ||} (orelse), {\tt idtac}, {\tt fail}
\item derived tactics: {\tt try}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{The {\tt auto} tactic: repeat theorem database application}
\begin{itemize}
\item Backward chaining: use the goal to guide search
\item Use the hypotheses as a data base of theorems and facts
\item Use extra pre-defined theorem stored as hints
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Automation playground}
\begin{small}
\begin{alltt}
Variables (A : Set) (blue red : A -> Prop).
Variables (f g: A -> A) (a0 : A).

Hypothesis blue_f : forall x, red x -> blue x -> blue (f x).
Hypothesis red_g : forall x, red (g x) -> red x.
Hypothesis red_g2_a0 : red (g (g a0)).
Hypothesis blue_a0 : blue a0.
Hypothesis red_f : forall x, red x -> red (f x).

\textcolor{red}{Hint Resolve blue_f red_g red_g2_a0 blue_a0 red_f : colors.}

Lemma auto_example : blue (f (f a0)).
Proof. \textcolor{red}{auto with colors.} Qed.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Play-by-play}
\begin{small}
\begin{alltt}
Lemma blue_fa0 : blue (f a0).
Proof. 
apply blue_f.
apply red_g.
apply red_g.
apply red_g2_a0.
apply blue_a0.
Qed.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{A limit of auto}
\begin{small}
\begin{alltt}
Lemma blue_fffa0' : blue (f (f (f a0))).
Proof.
Fail now auto with colors.
now auto \textcolor{red}{6} with colors.
Qed.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Do-it-yourself automation}
\begin{small}
\begin{alltt}
Ltac repeat_colors :=
repeat (apply blue_f || apply red_f || apply red_g2_a0 ||
        apply red_g || apply blue_a0).

Lemma blue_f8a0' : blue (f (f (f (f (f (f (f (f a0)))))))).
Proof. repeat_colors. Qed.
\end{alltt}
\end{small}
Beware of bugs, the order of theorem application matters
\begin{itemize}
\item {\tt red\_g2\_a0} should be tried before {\tt red\_g}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{the {\tt tauto} tactic}
\begin{itemize}
\item {\tt auto} fails if facts are wrapped in logical connectives in the context
\item {\tt tauto} will unwrap the context
\item {\tt tauto} is know to be complete for propositional logic
\item But {\tt tauto} does not use theorem data bases
\item {\tt intuition} performs the same tasks as {\tt tauto} and then calls
{\tt auto}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example with {\tt tauto}}
\begin{alltt}
Lemma ex_tauto (A B C : Prop) : A \coqand{} B \coqand{} C -> A \coqand{} C.
Proof.
\textcolor{red}{Fail solve[auto]}. (* auto does not solve this goal. *)
tauto. (* tauto does *)
Qed.
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{First order logic}
\begin{itemize}
\item First order logic formulas have quantification predicates and quantification over their arguments
\item There is a tactic named {\tt firstorder} for this kind of formulas
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example using {\tt firstorder}}
\begin{alltt}
Lemma ex_firstorder (A : Type) (P Q R : A -> Prop) :
  (forall x, P x -> Q x \coqand{} R x) ->
  (exists x, P x) ->
  (exists y, Q y \coqand{} P y).
Proof. firstorder. Qed.
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Reflecting on automatic proofs}
\begin{itemize}
\item Automatic proofs can be the object of reasoning
\item This often yields a program in Coq
\item This program can be used as yet another proof tool
\item Example
\begin{itemize}
\item colors: {\tt blue(f (...~(f a0)...))} always holds
\item expressed as {\tt blue\_n : \(\forall\) n, blue (Nat.iter n f a0)}\\
(proof omitted)
\end{itemize}
\item {\tt blue\_n} is a program that takes as input a natural number
and returns a proof that {\tt blue (f (f ... a0))} holds
\item Trouble: conclusion written as {\tt blue (Nat.iter ...)}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Reflected automatic proof}
\begin{alltt}
Lemma blue_f6a0 : blue (f (f (f (f  (f (f a0)))))).
Proof. exact (blue_n 6). Qed.
\end{alltt}
\begin{itemize}
\item {\tt blue\_n 6} has type {\tt Nat.iter 6 f a0}
\item The tactic {\tt apply blue\_n} will not find the value 6
\item {\tt f (f (f (f (f (f a0)))))} needs to be reverse computed
 into {\tt Nat.iter 6 f a0}
\item Computing the number can be programmed
\item This is called {\em reification}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Computing Coq data from terms}
\begin{alltt}
Ltac mk_nat  term :=
  match term with
  | f ?x => let v := mk_nat x in constr:(S v)
  | a0 => O
  end.

Ltac reflect_f :=
match goal with
  | |- blue ?x => 
    let v := mk_nat x in 
       apply (blue_n v)
end.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Computing Coq data from terms}
\begin{alltt}
Lemma blue_f10a0' : 
  blue (f (f (f (f (f (f (f (f (f (f a0)))))))))).
Proof. reflect_f. Qed.
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{More applications of reflection}
\begin{itemize}
\item Here we recognize the language {\tt f ... (f a0) ...}
\item The function {\tt Nat.iter} {\em inteprets} the number into a
concrete term of type {\tt nat}
\item In the {\tt ring} tactic, the language consists of
operators {\tt +}, {\tt *}, {\tt -}, and arbitrary numbers and variables
\item In the {\tt ring} tactic a specific interpretation function is
defined
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Arithmetic expressions}
\begin{alltt}
Inductive expr :=
| add (x y : expr) | mul (x y : expr) | opp (x : expr)
| exp (x : expr) (k : nat) | cst (z : Z) | var (n : nat).
\end{alltt}
\begin{itemize}
\item Datatype {\tt expr} plays the same role in {\tt ring} as {\tt nat} in our {\tt reflect\_f}
\end{itemize}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
