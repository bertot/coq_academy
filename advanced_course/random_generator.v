Require Import ZArith List.

Open Scope Z_scope.

(* Linear congruential generator. This is used to produce a low quality 
  sequence of random numbers.  References can be found at
  https://en.wikipedia.org/wiki/Linear_congruential_generator
  Apparently, the sequence used here is taken from "numerical recipes" by
  Press, Teukolsky, Vetterling, and Flannery, Cambridge University Press *)

Definition lcg_step (modulus a c seed : Z) := ((a * seed + c) mod modulus)%Z.

Definition iterates (modulus a c seed len : Z) :=
 Z.iter len 
   (fun p : list Z * Z => let acc := fst p in let seed := snd p in 
       let seed' := lcg_step modulus a c seed in
       (seed'::acc, seed')) (nil, seed).

(* We settle with a list of 2000 elements, taken between 0 and 2 ^ 32.
  The randomness is quite weak, as they alternate between odd and even
  numbers. *)
Definition rnd_list :=
    Eval compute in fst (iterates (2 ^ 32) 1664525 1013904223 33 3000). 

Definition rnd_list2 := 
    Eval compute in fst (iterates (2 ^ 32) 1664525 1013904223 1937 3000). 

(* For better randomness, we take only the higher 16 bits.  Then we can
  use a modulo operation to bring this inside a chosen interval. *)
Definition randp16 (rand : list Z) :=
  let p16 := (2 ^ 16)%Z in map (fun z => (z / p16)%Z) rand.

Definition nums := Eval compute in randp16 rnd_list.

Definition nums2 := Eval compute in randp16 rnd_list2.
