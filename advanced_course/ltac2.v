Require Import Arith.
From Ltac2 Require Ltac2.

Section playground.

Variables (A : Set) (blue red : A -> Prop).
Variables (f g : A -> A) (a0 : A).
Hypothesis blue_f : forall x, red x -> blue x -> blue (f x).
Hypothesis red_g : forall x, red (g x) -> red x.
Hypothesis red_g2_a0 : red (g (g a0)).
Hypothesis blue_a0 : blue a0.
Hypothesis red_f : forall x, red x -> red (f x).

Lemma blue_n n : blue (Nat.iter n f a0).
Proof.
assert (br : blue (Nat.iter n f a0) /\ red (Nat.iter n f a0)).
  induction n as [ | n IH].
    split; [exact blue_a0 | exact (red_g _ (red_g _ red_g2_a0))].
  simpl; split;[apply blue_f; tauto | apply red_f; tauto].
tauto.
Qed.

(* an example in Ltac that we will letter mimick in Ltac2 *)
Ltac mk_nat  term :=
  match term with
  | f ?x => let v := mk_nat x in constr:(S v)
  | a0 => O
  end.

Ltac reflect_f :=
match goal with
  | |- blue ?x =>
    let v := mk_nat x in
       apply (blue_n v)
end.

Lemma sandbox : blue (f (f (f a0))).
Proof.
reflect_f.
Qed.

Import Ltac2.

Lemma Nat_iter_add {B} (ff : B -> B) x n m :
   Nat.iter (n + m) ff x = Nat.iter n ff (Nat.iter m ff x).
Proof.
(* An example of using Ltac2 notations for simple tactic combination. *)
(induction n as [ | n IH]; simpl) > [ auto | rewrite &IH]; auto.
Qed.

(* a more generic iteration counting function.
  - the_fun is the function for which we want to count iterations
  - c is the numbero of iterations we have already encountered so far
  - the returned value is not the count, but a term written with
    Nat.iter that should be convertible to
      (Nat.iter $the_fun $c $term), but where $term is not of the form
    ($the_fun $some_argument) *)
Ltac2 rec count_iter2 (the_fun : constr) (c : constr) (term : constr) :=
  match! term with
  | ?g ?a => 
    match Constr.equal g the_fun with
      true =>
      count_iter2 the_fun open_constr:(S $c) a
    | _ => constr:(Nat.iter $c $the_fun $term)
    end
  | _ => constr:(Nat.iter $c $the_fun $term)
  end.

(* This is a simple version of change, where both arguments are
  ground terms of type constr.  Note the way a call to an Ltac tactic
  is performed: arguments that need to be passed from the Ltac2 world to
  the Ltac1 world are described as a context.  Conversion function are
  explicitely used for the terms of type constr. *)
Ltac2 simple_change_with (lhs : constr) (rhs : constr) :=
  (ltac1:(a b |- change a with b)
    (Ltac1.of_constr lhs) (Ltac1.of_constr rhs)).
  
(* This tactic is still parameterized by the function that we wish to
  recognize.  Note that Control.zero will force the first match clause
  to backtrack until we find an occurrence of an application with the right
  function. *)
Ltac2 mkiter2 (ff : constr) :=
  match! goal with
  | [ |- context [?g ?t] ] => 
    match Constr.equal g ff with
    | true => 
      simple_change_with constr:($g $t)
         (count_iter2 ff open_constr:(S O) t)
    | _ => Control.zero Match_failure
    end
  end.

(* Note how we need to place f in a qualifier to express that we are passing
  a Ltac term of type constr as argument. *)
Lemma sandbox1 : blue (f (f (f a0))).
Proof.
mkiter2 'f.
apply blue_n.
Qed.

(* another experience with Ltac1. *)
Ltac trial1 := match goal with
  | |- context [f ?x] =>
    change (f x) with (Nat.iter 1 f x)
  end.

Lemma sandbox2 : blue (f (f (f a0))).
Proof.
  ltac1:(trial1).
  repeat ltac1:(trial1); rewrite <- !Nat_iter_add; apply blue_n.
Qed.

Fail Ltac2 trial := match! goal with
  | [ |- context [Nat.add ?y ?x] ] =>
    simple_change_with constr:(&f $x) (constr:(Nat.iter 1 &f $x))
  end.

(* Now we show how to mimick mk_nat and reflect_f in Ltac2.
  Note that f is a section variable and cannot be referred to in
  in the code of mk_nat2.  On the other hand, we can refer to
  the element in the context whose name if f, using Control.hyp. *)
Ltac2 rec mk_nat2 term :=
  match! term with
  | ?ff ?x =>
    match Constr.equal ff (Control.hyp @f) with
      true => let v := mk_nat2 x in constr:(S $v)
    | false => constr:(0)
    end
  | _ => constr:(O)
  end.

Ltac2 reflect_f2 () :=
  match! goal with
  | [ |- ?fblue ?x ] =>
      match Constr.equal fblue (Control.hyp @blue) with
      | true => let v := mk_nat2 x in
        apply (blue_n $v)
      | false => Control.zero Match_failure
      end
   end.

Lemma sandbox3: blue (f (f (f a0))).
Proof.
reflect_f2().
Qed.

Ltac2 ffun1_arg (ff : constr) := match! goal with
  | [ |- context [?g ?x] ] =>
    match Constr.equal g ff with
    | true =>
      let e1 := constr:($g $x) in
      simple_change_with e1 (constr:(Nat.iter 1 $g $x))
    | false => Control.zero Match_failure
    end
  end.

Ltac2 rec ffun_arg (ff : constr) := match! goal with
  | [ |- context [Nat.iter ?n ?g (?g' ?x)] ] =>
    match Constr.equal g' ff with
    | true => 
      match Constr.equal g ff with
      | true => 
        let e1 := constr:(Nat.iter $n $g ($g' $x)) in
        let e2 := constr:(Nat.iter (S $n) $ff $x) in
        simple_change_with e1 e2;  ffun_arg ff
      | false =>
        Control.zero Match_failure
      end
    | false => Control.zero Match_failure
    end
  | [ |- context [?g ?x] ] =>
    match Constr.equal g ff with
    | true =>
      simple_change_with constr:($ff $x) constr:(Nat.iter 1 $ff $x);
      ffun_arg ff
    | _ => Control.zero Match_failure
    end
  | [ |- _ ] => ()
  end.

Ltac find_fun_arg ff := match goal with
  | |- context [Nat.iter ?n ff (ff ?x)] =>
    change (Nat.iter n ff (ff x)) with (Nat.iter (S n) ff x); find_fun_arg ff
  | |- context [ ff ?x] => 
    change (ff x) with (Nat.iter 1 ff x); find_fun_arg ff
  | _ => idtac
 end.

Ltac count_iter1 f n term :=
  match term with
  | (f ?x) =>
     let n' := constr:(S n) in
     count_iter1 f n' x
  | _ => constr:((n, term))
  end.

Ltac mkiter1 f := match goal with
  | |- context[f ?x] =>
    let v := count_iter1 f O x in
    let n := eval lazy iota beta delta [fst] in (fst v) in
    let a := eval lazy iota beta delta [snd] in (snd v) in
    change (f x) with (Nat.iter (S n) f a)
  end.

Lemma sandbox4: blue (Nat.iter 1000 f a0).
Proof.
simpl.
Time ffun_arg constr:(f).
simpl.
Time ltac1:(find_fun_arg f).
simpl.
Time mkiter2 constr:(f).
simpl.
Time ltac1:(mkiter1 f).
apply blue_n.
Qed.

End playground.
