Require Import ZArith List FMapPositive Bool Mergesort Orders.
Require Import random_generator.

Open Scope Z_scope.

Definition ZtoPos z :=
  match z with Z.pos x => x | Z.neg x => x | Z0 => 1%positive end.

Import PositiveMap.

Notation trie_set := (PositiveMap.tree bool).

(* In the following definitions, the suffix "m" is used to indicate the
  use of a positive map to encode a finite set. *)
Fixpoint interm (s1 s2 : trie_set) : trie_set :=
  match s1, s2 with
  | Node s1_1 o1 s1_2, Node s2_1 o2 s2_2 =>
    Node (interm s1_1 s2_1)             (* recursive call     *)
         (match o1, o2 with              (* take local value   *)
            Some _ , Some _ => Some true (* if present in      *)
          | _, _ => None                 (* both tries         *)
          end)
         (interm s1_2 s2_2)             (* recursive call     *)
  | Leaf _, t => Leaf bool               (* empty is absorbing *)
  | t, Leaf _ => Leaf bool
  end.

(* Your job: define an union function by following the same spirit. *)

(* Fixpoint unionm (s1 s2 : trie_set) : trie_set := *)

Arguments mem {A}%type.

Import PositiveMap.

Lemma intermP :
  forall s1 s2 x, mem x s1 = true /\ mem x s2 = true <->
                  mem x (interm s1 s2) = true.
Proof.
induction s1 as [ | s1_1 IH1 [v1|] s1_2 IH2];
   intros [ | s2_1 [v2|] s2_2] [x1 | x1 | ].
(* Here 27 goals are generated. *)
all: simpl; try (intuition discriminate).
(* Here only 8 goals remain. *)
all: rewrite <- ?IH2, <- ?IH1; tauto.
Qed.

(* your job: prove that unionm represents correctly the union
  of its two arguments. *)

