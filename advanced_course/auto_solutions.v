Require Import List Arith ZArith Lia.

(* The following exercises should be solved using ring, auto, tauto,
  lia, repeat, occasional apply, rewrite, and replace *)


Lemma auto_example (A : Type) (f : A -> A) (b : A -> A -> Prop)
      (e : A) :
  (forall x, b x x) ->
  (forall x y, b x y -> b x (f y)) ->
  b e (f (f (f e))).
Proof.
intros bxx bxf.
auto.
Qed.

Lemma auto_example2 (A : Type) (f : A -> A) (b : A -> A -> Prop)
      (e : A) :
  (forall x, b x x) ->
  (forall x y, b x y -> b x (f y)) ->
  b e (f (f (f (f (f (f e)))))).
Proof.
intros bxx bxf.
auto 20.
Qed.

Lemma auto_example3 (A : Type) (P Q : A -> Prop) (f : A -> A):
  (forall x, P x -> Q (f x)) -> (exists x, P x) ->
  (forall x, Q x -> Q (f x)) -> (exists y, Q (f (f (f (f y))))).
Proof.
intros PQf Px QQf.
assert (exists y, Q (f (f y))) by firstorder.
firstorder.
Qed.

Lemma auto_example4 (A : Type) (P Q : A -> Prop) (f : A -> A):
  (forall x, P x -> Q (f x)) -> (exists x, P x) ->
  (forall x, Q x -> Q (f x)) -> (exists y, Q (f (f (f (f y))))).
Proof.
intros PQf Px QQf.
assert (exists y, Q (f (f y))) by firstorder.
firstorder.
Qed.

Lemma auto_example5 x y : 0 <= x < y -> 2 * x + 3 * y < 3 * x + 4 * y.
Proof.
lia.
Qed.

Lemma auto_example6 x y : 0 < x < y -> y <= y ^ 2.
Proof.
intros xlty.
assert (2 <= y) by lia.
assert (y * 2 <= y * y).
  apply Nat.mul_le_mono_l.
  assumption.
replace (y ^ 2) with (y * y) by (simpl; ring).
lia.
Qed.

Lemma auto_example7 x y z :
  y ^ 3 = x ^ 2 ->
  (x + z) ^ 2 = y ^ 3 + 2 * x * z + z ^ 2.
Proof.
intros y3x2.
rewrite y3x2.
simpl; ring.
Qed.

Open Scope Z_scope.
Lemma auto_example8 (x y z : Z) :
  y ^ 3 = x ^ 2 ->
  (x + z) ^ 2 = y ^ 3 + 2 * x * z + z ^ 2.
Proof.
intros y3x2.
rewrite y3x2.
ring.
Qed.

Lemma Q1 : forall P : Prop, P -> P.
Proof. auto. Qed.

Lemma Q2 : forall P Q : Prop, P -> Q -> P /\ Q.
Proof. auto. Qed.

Lemma Q3 : forall P Q : Prop, P -> P \/ Q.
Proof. auto. Qed.

Lemma Q4 : forall P Q : Prop, Q -> P \/ Q.
Proof. auto. Qed.

Lemma Q5 : forall P Q : Prop, P /\ Q -> Q /\ P.
Proof. tauto. Qed.

Lemma Q6 : forall P Q : Prop, P \/ Q -> Q \/ P.
Proof. tauto. Qed.

Lemma Q7 : forall P Q R : Prop, (P -> Q) -> (Q -> R) -> (P -> R).
Proof. auto. Qed.

Lemma Q8 : forall P Q : Prop, ~ ((P -> Q) -> Q) -> ~ (P \/ Q).
Proof. tauto. Qed.

Lemma Q9 : forall P Q R : Prop, (P -> Q -> R) <-> ((P /\ Q) -> R).
Proof. tauto. Qed.

Require Import Classical.

Lemma Q10 : forall P Q : Prop, ((P -> Q) -> P) -> P.
Proof. 
(* destruct (classic A). permet de raisonner
   en supposant A dans un premier sous but et ~ A dans un second *)
intros P Q peirce. destruct (classic P); tauto.
Qed.

Lemma E1 : forall (T : Type) (A B C : T -> Prop),
  (exists x, A x /\ (B x \/ C x)) -> exists x, (A x /\ B x) \/ (A x /\ C x).
Proof. firstorder. Qed.

Lemma E2 : forall (T : Type) (A B : T -> Prop),
  (exists x, A x /\ B x) -> ~ (forall x, ~ A x \/ ~ B x).
Proof.
firstorder.
Qed.

Require Import Classical.

Lemma E3 : forall (T : Type) (P : T -> Prop), ~ (forall x, P x) -> exists x, ~ P x.
Proof.
intros T P.
(* destruct (classic P). permet de raisonner
   en supposant P dans un premier sous but et ~ P dans un second *)
destruct (classic (exists x, ~ P x)).
firstorder.
intros NFA; destruct NFA; intros x.
destruct (classic (P x)); firstorder.
Qed.
