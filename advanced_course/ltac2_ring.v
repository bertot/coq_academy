Require Import ZArith Arith List Sorting.

Inductive expr :=
| add (x y : expr) | mul (x y : expr) | opp (x : expr)
| exp (x : expr) (k : nat) | cst (z : Z) | var (n : nat).

Record rfuns {A : Type} := {
  rf_add : A -> A -> A
; rf_mul : A -> A -> A
; rf_opp : A -> A
; rf_exp : A -> nat -> A
; rf_0 : A
; rf_1 : A}.

Definition z_interp {A : Type} (ffs : @rfuns A) z :=
match z with
| Z.neg p =>
  rf_opp ffs (Z.iter (Z.pos p)
       (fun x => rf_add ffs (rf_1 ffs) x) (rf_0 ffs))
| _ => Z.iter z (fun x => rf_add ffs (rf_1 ffs) x) (rf_0 ffs)
end.

Fixpoint e_interp {A : Type} (ffs : @rfuns A) (l : list A) (e : expr) :=
match e with
| add x y => rf_add ffs (e_interp ffs l x) (e_interp ffs l y)
| mul x y => rf_mul ffs (e_interp ffs l x) (e_interp ffs l y)
| opp x => rf_opp ffs (e_interp ffs l x)
| exp x n => rf_exp ffs (e_interp ffs l x) n
| cst z => z_interp ffs z
| var n => nth n l (rf_0 ffs)
end.

Fixpoint insert (v : nat) (k : nat) (monomial : list (nat * nat)) :
   list (nat * nat) :=
match monomial with
| nil => (v, k)::nil
| (v', k')::l' =>
  if (v <? v')%nat then
     (v,k)::monomial
  else if (v =? v')%nat then
     (v', (k + k')%nat):: l'
  else (v', k') :: insert v k l'
end.

Fixpoint sort (monomial : list (nat * nat)) :=
match monomial with
| nil => nil
| (v, k) :: l => insert v k (sort l)
end.

Fixpoint monomial_cmp (l1 l2 : list (nat * nat)) :=
match l1, l2 with
| nil, nil => Eq
| (_, _)::_, nil => Gt
| nil, (_, _)::_ => Lt
| (v1, k1)::l1', (v2, k2)::l2' =>
  if v1 =? v2 then
     if k1 <? k2 then
       Lt
     else if k2 <? k1 then
       Gt
     else
       monomial_cmp l1' l2'
  else if v1 <? v2 then
    Gt
  else
    Lt
end.

Lemma cmp_eq l1 l2 : monomial_cmp l1 l2 = Eq -> l1 = l2.
Proof.
revert l2; induction l1 as [ | [v k] l1 IH].
  intros [ | [v' k'] l2].
    easy.
  discriminate.
intros [ | [v' k'] l2]; simpl.
  discriminate.
destruct (v =? v') eqn:cmpv.
  destruct (k <? k') eqn:cmp1k.
    discriminate.
  destruct (k' <? k) eqn:cmp2k.
    discriminate.
  replace v' with v by now rewrite <- Nat.eqb_eq.
  replace k' with k.
    now intros IHa; rewrite (IH _ IHa).
  now apply le_antisym; rewrite <- Nat.ltb_ge.
destruct (v <? v'); discriminate.
Qed.

Fixpoint mon_add (coef : Z) (mono : list (nat * nat))
   (p : list (Z * list (nat * nat))):=
match p with
| nil => (coef, mono) :: nil
| (c, m)::p' => 
  match monomial_cmp mono m with
  | Lt => (coef, mono) :: p
  | Eq => if (coef + c =? 0)%Z then p' else ((coef + c)%Z, mono):: p'
  | Gt => (c, m) :: mon_add coef mono p'
  end
end.
    
Fixpoint poly_add (p1 p2 : list (Z * list (nat * nat))) :=
match p1 with
| nil => p2
| (c, m)::p1' => mon_add c m (poly_add p1' p2)
end.

Definition mon_mul (coef : Z) (mono : list (nat * nat)) p :=
map (fun t => let '(c, m) := t in
  ((coef * c)%Z,
    fold_right 
       (fun p m => let '(v, k) := p in insert v k m) mono m)) p.

Fixpoint poly_mul (p1 p2 : list (Z * list (nat * nat))) :=
match p1 with
| (c, m) :: p1' =>
  poly_add (mon_mul c m p2) (poly_mul p1' p2)
| nil => nil
end.

Definition p_interp {A : Type} (ffs : @rfuns A) (l : list A)
   (p : list (Z * list (nat * nat))) :=
fold_right
  (fun '(z, m) a => rf_add ffs (rf_mul ffs (z_interp ffs z)
     (fold_right (fun '(v, k) b => rf_mul ffs (rf_exp ffs (nth v l (rf_0 ffs)) k) b) (rf_1 ffs)
              m)) a) (rf_0 ffs) p.

Fixpoint to_poly (e : expr) :=
match e with
| add x y => poly_add (to_poly x) (to_poly y)
| mul x y => poly_mul (to_poly x) (to_poly y)
| opp x => map (fun '(z, m) => (Z.opp z, m)) (to_poly x)
| exp x n => Nat.iter n (fun p => poly_mul (to_poly x) p) ((1%Z, nil)::nil)
| cst z => (z, nil)::nil
| var n => (1%Z, ((n, 1)::nil))::nil
end.

Compute
let x := 0 in let y := 1 in let z := 2 in
let the_exp :=  add (var x) (exp (mul (var y) (var z)) 3) in
  (e_interp (Build_rfuns Z Z.add Z.mul Z.opp Zpower_nat 0%Z 1%Z )
  (2::3::42::nil)%Z the_exp -
  p_interp (Build_rfuns Z Z.add Z.mul Z.opp Zpower_nat 0%Z 1%Z )
 (2::3::42::nil)%Z (to_poly the_exp))%Z.

Definition Zrfuns := (Build_rfuns Z Z.add Z.mul Z.opp Zpower_nat 0%Z 1%Z).

Lemma z_interp_correct z : z_interp Zrfuns z = z.
Proof.
unfold z_interp.
(* This reveals that z_interp is probably too inefficient for a really strong
  ring tactic. *)
assert (iter_step : forall p k , Pos.iter (fun x => (1 + x)%Z) k p =
                  (Z.pos p + k)%Z).
  induction p as [ p Ip | p Ip | ]; intros k.
      cbn -[Z.add]; rewrite Pos2Z.pos_xI, !Ip.
       replace 2%Z with (1 + 1)%Z by easy.
      rewrite Z.mul_add_distr_r, Z.mul_1_l, !Z.add_assoc, (Z.add_comm 1).
      now rewrite (Z.add_shuffle0 _ 1).
    cbn -[Z.add]; rewrite Pos2Z.pos_xO, !Ip.
    replace 2%Z with (1 + 1)%Z by easy.
    now rewrite Z.mul_add_distr_r, Z.mul_1_l, !Z.add_assoc.
  easy.
destruct z as [ | p | p];[easy |  | ].
  now unfold Z.iter; rewrite iter_step, Z.add_0_r.
now unfold Z.iter; rewrite iter_step, Z.add_0_r.
Qed.

Lemma mon_add_correct (l : list Z) z m p :
  p_interp Zrfuns l (mon_add z m p) =
  (z_interp Zrfuns z *
    fold_right (fun '(v, k) b => Zpower_nat (nth v l 0) k * b) 1 m +
    p_interp Zrfuns l p)%Z.
Proof.
induction p as [ | [c m'] p' Ip].
  easy.
simpl.
destruct (monomial_cmp m m') eqn:cmp.
    rewrite (cmp_eq _ _ cmp).
    destruct (z + c =? 0)%Z eqn:at0.
      rewrite Z.add_assoc, <- Z.mul_add_distr_r, !z_interp_correct.
      now rewrite (proj1 (Z.eqb_eq _ _) at0), Z.mul_0_l, Z.add_0_l.
    simpl.
    now rewrite Z.add_assoc, <- Z.mul_add_distr_r, !z_interp_correct.
  now simpl; rewrite Z.add_assoc.
now simpl; rewrite Ip, !Z.add_assoc, !z_interp_correct, (Z.add_comm (c * _)).
Qed.

Lemma poly_add_correct (l : list Z) p1 p2 :
  p_interp Zrfuns l (poly_add p1 p2) =
  (p_interp Zrfuns l p1 + p_interp Zrfuns l p2)%Z.
Proof.
induction p1 as [ | [z mono] p1' Ip1].
  easy.
simpl.
change (p_interp Zrfuns l ((z, mono) :: p1')) with
  (z_interp Zrfuns z *
   fold_right (fun '(v, k) b =>
                (Zpower_nat (nth v l 0%Z) k) * b) 1%Z mono +
    p_interp Zrfuns l p1')%Z.
now rewrite mon_add_correct, Ip1, !Z.add_assoc.
Qed.

Lemma insert_correct  n k l m :
  fold_right (fun '(v, k) b => Zpower_nat (nth v l 0) k * b)%Z 1%Z (insert n k m) =
  (Zpower_nat (nth n l 0%Z) k * fold_right  (fun '(v, k) b => Zpower_nat (nth v l 0) k * b)%Z 1 m)%Z.
Proof.
induction m as [ | [n' k'] m Ih].
  easy.
simpl.
destruct (n <? n') eqn:cmp1.
  easy.
destruct (n =? n') eqn:cmp2.
  assert (nn' : n = n') by now rewrite <- Nat.eqb_eq.
  now simpl; rewrite Zpower_nat_is_exp, Z.mul_assoc, nn'.
now simpl; rewrite Ih, !Z.mul_assoc, (Z.mul_comm (Zpower_nat _ _)).
Qed.

Lemma mon_mul_correct l z m p :
  p_interp Zrfuns l (mon_mul z m p) =
  (z * fold_right (fun '(v, k) b => Zpower_nat (nth v l 0) k * b) 1 m *
  p_interp Zrfuns l p)%Z.
Proof.
induction p as [ | [c m'] p' Ih].
  now simpl; rewrite Z.mul_0_r.
simpl.
rewrite !z_interp_correct, Ih; clear Ih.
rewrite Z.mul_add_distr_l.
rewrite !(Z.add_comm _ (_ * _ * p_interp Zrfuns l p')).
apply f_equal.
induction m' as [ | [n k] m' Ih].
  now simpl; rewrite Z.mul_1_r, Z.mul_shuffle0.
simpl.
rewrite insert_correct, !Z.mul_assoc. 
rewrite !(Z.mul_shuffle0 _ (Zpower_nat _ _)), Ih.
now rewrite !Z.mul_assoc.
Qed.

Lemma poly_mul_correct (l : list Z) p1 p2 :
  p_interp Zrfuns l (poly_mul p1 p2) =
  (p_interp Zrfuns l p1 * p_interp Zrfuns l p2)%Z.
Proof.
induction p1 as [ | [z m] p1' Ih].
  easy.
simpl.
rewrite poly_add_correct; simpl.
rewrite Z.mul_add_distr_r.
rewrite Ih.
rewrite !z_interp_correct, !(Z.add_comm _ (p_interp Zrfuns l p1' * _)); apply f_equal.
now rewrite mon_mul_correct.
Qed.

Lemma to_poly_correct (l : list Z) e :
 e_interp Zrfuns l e = p_interp Zrfuns l (to_poly e).
induction e as [x Ix y Iy | x Ix y Iy | x Ix | x Ix n | z | v].
- now simpl; rewrite Ix, Iy, poly_add_correct.
- now simpl; rewrite Ix, Iy, poly_mul_correct.
- simpl; rewrite Ix; clear Ix.
  induction (to_poly x) as [ | [z m] p Ip]; [easy | simpl; rewrite <-Ip].
  now rewrite !z_interp_correct, Z.opp_add_distr, <- Z.mul_opp_l.
- simpl; rewrite Ix; clear Ix.
  induction n as [ | n Ih];[easy | simpl; rewrite poly_mul_correct, Ih; easy ].
- now simpl; rewrite Z.mul_1_r, Z.add_0_r.
- now cbn -[Z.add]; rewrite !Z.add_0_r, !Z.mul_1_r, Z.mul_1_l.
Qed.

Require Import Ltac2.Ltac2.

Ltac2 Type env_and_val := { env : constr; val : constr}.

Ltac2 rec from_env (env : constr) (t : constr) :=
match! env with
| nil => {env := constr:(cons $t nil); val := constr:(O)}
| ?a :: ?tl => 
  match Constr.equal a t with
  | true => {env := env; val := constr:(O)}
  | false => let eav := from_env tl t in
             let other_env := eav.(env) in
             let i := eav.(val) in
             {env := constr:($a :: $other_env); val := constr:(S $i)}
  end
end.


Ltac2 rec groundZ (c : constr) :=
  match! c with
  | Zpos ?p => groundZ p
  | Zneg ?p => groundZ p
  | Z0 => true
  | xI ?p => groundZ p
  | xO ?p => groundZ p
  | xH => true
  | _ => false
  end.

Ltac2 rec z_term_to_expr (l : constr) (term : constr) :=
match! term with
| ?bin_op ?x ?y =>
  match Constr.equal bin_op constr:(Z.add) with
  | true => 
    let v1 := z_term_to_expr l x in
    let val1 := v1.(val)in
    let v2 := z_term_to_expr (v1.(env)) y in
    let val2 := v2.(val) in
    {env := v2.(env); val := constr:(add $val1 $val2)}
  | false =>
    match Constr.equal bin_op constr:(Z.mul) with
    | true => 
      let v1 := z_term_to_expr l x in
      let val1 := v1.(val) in
      let v2 := z_term_to_expr (v1.(env)) y in
      let val2 := v2.(val) in
      {env := v2.(env); val := constr:(mul $val1 $val2)}
    | false =>
      match Constr.equal bin_op constr:(Zpower_nat) with
      | true => 
        let v1 := z_term_to_expr l x in
        let val1 := v1.(val) in
        {env := v1.(env); val := constr:(exp $val1 $y)}
      | false =>
        match Constr.equal bin_op constr:(Z.sub) with
        | true => 
          let v1 := z_term_to_expr l x in
          let val1 := v1.(val) in
          let v2 := z_term_to_expr (v1.(env)) y in
          let val2 := v2.(val) in
           {env := v2.(env); val := constr:(add $val1 (opp $val2))}
        | false =>
          Control.zero Match_failure
        end
      end
    end
  end
| ?mon_op ?x =>
  match Constr.equal mon_op constr:(Z.opp) with
  | true =>
        let v1 := z_term_to_expr l x in
        let val1 := v1.(val) in
        {env := v1.(env); val := constr:(opp $val1)}
  | false =>
    Control.zero Match_failure
  end
| _ =>
    match groundZ term with
    | true => {env := l; val := constr:(cst $term)}
    | false => 
      let new_env_and_index := from_env l term in
      let index := new_env_and_index.(val) in
      {env := new_env_and_index.(env); val := constr:(var $index)}
    end
end.

Ltac2 Eval z_term_to_expr constr:((1 :: nil)%Z) constr:((2 + 3)%Z).

Ltac2 pre_ring () := 
match! goal with
| [ |- ?x = ?y ] =>
  let ev := z_term_to_expr constr:(@nil Z) x in
  let valx := ev.(val) in
  let ev2 := z_term_to_expr (ev.(env)) y in
  let the_env := ev2.(env) in
  let valy := ev2.(val) in
  change (e_interp Zrfuns $the_env $valx =
          e_interp Zrfuns $the_env $valy)

end.

Lemma tutu : 
  forall x y z, (y + Zpower_nat (x + z) 2 - y =
                 x * x + 2 * x * z  + Zpower_nat z 2)%Z.
Proof.
intros x y z.
pre_ring().
rewrite 2!to_poly_correct.
apply f_equal.
 (* to show that the statement at hand is now symbolic and does
    really depend on x y or z. *)
clear x y z.
cbn.
reflexivity.
Qed.
