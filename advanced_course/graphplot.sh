#!/bin/bash
echo "\\begin{tabular}{|c|c|c|} \\hline number of elements & with lists (ms) & with tries (ms) \\\\ \\hline" > times_table.tex
perl -ne 'if (/([^,]*),([^,]*),([^,]*)/) {print "$1 & $2 & $3 \\\\ \\hline\n"};' times.csv >> times_table.tex
echo "\\end{tabular}" >> times_table.tex

gnuplot -p -e "set terminal svg" -e "set output 'timechart.svg'" -e "plot 'times.csv' using 1:2 with linespoints title 'using lists', 'times.csv' using 1:3 with linespoints title 'using tries'"

