#!/usr/bin/bash
cat /dev/null > times.csv
A=$(mktemp)
for i in 500 1000 1500 2000 2500 3000;
do 
echo -n "$i, " >> times.csv
echo "Time Compute let v := $i in (v, length (inter (firstn (Z.to_nat v) nums) (firstn (Z.to_nat v) nums2))). " | coqtop -load-vernac-source finitesets.v 2>/dev/null | tail -3 > $A
perl -ne 'if(/Finished transaction in .*secs \(([^u]*)u/) {print "$1, "}' $A >> times.csv
( echo "Definition v := $i." \
  " Definition t1 := Eval vm_compute in fold_right (fun e m => add e true m) (empty bool) (firstn (Z.to_nat v) pnums)."  \
  " Definition t2 := Eval vm_compute in fold_right (fun e m => add e true m) (empty bool) (firstn (Z.to_nat v) pnums2)." \
  "Time Compute (v, cardinal (interm t1 t2)). " 
) | coqtop -load-vernac-source finitesets.v 2>/dev/null |tail -3 > $A
perl -ne 'if(/Finished transaction in .*secs \(([^u]*)u/) {print "$1\n"}' $A >> times.csv
rm $A
done
