\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{alltt}
\usepackage{color}
\definecolor{ttblue}{RGB}{48,48,170}
\setbeamertemplate{footline}[frame number]
\title{Dependent types}
\author{Yves Bertot}
\date{December 2020}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Objectives}
\begin{itemize}
\item Functions with dependent types
\item Constructing proofs by function composition
\item Dependently typed pattern-matching
\item Dependent inductive types
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Typing rules for functions: advanced case}
\begin{itemize}
\item If function \(f\) has type \(\forall x : A, B\)
\item If value \(e\) has type \(A\)
\item Then \(f~e\) is well-formed
\item The result has type \(B [x\leftarrow e]\)
\item This means the function can return values in a different type for each input
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Advanced typing illustration}
\begin{alltt}
Require Import ZArith Lia.

Open Scope Z_scope.

Definition target : Z -> Type :=
  fun x => match x with 0 => bool | _ => Z end.

Definition g : \textcolor{red}{forall x : Z, target x} :=
  fun x => match x with 0 => true | _ => x end.

Check g 2.
\textcolor{ttblue}{g 2 : target 2}
Compute g 2.
\textcolor{ttblue}{= 2 : target 2}
Compute g 0.
\textcolor{ttblue}{= true : target 0}
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Illustration with proofs}
\begin{alltt}
Check Z.le_refl.
\textcolor{ttblue}{Z.le_refl : forall n : Z, n <= n}.

Lemma Z_le_add1 n m : n <= m -> n <= m + 1.
Proof. intros; lia. Qed.

Check Z.le_refl 3.
\textcolor{ttblue}{Z.le_refl 3 : 3 <= 3}

Check (Z_le_add1 3 3 (Z.le_refl 3 : \textcolor{red}{3 <= 3}) : \textcolor{red}{3 <= 4}).
\textcolor{ttblue}{le_S 3 3 (le_n 3 : 3 <= 3) : 3 <= 4}
\end{alltt}
\begin{itemize}
\item {\tt Z\_le\_add1 3 3 (Z.le\_refl 3)} is a proof of {\tt 3 <= 4}
\item How do you construct a proof of {\tt 3 <= 5}?
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Illustration with bounded integers}
\begin{itemize}
\item Coq provides a notation {\tt \{x~:~A | P x\}}\\ for {\em the type of
elements of {\tt A} that satisfy {\tt P}}
\item This is call a {\em sigma-type}
\item The constructor is named {\tt exist}\\
{\tt exist~:~forall \{A~:~Type\} (P~:~A -> Prop)}\\
{\tt~~~~~~~~ (e~:~A), P e -> \{x~:~A | P x\}}
\item There is a projection {\tt proj1\_sig} that returns the value in {\tt A}
\item For {\tt P} we can choose the property: the number is smaller than
a given bound.
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Defining bounded integers with a sigma type}
\begin{alltt}
Definition upper_bounded (k : Z) := \{x : Z | x < k\}.

Definition mkub \{k : Z\} (n : Z) (prf : n < k)
    : upper_bounded k :=
   exist _ n prf.

Definition toZ \{k : Z\} (n : upper_bounded k) : Z :=
  proj1_sig n.

Coercion toZ : upper_bounded >-> Z.
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Explanations for {\tt upper\_bound}, {\tt mkub}, and {\tt toZ}}
\begin{itemize}
\item {\tt upper\_bounded} describes integers that are guaranteed to be smaller than a given bound
\item {\tt mkub} is the function that produces elements in this datatype
\item The first argument of {\tt mkub} is the upper bound (but it is made {\em implicit} so that we will seldom write it)
\item The secong argument of {\tt mkub} is an integer
\item The third argument is a proof
\item The function {\tt toZ} can be used to insert a bounded integer where a plain integer is needed
\item {\tt toZ} is declared as a coercion, so that it will be inserted when needed
\item Users will have the illusion that bounded integers can be used as integers
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Making a default element of type {\tt upper\_bounded}}
\begin{alltt}
Lemma Zm1lt x : x - 1 < x. Proof. lia. Qed.

Definition maxub (k : Z) : upper_bounded k :=
  mkub (k - 1) (Zm1lt k).

Check maxub.
\textcolor{ttblue}{maxub : forall k, upper_bounded k.}

Check maxub 3.
\textcolor{ttblue}{maxub 3 : upper_bounded 3}

Compute maxub 3 + 2.
\textcolor{ttblue}{ = 4 : Z}
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Converting an arbitrary integer into a bounded one}
\begin{itemize}
\item {\tt maxub} does not call any testing function
\begin{itemize}
\item But the value is fixed by the type
\end{itemize}
\item When programming we can check that a property is satisfied
\item Illustration requires more advanced programming concepts
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Knowledge from testing : problem exposition}
\begin{alltt}
Fail Definition test_mkub (k x : Z) : upper_bounded k :=
  if x <? k then mkub x \textcolor{red}{true} else maxub k.
\textcolor{blue}{In environment
\textcolor{red}{k, x : Z}
The term "true" has type "bool" while it is expected
to have type "x < ?k"}
\end{alltt}
\begin{itemize}
\item No information deducted from the position wrt {\tt if-then-else}
\item Need to find a wy that environment also contains some hypothesis {\tt x~<?~k = true}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Functions as proofs}
\begin{itemize}
\item Function code can also be constructed by tactics
\item Applying a function is expressed by the tactic {\tt apply}
\item Writing a {\tt fun ... => ...} is expressed by {\tt intros}
\item Writing a {\tt match} statement is done by {\tt destruct}
\item Writing a {\tt match} with extra equality hypotheses is done by {\tt destruct ... eqn:...}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example building {\tt test\_mkub} as a proof}
\begin{alltt}
Definition test_mkub (k n : Z) : upper_bounded k.
destruct (n <? k) eqn:h.
  \textcolor{red}{(* case where h has type n <? k = true *)}
  apply (mkub n).
  apply Z.ltb_lt.
  exact h.
\textcolor{red}{(* case where h has type n <? k = false *)}
apply maxub.
Defined.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{The inspect pattern}
\begin{alltt}
Definition inspect \{A : Type\}(a : A) : \{b : A | a = b\} :=
  exist (fun b' => a = b') a eq_refl.
\end{alltt}
\begin{itemize}
\item The {\tt inspect} function gives two names to the same value
\item The equality links the two names
\item In pattern matching, we can modify one and keep the other untouched
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Inspect use case}
\begin{alltt}
Definition test_mkub2 (k x : Z) : upper_bounded k :=
  match inspect (x <? k) with
  | exist _ true h =>
    \textcolor{red}{(* here h is a proof that x <? k = true *)}
    mkub x (proj1 (Z.ltb_lt x k) \textcolor{red}{h})
  | exist _ false _ => maxub k
  end.
\end{alltt}
\begin{itemize}
\item A match on {\tt inspect \(e\)} is like a match on \(e\)
\item Produces an extra hypothesis expressing what computation was performed
\item Pattern matching on the first explicit argument of {\tt exist} has two
effects
\begin{itemize}
\item Two rules are produced (two constructors for {\tt bool})
\item The type of {\tt h} is different in each branch
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Dependent pattern-matching on natural numbers}
\begin{alltt}
Definition nat_dep_case (P : nat -> Prop) (h0 : P O)
  (hs : forall k, P (S k)) (n : nat) : P n :=
  match n with
  | O => h0 : P O
  | S p => hs p : P (S p)
  end.
\end{alltt}
\begin{itemize}
\item The expected type dependent on the matched expression
\item Coq exects a different type for each branch
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Dependent pattern-matching with recursion}
\begin{alltt}
Fixpoint nat_dep_rec (P : nat -> Prop) (h0 : P O)
  (hs : forall k, P k -> P (S k)) (n : nat) : P n :=
  match n with
  | O => h0
  | S p => hs p (nat_dep_rec P h0 hs p : \textcolor{red}{P p}) : \textcolor{red}{P (S p)}
  end.
\end{alltt}
\begin{itemize}
\item The recursive call does not belong to the same type
\item From the point of view of logic: induction principle on natural numbers
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{recursive inductive dependent type}
\begin{alltt}
Inductive t2 : nat -> Type :=
| start2 : t2 O
| step2 : forall n, t2 n -> t2 (S (S n)).
\end{alltt}
\begin{itemize}
\item Type {\tt t2 \(n\)} has an element exactly when \(n\) is even
\item Many interesting concepts can be defined this way
\item Dependent pattern matching also works here
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Dependent singleton type}
\begin{alltt}
Inductive singleton3 : Z -> Prop :=
  is_3 : singleton3 3.

Definition singleton3_dep (x : Z) (prf : singleton3 x) 
  (P : Z -> Prop) (p3 : P 3) : P x :=
  match prf with is_3 => p3 end.
\end{alltt}
\begin{itemize}
\item {\tt singleton3} is satisfied only once.
\item In pattern matching rule, we used the fact that the constructor is
explicitely used for 3
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Parameterized singleton type}
\begin{alltt}
Inductive singleton \{A : Type\} (x : A) : A -> Prop :=
  single : singleton x x.

Definition singleton_dep
   \{A : Type\} \{x y : A\} (prf : singleton x y)
   (P : A -> Prop) (px : P x)  : P y :=
  match prf in singleton _ b return P b with
  | single _ => px
  end.  
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Singleton type as equality}
\begin{alltt}
Check single.
\textcolor{blue}{single : forall x : ?A, singleton x x}

Check singleton_dep.
\textcolor{blue}{singleton_dep
     : singleton ?x ?y -> forall P : ?A ->
       Prop, P ?x -> P ?y}
\end{alltt}
\begin{itemize}
\item {\tt singleton x y} really means {\tt x = y}
\item {\tt apply singleton\_dep H } really means {\tt rewrite <- H}
\item This teaches us that inductive types are at the foundation Coq
\end{itemize}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
