Require Import List Arith Lia.

(* La bibliothèque des listes de Coq fournit deux fonctions pour
  récupérer un élément d'une liste en donnant son rang (le premier élement
  est donné par le rang 0).  Dans les deux cas, il y a un problème si le
  rang donné est supérieur ou égal à la longueur de la liste, par exemple
  dans le cas ou la liste est vide.

  La première des deux fonctions traite le cas problématique en retournant
  une valeur par défaut qui doit être fournie par l'utilisateur.

  La deuxième fonction traite le problème en retournant une valeur dans
  le type option.  Le code de ces deux fonctions est donné par le texte
  suivant.

  Fixpoint nth {A : Type} (n:nat) (l:list A) (default:A) {struct l} : A :=
    match n, l with
      | O, x :: l' => x
      | O, other => default
      | S m, [] => default
      | S m, x :: t => nth m t default
    end.

  Fixpoint nth_error (l:list A) (n:nat) {struct n} : option A :=
    match n, l with
      | O, x :: _ => Some x
      | S n, _ :: l => nth_error l n
      | _, _ => None
    end. *)

Lemma nth_map {A B : Type} (f : A -> B) (a : A) (l : list A) (n : nat) :
  nth n (map f l) (f a) = f (nth n l a).
Proof.  ... Qed.

Lemma nth_error_some_length {A : Type} (l : list A) (n : nat) (v : A):
  nth_error l n = Some v -> n < length l.
Proof.  ... Qed.

Lemma nth_error_some_nth {A : Type} (l : list A) n v e :
  nth_error l n = Some v -> nth n l e = v.
Proof.  ... Qed.

Lemma nth_error_none_length {A : Type} (l : list A) n :
 nth_error l n = None -> length l <= n.
Proof.  ... Qed.

Lemma nth_error_nth {A : Type} (l : list A) n e :
  nth_error l n = None -> nth n l e = e.
Proof. ... Qed.
