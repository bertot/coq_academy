Require Import Arith.

Section playground.

Variables (A : Set) (blue red : A -> Prop) (f g: A -> A) (a0 : A).

Hypothesis blue_f : forall x, red x -> blue x -> blue (f x).

Hypothesis red_g : forall x, red (g x) -> red x.

Hypothesis red_g2_a0 : red (g (g a0)).

Hypothesis blue_a0 : blue a0.

Hypothesis red_f : forall x, red x -> red (f x).

Lemma blue_fa0 : blue (f a0).
Proof.
apply blue_f.
apply red_g.
apply red_g.
apply red_g2_a0.
apply blue_a0.
Qed.

Hint Resolve blue_f red_g red_g2_a0 blue_a0 red_f : colors.

Lemma blue_ffa0' : blue (f (f a0)).
Proof. auto with colors. Qed.

Lemma blue_fffa0' : blue (f (f (f a0))).
Proof.
Fail now auto with colors.
now auto 6 with colors.
Qed.

Ltac repeat_colors :=
repeat (apply blue_f || apply red_f || apply red_g2_a0 ||
        apply red_g || apply blue_a0).

Lemma blue_f8a0' : blue (f (f (f (f (f (f a0)))))).
Proof. repeat_colors. Qed.

Lemma red_n n : red (Nat.iter n f a0).
Proof.
induction n as [ | n IH].
  simpl.
  do 2 apply red_g.
  now apply red_g2_a0.
simpl.
now apply red_f.
Qed.

Lemma blue_n n : blue (Nat.iter n f a0).
Proof.
induction n as [ | p IH].
simpl.
apply blue_a0.
simpl.
apply blue_f.
apply red_n.
assumption.
Qed.

Ltac mk_nat term :=
  match term with
  | f ?x => let v := mk_nat x in constr:(S v)
  | a0 => O
  end.

Ltac reflect_f :=
match goal with
  | |- blue ?x => 
    let v := mk_nat x in 
       change x with (Nat.iter v f a0);
       apply blue_n
end.

Lemma blue_f6a0 : blue (f (f (f (f  (f (f a0)))))).
Proof. exact (blue_n 6). Qed.

Lemma blue_f10a0' : blue (f (f (f (f (f (f (f (f (f (f a0)))))))))).
Proof. reflect_f. Qed.

Print blue_f10a0'.

End playground.
