# Coq courses for Inria Academy

This is material for courses that will be disseminated by Inria Academy.  This
courses should be adapted for an industrial audience (engineers with a variety of
background, probably well-trained in software development, but not in functional
programming).

The material should fall into three main categories

1. Basic course, to be given in one day
2. Advanced course, to be given in two days
3. Expert course, to be given in two days.

The whole package: basic+advanced+expert could be given in a week for companies
that would be willing to offer this kind of course to their personnel.

# Basic course outline

1. Basic Type theory

   - algebraic datatypes and pattern-matching
   - bounded arrays and indices and dependent types
   - proofs as tree-like structures

   **slides**: `basic_type_theory.tex`, this also relies on the C programs
   `example.c` and `example1.c`.  This course gives a general explanation for
   algebraic data-types (to avoid dangling pointers) and dependent types
   (to avoid buffer and array overflow).  It also illustrates the use of
   programs that construct proof terms.

2. How to describe algorithms: finite sets and lists

   - Functional programming with typed lists
   - Pattern-matching and recursion
   - An introduction to `Import` and `Search`
   - Basic operations on finite sets

   **slides**: `programming.tex`
   **example file**: `programming.v`

3. Logical expressions and specifications

   - Logical connectives
   - Show the possibility to use tests functions to express specifications
   - Useful equivalences between logical constructs

   **slides**: `properties_of_programs.tex`

4. Proof techniques

   - Goal directed proof
   - Reasoning about logical connectives
   - Reasoning about programming constructs
   - Proofs by induction

   **slides**: `coq_proof_techniques.tex`

5. Modeling imperative programs

   - Simple methods to describe programs that modify a state
   - Monadic approach
   - Reference to Advanced tools like VST and IRIS

   **slides**: `imperative.tex`

6. Extracting from the Coq world

   - Show how Coq code is translated into OCaml
   - Show how polymorphism is handled
   - Show how Proofs are removed

   **slides** : `extraction.tex` (in French)

7. A panorama of flagship applications
   - CompCert
   - SiFive
   - JavaCard
   - Verdi
   - CertiKOS
   - Fiat-Crypto
   - Feit-Thompson

   **slides** : `successes.tex` (in French)

# advanced course outline

1. Recursive programming
   - examples of inductive types
   - how to rely on Search to know what functions do
   - Coq lab sessions, testing the code with `Compute`

2. Logical formulas
   - using Venn diagramms (potatoes?)
   - discussion of the distinction between bool and Prop, the `is_true` coercion

3. Proof techniques
   - Basic logic: a table of connectives and tactics
        `destruct`, `split`, `left`, `right`, `exact`, `assumption`, `assert`
   - program proving : `unfold`, `simpl`, `induction`, `discriminate`,
         `injection`, `change`
   - algebra reasoning: `rewrite`
   - How to rely on `Search` to find relevant theorems

4. Arithmetic and automated proofs
   - `ring`, `lia`, `nia`, how to know about their domain of applicability
   - `auto`, how to program it
   - first_order

5. Dependent types
   - sigma-types, etc.
   - dependent pattern-matching
   - extraction

# expert courses

1. General recursion
2. Real number arithmetic and floating point numbers
3. packaging large theories, the math-comp approach
4. Describing programming language and tools

# master class
  - pattern matching as a way to avoid bugs in pointer manipulation
  - dependent typing as a way to avoid array overflow
  - Proofs as programs
  - Goal directed proof
  - Software production, extraction
  - Success stories

  **slides**: `master-chile.tex`  To compile, symbolic links must be placed
  towards illustrations that appear in directories `drawings` and
  `basic_course`.  Compilation must be run in these directories beforehand.