Lemma curry_and  (A B C : Prop) :
      (A -> B -> C) <-> ((A /\ B) -> C).
Proof.
tauto.
Qed.

Lemma use_negated_disjunct (A B : Prop) :
  ~(A \/ B) <-> ~A /\ ~B.
Proof.
tauto.
Qed.


Lemma use_disjunct_negations (A B : Prop) :
  (~ A \/ ~ B) -> ~(A /\ B).
Proof.
tauto.
Qed.

Example no_negated_conjunct (A B : Prop) :
  ~(A /\ B) -> ~A \/ ~B.
Proof.
Fail tauto.
Abort.

Lemma curry_exists (T : Type) (P : T -> Prop) (C : Prop) :
 (forall x, P x -> C) <-> ((exists x, P x) -> C).
Proof.
now firstorder.
Qed.

Lemma negated_exist (T : Type)(P : T -> Prop) :
  ~(exists x, P x) <-> (forall x, ~ P x).
Proof.
now firstorder.
Qed.

Lemma use_existential_negation (T : Type) (P : T -> Prop) :
  (exists x, ~ P x) -> ~(forall x, P x).
Proof.
now firstorder.
Qed.

Example no_negated_universal_quantification
  (T : Prop) (P : T -> Prop) :
  ~(forall x, P x) -> exists x, ~ P x.
Proof.
Fail now firstorder.
Abort.
