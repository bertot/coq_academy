Require Import Reals Interval.Tactic Coquelicot.Coquelicot.

Open Scope R_scope.

(* This tactic is meant to be used in a "tactic in term" fashion. *)
Ltac binary_approx e :=
 let binary_result := fresh "binary_result" in
 (interval_intro e as binary_result);
  let v := type of binary_result in
  (* the tactic displays the interval that will be saved. *)
  idtac v;
  exact binary_result.

Example tata x (intx : 0.4 <= x <= 0.5) :=
 ltac:(binary_approx (sin x)).

(* This tactic proves a decimal approximation with n significant
  digits for the expression e.  n has to be a natural number.
  This is also meant to be use as a "tactic in term" *)
Ltac decimal_approx n e :=
 let binary_result := fresh "binary_result" in
 (interval_intro e as binary_result)
 ;
 match type of binary_result with
  (Rdiv (IZR ?a) (IZR ?b) <= _ <=
   Rdiv (IZR ?c) (IZR ?d)) =>
  let v := eval compute in ((a * 10 ^ (Z.of_nat n)) / b)%Z in
  let w := eval compute in ((c * 10 ^ (Z.of_nat n)) / d + 1)%Z in
  let v':= constr:(Rdiv (IZR v) (10 ^ n)) in
  let w':= constr:(Rdiv (IZR w) (10 ^ n)) in
  assert (decimal_result : v' <= e <= w') by interval with (i_prec 53);
  idtac v' "<=" e "<=" w';
  exact decimal_result
  end.

(* The following illustrates the use of decimal_approx for
  sin x, where x is in interval [0.4, 0.5] *)
Example tutu (x : R) (intx : 0.4 <= x <= 0.5) : _ :=
ltac:(decimal_approx 6%nat(sin x)).

(* The results are not satisfactory if x is given inside a strict interval. *)
(* It might be interesting to fine-tune the tactic decimal_approx so that
  the constants will be displayed as 0.389418 and 0.479426 *)
