Require Import List.

(* Two lists are equal when:
   1/ they have the same length
   2/ the function nth returns the same result on both lists
      for any index smaller than the length of both lists

   State a prove a theorem expressing this. *)

