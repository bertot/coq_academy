Require Import ZArith List Bool Lia.

Open Scope Z_scope.

Fixpoint sorted {A : Type} (cmp : A -> A -> bool)
  (l : list A) : bool :=
match l with
  nil => true
| a :: tl =>
    match tl with
    | nil => true
    | b :: _ => cmp a b && sorted cmp tl
    end
end.

Definition sorting_function_first_prop {A : Type}
  (cmp : A -> A -> bool) (f : list A -> list A) :=
  forall l : list A, sorted cmp (f l) = true.

(* Example with a simple sort algorithm. *)

Fixpoint insert (x : Z) (l : list Z) :=
  match l with
  | nil => x :: nil
  | y :: tl => if (x <=? y) then x :: l else y :: insert x tl
  end.

Fixpoint sort (l : list Z) :=
  match l with
  | nil => nil
  | x :: tl => insert x (sort tl)
  end.

Lemma insert_sorted x l : sorted Z.leb l = true -> 
   sorted Z.leb (insert x l) = true.
Proof.
induction l as [ | y l IH].
  easy.
destruct l as [ | z l].
  simpl.
  destruct (x <=? y) eqn: cmpxy.
    now simpl; rewrite cmpxy.
  simpl; enough (cmpyx : y <=? x = true) by now rewrite cmpyx.
  apply Z.leb_le.
  apply Z.lt_le_incl.
  apply Z.leb_gt.
  exact cmpxy.
intros yzl.
assert (y <=?z = true /\ sorted Z.leb (z :: l) = true) as [cmpyz zl].
  rewrite <- andb_true_iff.
  exact yzl.
change (insert x (y :: z :: l)) with
       (if (x <=? y) then x :: y :: z :: l
       else y :: insert x (z :: l)).
  destruct (x <=? y) eqn:cmpxy.
  simpl; rewrite andb_true_iff; split.
    easy.
  exact yzl.
change (insert x (z :: l)) with 
       (if (x <=? z) then x :: z :: l
       else z :: insert x l).
assert (cmpyx: y <=? x = true).
  now apply Z.leb_le; apply Z.lt_le_incl, Z.leb_gt.
destruct (x <=? z) eqn: cmpxz.
  simpl.
  rewrite cmpyx, cmpxz; simpl; exact zl.
simpl.
rewrite cmpyz; simpl.
change (sorted Z.leb (z :: insert x l) = true).
assert (IH' : sorted Z.leb (insert x (z :: l)) = true).
  now apply IH.
simpl in IH'.
rewrite cmpxz in IH'.
exact IH'.
Qed.

Lemma sort_sorted : sorting_function_first_prop Z.leb sort.
Proof.
intros l; induction l as [ | x l IH].
  easy.
now simpl; apply insert_sorted.
Qed.

