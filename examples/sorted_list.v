From Coq Require Import Bool List Mergesort Orders Sorting ZArith.

From Coq Require Extraction.

(* Linear congruential generator. This is used to produce a low quality
  sequence of random numbers. *)

Definition lcg_step (modulus a c seed : Z) := ((a * seed + c) mod modulus)%Z.

Definition iterates (modulus a c seed len : Z) :=
 Z.iter len
   (fun p : list Z * Z => let acc := fst p in let seed := snd p in
       let seed' := lcg_step modulus a c seed in
       (seed'::acc, seed')) (nil, seed).

(* We settle with a list of 2000 elements, taken between 0 and 2 ^ 32.
  The randomness is quite weak, as they alternate between odd and even
  numbers. *)

(*
Definition rnd_list :=
    Eval compute in fst (iterates (2 ^ 32) 1664525 1013904223 33 2000).

Definition rnd_list2 :=
    Eval compute in fst (iterates (2 ^ 32) 1664525 1013904223 1937 2000).
*)

(* For better randomness, we take only the higher 16 bits.  Then we can
  use a modulo operation to bring this inside a chosen interval. *)
Definition randp16 (rand : list Z) :=
  let p16 := (2 ^ 16)%Z in map (fun z => (z / p16)%Z) rand.

(*
Definition nums := Eval compute in randp16 rnd_list.

Definition nums2 := Eval compute in randp16 rnd_list2.
*)

Definition ZtoPos z :=
  match z with Z.pos x => x | Z.neg x => x | Z0 => 1%positive end.

(*
Definition pnums := map ZtoPos nums.

Definition pnums2 := map ZtoPos nums2.
*)

Module ZOrder <: TotalLeBool.
  Definition t := Z.
  Definition leb := Z.leb.

Theorem leb_total : forall x y, leb x y = true \/ leb y x = true.
Proof.
now intros x y; case (Zle_bool_total x y); auto.
Qed.

End ZOrder.

Module Import ZSort := Sort ZOrder.

(*
Compute sort nums.
*)

Fixpoint adjacent {A : Type} (l : list A) :=
  match l with
  | nil => nil
  | _ :: nil => nil
  | a :: (b :: _ as tl) => (a,b) :: adjacent tl
  end.

(*
Compute adjacent (sort nums).

Compute length (filter (fun p => (fst p =? snd p)%Z) (adjacent (sort nums))).

Compute existsb (fun x => (x =? 0)%Z) nums.

Compute existsb (fun x => (x =? 1)%positive) pnums.
*)

Definition Inf := HdRel Z.lt.

Definition ZSorted : list Z -> Prop := Sorted Z.lt.

Definition set := {l : list Z | ZSorted l}.

Definition empty : set := exist _ nil (Sorted_nil Z.lt).

Definition cardinal (s : set) : nat := length (proj1_sig s).

Lemma lt_ZSorted (x y : Z) (l : list Z) :
  Z.lt x y -> ZSorted (y :: l) -> ZSorted (x :: l).
Proof.
intro Hlt; elim l; clear l; [constructor; auto|].
intros a l IHl Hl; constructor; [apply Sorted_inv in Hl; tauto|].
constructor; apply Z.lt_trans with y; [auto|clear Hlt].
apply Sorted_inv in Hl; destruct Hl as [_ Hlt]; apply HdRel_inv in Hlt; auto.
Qed.

Fixpoint mem_aux (x : Z) (l : list Z) :=
  match l with
    | nil    => false
    | y :: l' =>
        match Z.compare x y with
          | Lt => false
          | Eq => true
          | Gt => mem_aux x l'
        end
  end.

Definition mem (x : Z) (s : set) := mem_aux x (proj1_sig s).

Fixpoint add_aux (x : Z) (l : list Z) : list Z :=
  match l with
    | nil    => x :: nil
    | y :: l' =>
        match Z.compare x y with
          | Lt => x :: l
          | Eq => l
          | Gt => y :: add_aux x l'
        end
  end.

Lemma add_inf : forall (l : list Z) (x a : Z),
  Inf a l -> Z.lt a x -> Inf a (add_aux x l).
Proof.
simple induction l; clear l; simpl; [intros; apply HdRel_cons; auto|].
intros a2 l Hl x a1 HInf Hlt; apply HdRel_inv in HInf.
elim (Z.compare x a2); apply HdRel_cons; auto.
Qed.

Lemma add_ok : forall (l : list Z) (x : Z) `(ZSorted l), ZSorted (add_aux x l).
Proof.
simple induction l; simpl; clear l;
[intros; apply Sorted_cons; [apply Sorted_nil|apply HdRel_nil]|].
intros a l Hl x HS; elim (Z.compare_spec x a); intro;
[auto|apply Sorted_cons; [|apply HdRel_cons]; auto|].
apply Sorted_cons; [|apply Sorted_inv in HS; apply add_inf; tauto].
apply Hl; apply Sorted_inv in HS; tauto.
Qed.

Definition add (x : Z) (s : set) : set :=
  exist _ (add_aux x (proj1_sig s)) (add_ok (proj1_sig s) x (proj2_sig s)).

Inductive In_list (x : Z) : list Z -> Prop :=
  | In_cons_hd : forall y l, x = y -> In_list x (y :: l)
  | In_cons_tl : forall y l, In_list x l -> In_list x (y :: l).

Lemma In_cons (x y : Z) (l : list Z) :
  In_list x (y :: l) <-> eq x y \/ In_list x l.
Proof.
split; [intro; invlist In_list; auto|].
intros [Heq|HIn]; [apply In_cons_hd|apply In_cons_tl]; auto.
Qed.

Lemma mem_aux_iff : forall (l : list Z) (x : Z) (Hl : ZSorted l),
  mem_aux x l = true <-> In_list x l.
Proof.
intro l; elim l; clear l; [intuition; [discriminate|invlist In_list]|].
intros a l IHl x Hl; simpl; destruct (Z.compare_spec x a) as [|L|G];
rewrite In_cons; [intuition| |split; [right|intros [|]]];
[..|exfalso; apply (Z.lt_irrefl a); subst; auto|];
[|apply IHl; auto; eapply Sorted_inv; eauto..].
split; [intro; discriminate|intros [|HIn]];
[exfalso; apply (Z.lt_irrefl a); subst; auto|].
exfalso; assert (Z.lt a x); [|apply (Z.lt_irrefl a), Z.lt_trans with x; auto].
apply Sorted_extends in Hl; [|intro; apply Z.lt_trans].
eapply Forall_forall; eauto; clear dependent a; clear IHl; revert dependent x.
elim l; clear l; [simpl; intros; invlist In_list|intros a l IHl x HIn].
apply In_cons in HIn; destruct HIn as [HEq|HIn]; [left; auto|right; auto].
Qed.

Lemma add_aux_spec : forall (l : list Z) (x y : Z) (Hl : ZSorted l),
  In_list y (add_aux x l) <-> eq x y \/ In_list y l.
Proof.
intro l; elim l; clear l; simpl;
[intuition; invlist In_list; rewrite ?In_cons; auto|intros a l IHl x y Hl].
simpl; elim (Z.compare_spec x a); intro; rewrite !In_cons, ?IHl;
[split; [intros [|]; auto|]|intuition..|eapply Sorted_inv; eauto].
intros [HE|[|]]; [rewrite <- HE|..]; auto.
Qed.

Lemma mem_aux_cons (x a : Z) (l : list Z) (Hl : ZSorted l) :
  mem_aux x (add_aux a l) = Z.eqb x a || mem_aux x l.
Proof.
generalize (mem_aux_iff (add_aux a l) x (add_ok l a Hl)) (mem_aux_iff l x Hl).
generalize (add_aux_spec l a x Hl) (Z.eqb_eq x a).
case (Z.eqb x a), (mem_aux x l), (mem_aux x (add_aux a l)); intuition.
simpl; apply eq_sym; intuition.
Qed.

Lemma mem_cons (x a : Z) (s : set) : mem x (add a s) = Z.eqb x a || mem x s.
Proof. destruct s; unfold add, mem; simpl; apply mem_aux_cons; auto. Qed.

Fixpoint filter_aux {A : Set} (f : A -> bool) l :=
  match l with
    | nil     => nil
    | x :: l' => if f x then x :: filter_aux f l' else filter_aux f l'
  end.

Lemma filter_ok f l : forall `(ZSorted l), ZSorted (filter_aux f l).
Proof.
elim l; [simpl; auto|clear l; intros x l IHl].
simpl; case (f x); intro Hl; [|apply IHl; eapply Sorted_inv; eauto].
constructor; [apply IHl; eapply Sorted_inv; eauto|clear IHl; revert Hl].
elim l; clear l; [simpl; intros; apply HdRel_nil|intros y l IHl].
simpl; case (f y); intro; [constructor|apply IHl; clear IHl];
[apply Sorted_inv in Hl; destruct Hl as [_ Hlt]; apply HdRel_inv in Hlt; auto|].
apply Sorted_inv in Hl; destruct Hl as [Hl Hlt]; apply HdRel_inv in Hlt.
apply lt_ZSorted with y; auto.
Qed.

Definition filter (f : Z -> bool) (s : set) : set :=
  exist _ (filter_aux f (proj1_sig s))
        (filter_ok f (proj1_sig s) (proj2_sig s)).

Lemma filter_aux_spec : forall (l : list Z) (x : Z) (f : Z -> bool),
  (In_list x (filter_aux f l) <-> In_list x l /\ f x = true).
Proof.
intro l; elim l; clear l; simpl; [intros; split; intuition; invlist In_list|].
intros a l IHl x f; destruct (f a) eqn:F; rewrite !In_cons, ?IHl; split;
[intros [HEq|[HIn Fx]]; [rewrite HEq|]|intuition..|intros [[HEq|HIn] Fx]]; auto.
rewrite HEq in Fx; rewrite F in Fx; discriminate.
Qed.

Lemma mem_filter (f : Z -> bool) (x : Z) (s : set) :
  mem x (filter f s) = mem x s && f x.
Proof.
destruct s as [l Hl]; unfold mem, filter; simpl.
generalize (mem_aux_iff (filter_aux f l) x (filter_ok f l Hl)).
generalize (mem_aux_iff l x Hl) (filter_aux_spec l x f).
case (f x), (mem_aux x l), (mem_aux x (filter_aux f l)); intuition.
Qed.

Fixpoint inter_aux (l : list Z) : list Z -> list Z :=
  match l with
    | nil    => fun _ => nil
    | x :: l' =>
        (fix inter_aux_aux (l'' : list Z) : list Z :=
           match l'' with
             | nil      => nil
             | x' :: l''' =>
                  match Z.compare x x' with
                    | Lt => inter_aux l' l''
                    | Eq => x :: inter_aux l' l'''
                    | Gt => inter_aux_aux l'''
                  end
           end)
  end.

Lemma inter_ok l l' : forall `(ZSorted l, ZSorted l'), ZSorted (inter_aux l l').
Proof.
revert l'; elim l; clear l; [simpl; auto|].
intros a l IHl l'; elim l'; clear l'; [simpl; auto|intros a' l' IHl' Hl Hl'].
simpl; destruct (Z.compare_spec a a') as [E|L|G]; simpl; [rewrite <- E in *|..].

  {
  constructor; [apply IHl|];[apply Sorted_inv in Hl|apply Sorted_inv in Hl'|];
  [intuition..|]; clear a' E IHl IHl'; revert dependent l'; revert dependent l.
  intro l; elim l; clear l; [simpl; auto|intros x l IHl Hl l'].
  elim l'; clear l'; [simpl; auto|intros x' l' IHl' Hl'].
  simpl; assert (H1 := Hl); assert (H2 := Hl').
  apply Sorted_inv in H1; apply Sorted_inv in H2.
  elim (Z.compare x x'); simpl; intros;
  [constructor; destruct H1 as [_ Hlt]; apply HdRel_inv in Hlt; auto|..];
  [apply IHl|apply IHl']; [|intuition|];
  [apply lt_ZSorted with x|apply lt_ZSorted with x']; try solve [intuition];
  [destruct H1 as [_ Hlt]|destruct H2 as [_ Hlt]]; apply HdRel_inv in Hlt; auto.
  }

  {
  apply IHl; auto;  apply Sorted_inv in Hl; intuition.
  }

  {
  cut (ZSorted (inter_aux (a :: l) l')); [simpl; auto|apply IHl'; auto].
  apply Sorted_inv in Hl'; intuition.
  }
Qed.

Definition inter (s1 s2 : set) : set :=
  exist _ (inter_aux (proj1_sig s1) (proj1_sig s2))
        (inter_ok (proj1_sig s1) (proj1_sig s2) (proj2_sig s1) (proj2_sig s2)).

Lemma In_list_In (x : Z) (l : list Z) : In_list x l <-> In x l.
Proof.
elim l; clear l; simpl; [intuition; invlist In_list|intros a l IHl].
split; [intro HIn; apply In_cons in HIn; intuition|].
intros [HEq|HIn]; apply In_cons; intuition.
Qed.

Lemma inter_aux_spec :
  forall (l l' : list Z) (x : Z) (Hl : ZSorted l) (Hl' : ZSorted l'),
    In_list x (inter_aux l l') <-> In_list x l /\ In_list x l'.
Proof.
intro l; elim l; clear l; simpl; [intuition; invlist In_list|].
intros a l IHl l'; elim l'; clear l'; [intuition; invlist In_list|].
intros a' l' IHl' x Hl Hl'; simpl; assert (Hal := Hl); assert (Ha'l' := Hl').
apply Sorted_inv in Hl; destruct Hl as [Hl _].
apply Sorted_inv in Hl'; destruct Hl' as [Hl' _].
elim (Z.compare_spec a a'); intro HC; rewrite !In_cons, ?IHl, ?IHl'; auto;
[rewrite HC; intuition|split..]; try solve [intuition; apply In_cons; auto];
[intros [[HEq1|HIn1] [HEq2|HIn2]]|intros [[HEq1|HIn1] [HEq2|HIn2]]];
try solve [exfalso; apply (Z.lt_irrefl x); intuition];
try solve [split; try apply In_cons; auto];
exfalso; apply (Z.lt_irrefl a), Z.lt_trans with a'; auto;
[assert (Hlt := Ha'l')|assert (Hlt := Hal)]; apply Sorted_extends in Hlt;
try solve [intro; apply Z.lt_trans];
[cut (forall x, In x l' -> (a' < x)%Z)|cut (forall x, In x l -> (a < x)%Z)];
try solve [intro H; apply H, In_list_In; subst; auto];
apply Forall_forall; auto.
Qed.

Lemma interP (x : Z) (s1 s2 : set) :
  mem x (inter s1 s2) = mem x s1 && mem x s2.
Proof.
destruct s1 as [l1 Hl1]; destruct s2 as [l2 Hl2]; unfold mem, inter; simpl.
generalize (inter_aux_spec l1 l2 x Hl1 Hl2) (mem_aux_iff (inter_aux l1 l2) x).
generalize (mem_aux_iff l1 x) (mem_aux_iff l2 x) (inter_ok l1 l2).
case (mem_aux x l1), (mem_aux x l2), (mem_aux x (inter_aux l1 l2)); intuition.
Qed.

Fixpoint union_aux (l : list Z) : list Z -> list Z :=
  match l with
    | nil    => fun l' => l'
    | x :: l' =>
        (fix union_aux_aux (l'' : list Z) : list Z :=
           match l'' with
             | nil      => l
             | x' :: l''' =>
                  match Z.compare x x' with
                    | Lt => x  :: union_aux l' l''
                    | Eq => x  :: union_aux l' l'''
                    | Gt => x' :: union_aux_aux l'''
                  end
           end)
  end.

Lemma union_inf :
  forall (l l' : list Z) (a : Z) (Hl : ZSorted l) (Hl' : ZSorted l'),
    Inf a l -> Inf a l' -> Inf a (union_aux l l').
Proof.
intro l; elim l; clear l; [simpl; auto|intros x l IHl l'; simpl].
destruct l' as [|x' l']; [simpl; auto|intros a Hl Hl'].
elim (Z.compare_spec x x'); intros; apply HdRel_cons; eapply HdRel_inv; eauto.
Qed.

Lemma union_ok l l' : forall `(ZSorted l, ZSorted l'), ZSorted (union_aux l l').
Proof.
revert l'; elim l; clear l; [simpl; auto|intros x l IHl l'].
elim l'; clear l'; [simpl; auto|intros x' l' IHl' Hl Hl'].
simpl; destruct (Z.compare_spec x x') as [E|L|G]; simpl.

  {
  constructor; [apply IHl|]; [apply Sorted_inv in Hl|apply Sorted_inv in Hl'|];
  [intuition..|apply union_inf; eapply Sorted_inv; eauto].
  rewrite E; auto.
  }

  {
  constructor; [apply IHl|]; [apply Sorted_inv in Hl|..]; [intuition..|].
  apply union_inf;  eapply Sorted_inv; eauto.
  }

  {
  cut (ZSorted (x' :: union_aux (x :: l) l')); [simpl; auto|].
  constructor; [apply IHl'|apply union_inf]; try solve [constructor; auto];
  eapply Sorted_inv; eauto.
  }
Qed.

Definition union (s1 s2 : set) : set :=
  exist _ (union_aux (proj1_sig s1) (proj1_sig s2))
        (union_ok (proj1_sig s1) (proj1_sig s2) (proj2_sig s1) (proj2_sig s2)).

Lemma union_aux_spec :
  forall (l l' : list Z) (x : Z) (Hl : ZSorted l) (Hl' : ZSorted l'),
    In_list x (union_aux l l') <-> In_list x l \/ In_list x l'.
Proof.
intro l; elim l; clear l; simpl; [intuition; invlist In_list|].
intros a l IHl l'; elim l'; clear l'; [intuition; invlist In_list|].
intros a' l' IHl' x Hl Hl'; simpl; elim (Z.compare_spec a a'); intro HC;
rewrite !In_cons, ?IHl, ?IHl'; try solve [eapply Sorted_inv; eauto];
rewrite ?In_cons; intuition.
Qed.

Lemma unionP (x : Z) (s1 s2 : set) :
  mem x (union s1 s2) = mem x s1 || mem x s2.
Proof.
destruct s1 as [l1 Hl1]; destruct s2 as [l2 Hl2]; unfold mem, union; simpl.
generalize (union_aux_spec l1 l2 x Hl1 Hl2) (mem_aux_iff (union_aux l1 l2) x).
generalize (mem_aux_iff l1 x) (mem_aux_iff l2 x) (union_ok l1 l2).
case (mem_aux x l1), (mem_aux x l2), (mem_aux x (union_aux l1 l2)); intuition.
Qed.

(*
Definition strictsort (l : list Z) : list Z := sort (nodup Z.eq_dec l).

Lemma NoDup_Sorted_Stricly_Sorted (l : list Z) :
  NoDup l -> Sorted Z.le l -> Sorted Z.lt l.
Proof.
elim l; clear l; [constructor|intros a l IHl HND HS].
constructor; [apply IHl; [invlist NoDup|invlist Sorted]; auto|].
clear IHl; revert dependent l; intro l; elim l; clear l; [constructor|].
intros a' l _ HND HS; constructor; apply Sorted_inv in HS; destruct HS as [_ H].
invlist HdRel; apply Z.le_neq; split; [auto|intro subst].
apply NoDup_cons_iff in HND; destruct HND as [HF _]; apply HF; simpl; auto.
Qed.

Lemma NoDup_Sorted_NoDup (l : list Z) : NoDup l -> NoDup (sort l).
Proof.
intro; apply Permutation.Permutation_NoDup with l; [apply Permuted_sort|]; auto.
Qed.

Lemma StriclySorted_strictsort (l : list Z) : ZSorted (strictsort l).
Proof.
apply NoDup_Sorted_Stricly_Sorted; [apply NoDup_Sorted_NoDup, NoDup_nodup|].
assert (HS := LocallySorted_sort (nodup Z.eq_dec l)); unfold strictsort.
remember (sort (nodup Z.eq_dec l)) as l'; clear dependent l.
revert dependent l'; intro l; elim l; clear l; [constructor|].
intros a l IHl HS; constructor; [apply IHl; invlist Sorted; auto|].
clear IHl; apply Sorted_inv in HS; destruct HS as [_ Hlt].
revert dependent l; intro l; destruct l as [|a' l]; [constructor|].
intro; invlist HdRel; constructor; apply Z.leb_le; auto.
Qed.

Definition list2set (l : list Z) : set :=
  exist _ (strictsort l) (StriclySorted_strictsort l).

Definition snums := list2set nums.

Compute cardinal snums.

Definition snums2 := list2set nums2.

Compute cardinal snums2.

Time Compute cardinal (inter snums snums2).

Time Compute cardinal (union snums snums2).
*)

Definition select (test : Z -> bool) (s : set) := filter test s.

(*
https://coq.inria.fr/refman/addendum/extraction.html
*)

(*
Extraction Language Haskell.
*)

Extraction filter_aux.

Extraction filter.

Recursive Extraction inter.

Recursive Extraction inter union select.

(*
Extraction "test" select.
*)

Lemma select2 s p1 p2 x :
  mem x (select p1 (select p2 s)) =
  mem x (select (fun y => p1 y && p2 y) s).
Proof.
unfold select; repeat rewrite mem_filter.
case (mem x s); simpl; [rewrite andb_comm|]; reflexivity.
Qed.