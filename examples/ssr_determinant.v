From mathcomp Require Import all_ssreflect all_algebra.

Set Implicit Arguments.
Unset Strict Implicit.
Set Printing Implicit Defensive.

Import GRing.

Section playground.

Open Scope ring_scope.

Variable R : comRingType.

Definition m1l : seq (seq R) :=
  [:: [:: 1%:R; 2%:R; 3%:R];
      [:: 4%:R; 5%:R; 6%:R];
      [:: 7%:R; 8%:R; 9%:R]].

Definition m1 : 'M[R]_3 :=
  \matrix_(i, j) nth 0 (nth [::] m1l i) j.

Lemma difficulty : (5 * 6)%:R + ((2 * 1)%:R) = 32%:R :> R.
Proof. by rewrite (* !addrA *) -!mulrnDr. Qed.

Lemma nat_sub_compute n m : n%:R - m%:R =
  (if (n < m)%N then 1 *- (m - n) else (n - m)%:R) :> R.
Proof.
by have [nltm | ngem] := ltnP n m; rewrite natrB // 1?ltnW // opprB.
Qed.

Lemma nat_sub_compute' n m : - m %:R + n%:R =
  (if (n < m)%N then 1 *- (m - n) else (n - m)%:R) :> R.
Proof. by rewrite addrC nat_sub_compute. Qed.

Lemma cmpt_det : \det m1 = 0%:R.
Proof.
repeat
  rewrite (expand_det_col _ ord0) !big_ord_recr big_ord0 /m1 !mxE /= /cofactor.
rewrite !addn0 ?exprS !expr0 !det_mx00.
rewrite !(add0r, addr0, mul1r, mul0r, mulN1r, mulr1, mulrN, opprK,
   mulr0, mulrDl, mulrDr, mulrA, mulNr, fun n m => esym (natrD R n m),
   fun n m => esym (natrM R n m)).
by rewrite !(nat_sub_compute, nat_sub_compute').
Qed.

Lemma cmpt_det' : \det m1 = 0%:R.
Proof.
repeat
  rewrite (expand_det_col _ ord0) !big_ord_recr big_ord0 /m1 !mxE /= /cofactor.
have mulNA : forall (v : R) x y, v *- x *+ y = v *- (x * y).
  by move=> v x y; rewrite -mulNrn -mulrnA mulNrn.
rewrite !addn0 ?exprS !expr0 !det_mx00.
rewrite !(add0r, addr0, mul1r, mul0r, mulN1r, mulr1, mulrN, opprK,
   mulr0, mulrDl, mulrDr, mulrA, mulNr, fun n m => esym (natrD R n m),
   fun n m => esym (natrM R n m)).
have [f fq] : exists f, f =1 natmul (1 : R) by exists (natmul 1).
by rewrite -!fq !addrA !fq !(addrAC _ (- _) (1 *+ _)) -!natrD -!natrB.
Qed.

End playground.

Definition m2l : seq (seq int) :=
 [seq [seq Posz e | e <- row]
    | row <- [:: [:: 1; 2; 3];
                 [:: 4; 5; 6];
                 [:: 7; 8; 9]]].

Open Scope ring_scope.

Definition m2 : 'M[int]_3 :=
  (\matrix_(i, j) ((i * 3 + j)%N : int))%R.

Lemma detm2 : \det m2 = 0.
repeat
  rewrite !(expand_det_col _ ord0) !big_ord_recr /= big_ord0 /m2 !mxE /cofactor.
rewrite /= /bump /=.
rewrite !(add0r, mul1r, mulr1, addn0, expr1, det_mx00, big_ord0, add1n, add0n).
by [].
Qed.

Definition m2x : 'M[int]_3 :=
 \matrix_(i, j) nth (0 : int) (nth [::] m2l i) j.

Lemma detm2x : (\det m2x)%R = 0.
Proof.
repeat
  rewrite !(expand_det_col _ ord0) !big_ord_recr /= big_ord0 /m2x !mxE /cofactor
     /=.
rewrite !(add0r, mul1r, mulr1, addn0, expr1, det_mx00, big_ord0).
by [].
Qed.

Definition mg (n : nat) : 'M[int]_n :=
  \matrix_(i, j) ((i * n + j)%N : int).

Lemma det_mg_0 n : (2 < n)%N -> \det (mg n) = 0.
Proof.
move=> ngt2.
have [n' n'p] : exists n', n = n'.+3.
  by case: n ngt2 => [ | [ | [ | n']]] // _; exists n'.
rewrite n'p {n ngt2 n'p}.
set v : 'rV[int]_n'.+3 :=
  \matrix_(i, j) 
  (if val j == 0%N then 1 else
   if val j == 1%N then -(2%N : int) else
   if val j == 2%N then 1 else 0).
apply/eqP/det0P; exists v.
  apply/eqP=> abs.
  have lt0n : (0 < n'.+3)%N by [].
  have : v ord0 (Ordinal lt0n) = 1 by rewrite /v !mxE.
  by rewrite abs mxE.
rewrite mulmx_sum_row.
set f := fun (j : nat) => v 0 (inord j) *:  row (inord j) (mg n'.+3).
rewrite (eq_bigr (fun i => f (nat_of_ord i))); last first.
  by move=> i _; rewrite /f inord_val.
rewrite -(big_mkord xpredT f).
rewrite !big_nat_recl // !addrA.
rewrite (@eq_big_nat _ _ +%R 0%nat n' _ (fun i => 0)); last first.
  by move=> i /andP[_ iltn]; rewrite /f /v mxE /= inordK //= scale0r.
rewrite big1 // addr0.
rewrite /f !mxE /= !inordK //= !scale1r.
apply/rowP=> j; rewrite /mg !mxE /= !inordK // !(mul0n, add0n, mul1r, mul1n).
rewrite !PoszD PoszM mulrDr !addrA. 
rewrite 3!(addrAC _ (_ * (Posz n'.+3))) !mulNr addrK.
by rewrite addrAC -mul2z addrN.
Qed.
