Require Import ZArith Lia.

Fixpoint fib (n : nat) :=
  match n with
  | S (S p as p') => fib p + fib p'
  | _ => 1
  end.

Fixpoint fibt (n : nat) (acc1 acc2 : nat) : nat :=
  match n with
  | S p => fibt p (acc2 + acc1) acc1
  | 0 => acc1
  end.

Fixpoint Zfibt (n : nat) (acc1 acc2 : Z) : Z :=
  match n with
  | S p => Zfibt p (acc2 + acc1) acc1
  | _ => acc1
  end.

Lemma fibt_aux (n k: nat) : fibt n (fib (S k)) (fib k) = fib (S (n + k)).
Proof.
revert k.
induction n as [ | p IH]; intros k.
  cbn [fibt].
  now rewrite Nat.add_0_l.
cbn [fibt].
replace (fib k + fib (S k)) with (fib (S (S k))).
  rewrite IH.
  now rewrite Nat.add_succ_r.
easy.
Qed.

Lemma fibtP (n : nat) : fibt n 1 0 = fib n.
Proof.
destruct n as [ | p].
  easy.
cbn[fibt].
replace (0 + 1) with (fib 1) by easy.
replace 1 with (fib 0) at 2 by easy.
rewrite (fibt_aux p 0).
rewrite Nat.add_0_r.
easy.
Qed.

Lemma fibt_wide (a b n : nat) :
  fibt (S n) a b = fib (S n) * a + fib n * b.
Proof.
revert a b; induction n as [ | p IHp]; intros a b.
simpl.
ring.
replace (fib (S (S p))) with (fib p + fib (S p)) by easy.
replace (fibt (S (S p)) a b) with
  (fibt (S p) (b + a) a) by easy.
rewrite IHp.
ring.
Qed.
