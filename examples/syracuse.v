Require Import List ZArith Lia.

Import ListNotations.

Open Scope Z_scope.

Inductive syracuse : Z -> Prop :=
  syracuse1 : syracuse 1
| syracuse2 : forall x, syracuse x -> syracuse (2 * x)
| syracuse3 : forall x,
    syracuse (3 * (2 * x + 1) + 1) -> syracuse (2 * x + 1).

Definition syracuse_step (x : Z) :=
  if (x =? 1) then
    x
  else if (Z.even x) then
    (x / 2)
  else
    (3 * x + 1).

Definition prog_syracuse (x y : Z) : bool :=
  Z.iter y syracuse_step x =? 1.

Lemma syracuseP (x y : Z) : prog_syracuse x y = true -> syracuse x.
Proof.
unfold prog_syracuse.
assert (step1 : forall u, u =? 1 = true -> syracuse u).
  intros u; rewrite Z.eqb_eq; intros xis1; rewrite xis1; exact syracuse1.
destruct (Z_le_dec 0 y); cycle 1.
  destruct y as [p | p | ]; try (case n; lia).
  exact (step1 x).
rewrite iter_nat_of_Z; try lia; clear l.
revert x; induction (Z.abs_nat y) as [ | n IH].
  exact step1.
enough (unroll : forall x:Z, nat_rect _ x (fun _ =>  syracuse_step) (S n) =
        nat_rect _ (syracuse_step x) (fun _ => syracuse_step) n).
  intros x; rewrite unroll; simpl.
  unfold syracuse_step at 1.
  destruct (x=? 1) eqn:test1.
    now intros _; apply step1; auto.
  destruct (Z.even x) eqn:test2.
  intros rec.
  assert (xdouble : x = 2 * (x / 2)).
    apply Z_div_exact_2; try lia.
    now rewrite Zmod_even, test2.
  now rewrite xdouble; apply syracuse2, IH, rec.
  intros rec.
  assert (xdoublep1 : x = 2 * (x / 2) + 1).
    replace 1 with (x mod 2).
      apply Z_div_mod_eq; lia.
    now rewrite Zmod_even, test2.
  rewrite xdoublep1; apply syracuse3.
  rewrite <- xdoublep1.
  now apply IH, rec.
clear IH; induction n as [ | n IH].
  reflexivity.
intros x.
 now simpl in IH |- *; rewrite IH.
Qed.

Compute map (fun n => Z.iter n syracuse_step 43) [1;2;3;4;5;6;7;10;20;25;30].

Lemma syracuse43 : syracuse 43.
Proof.  now apply (syracuseP _ 30). Qed.

