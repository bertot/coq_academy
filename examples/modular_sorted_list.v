From Coq Require Import Bool List Mergesort Orders Sorting ZArith.

From Coq Require Extraction.

Variable S : Set.

Variable lt : S -> S -> Prop.

Variable lt_irrefl : forall a, ~ lt a a.

Variable lt_trans : forall a b c, lt a b -> lt b c -> lt a c.

Definition eq (a b : S) := a = b.

Variable lt_neq : forall a b, lt a b -> ~ eq a b.

Variable eqb : S -> S -> bool.

Variable eqb_eq : forall a b, eqb a b = true <-> eq a b.

Variable compare : S -> S -> comparison.

Variable compare_spec : forall a b, CompareSpec (eq a b) (lt a b) (lt b a) (compare a b).

Definition Inf := HdRel lt.

Definition SSorted : list S -> Prop := Sorted lt.

Definition set := {l : list S | SSorted l}.

Definition empty : set := exist _ nil (Sorted_nil lt).

Definition cardinal (s : set) : nat := length (proj1_sig s).

Lemma lt_SSorted (x y : S) (l : list S) :
  lt x y -> SSorted (y :: l) -> SSorted (x :: l).
Proof.
intro Hlt; elim l; clear l; [constructor; auto|].
intros a l IHl Hl; constructor; [apply Sorted_inv in Hl; tauto|].
constructor; apply lt_trans with y; [auto|clear Hlt].
apply Sorted_inv in Hl; destruct Hl as [_ Hlt]; apply HdRel_inv in Hlt; auto.
Qed.

Fixpoint mem_aux (x : S) (l : list S) :=
  match l with
    | nil    => false
    | y :: l' =>
        match compare x y with
          | Lt => false
          | Eq => true
          | Gt => mem_aux x l'
        end
  end.

Definition mem (x : S) (s : set) := mem_aux x (proj1_sig s).

Fixpoint add_aux (x : S) (l : list S) : list S :=
  match l with
    | nil    => x :: nil
    | y :: l' =>
        match compare x y with
          | Lt => x :: l
          | Eq => l
          | Gt => y :: add_aux x l'
        end
  end.

Lemma add_inf : forall (l : list S) (x a : S),
  Inf a l -> lt a x -> Inf a (add_aux x l).
Proof.
simple induction l; clear l; simpl; [intros; apply HdRel_cons; auto|].
intros a2 l Hl x a1 HInf Hlt; apply HdRel_inv in HInf.
elim (compare x a2); apply HdRel_cons; auto.
Qed.

Lemma add_ok : forall (l : list S) (x : S) `(SSorted l), SSorted (add_aux x l).
Proof.
simple induction l; simpl; clear l;
[intros; apply Sorted_cons; [apply Sorted_nil|apply HdRel_nil]|].
intros a l Hl x HS; elim (compare_spec x a); intro;
[auto|apply Sorted_cons; [|apply HdRel_cons]; auto|].
apply Sorted_cons; [|apply Sorted_inv in HS; apply add_inf; tauto].
apply Hl; apply Sorted_inv in HS; tauto.
Qed.

Definition add (x : S) (s : set) : set :=
  exist _ (add_aux x (proj1_sig s)) (add_ok (proj1_sig s) x (proj2_sig s)).

Inductive In_list (x : S) : list S -> Prop :=
  | In_cons_hd : forall y l, eq x y -> In_list x (y :: l)
  | In_cons_tl : forall y l, In_list x l -> In_list x (y :: l).

Lemma In_cons (x y : S) (l : list S) :
  In_list x (y :: l) <-> eq x y \/ In_list x l.
Proof.
split; [intro; invlist In_list; auto|].
intros [Heq|HIn]; [apply In_cons_hd|apply In_cons_tl]; auto.
Qed.

Lemma mem_aux_iff : forall (l : list S) (x : S) (Hl : SSorted l),
  mem_aux x l = true <-> In_list x l.
Proof.
intro l; elim l; clear l; [intuition; [discriminate|invlist In_list]|].
intros a l IHl x Hl; simpl; destruct (compare_spec x a) as [E|L|G];
rewrite In_cons; intuition; try discriminate;
[exfalso; eapply lt_neq; eauto|..|
 exfalso; eapply lt_neq; eauto; apply eq_sym; auto|];
[|right; apply IHl; auto; eapply Sorted_inv; eauto|
  eapply IHl; auto; eapply Sorted_inv; eauto].
exfalso; assert (lt a x); [|apply (lt_irrefl a), lt_trans with x; auto].
apply Sorted_extends in Hl; [|intro; apply lt_trans].
eapply Forall_forall; eauto; clear dependent a; clear IHl; revert dependent x.
elim l; clear l; [simpl; intros; invlist In_list|intros a l IHl x HIn].
apply In_cons in HIn; destruct HIn as [HEq|HIn]; [left; auto|right; auto].
Qed.

Lemma add_aux_spec : forall (l : list S) (x y : S) (Hl : SSorted l),
  In_list y (add_aux x l) <-> eq x y \/ In_list y l.
Proof.
intro l; elim l; clear l; simpl;
[intuition; invlist In_list; rewrite ?In_cons; unfold eq in *; auto|].
intros a l IHl x y Hl; simpl; elim (compare_spec x a); intro;
rewrite !In_cons, ?IHl; unfold eq in *; subst; intuition.
eapply Sorted_inv; eauto.
Qed.

Lemma mem_aux_cons (x a : S) (l : list S) (Hl : SSorted l) :
  mem_aux x (add_aux a l) = eqb x a || mem_aux x l.
Proof.
generalize (mem_aux_iff (add_aux a l) x (add_ok l a Hl)) (mem_aux_iff l x Hl).
generalize (add_aux_spec l a x Hl) (eqb_eq x a).
case (eqb x a), (mem_aux x l), (mem_aux x (add_aux a l)); intuition.
simpl; apply eq_sym; intuition.
Qed.

Lemma mem_cons (x a : S) (s : set) : mem x (add a s) = eqb x a || mem x s.
Proof. destruct s; unfold add, mem; simpl; apply mem_aux_cons; auto. Qed.

Fixpoint filter_aux {A : Set} (f : A -> bool) (l : list A) : list A :=
  match l with
    | nil     => nil
    | x :: l' => if f x then x :: filter_aux f l' else filter_aux f l'
  end.

Lemma filter_ok f l : forall `(SSorted l), SSorted (filter_aux f l).
Proof.
elim l; [simpl; auto|clear l; intros x l IHl].
simpl; case (f x); intro Hl; [|apply IHl; eapply Sorted_inv; eauto].
constructor; [apply IHl; eapply Sorted_inv; eauto|clear IHl; revert Hl].
elim l; clear l; [simpl; intros; apply HdRel_nil|intros y l IHl].
simpl; case (f y); intro; [constructor|apply IHl; clear IHl];
[apply Sorted_inv in Hl; destruct Hl as [_ Hlt]; apply HdRel_inv in Hlt; auto|].
apply Sorted_inv in Hl; destruct Hl as [Hl Hlt]; apply HdRel_inv in Hlt.
apply lt_SSorted with y; auto.
Qed.

Definition filter (f : S -> bool) (s : set) : set :=
  exist _ (filter_aux f (proj1_sig s))
        (filter_ok f (proj1_sig s) (proj2_sig s)).

Lemma filter_aux_spec :
  forall (l : list S) (x : S) (f : S -> bool) (fP : Proper (eq ==> Logic.eq) f),
    (In_list x (filter_aux f l) <-> In_list x l /\ f x = true).
Proof.
intro l; elim l; clear l; simpl; [intros; split; intuition; invlist In_list|].
intros a l IHl x f fP; destruct (f a) eqn:F; rewrite !In_cons, ?IHl; auto;
unfold eq; split; [intros [HEq|[HIn Fx]]|intuition..|intros [[HEq|HIn] Fx]];
try rewrite (fP _ _ HEq); auto.
rewrite HEq in Fx; rewrite F in Fx; discriminate.
Qed.

Lemma mem_filter (f : S -> bool) (x : S) (s : set) :
  Proper (eq ==> Logic.eq) f -> mem x (filter f s) = mem x s && f x.
Proof.
destruct s as [l Hl]; unfold mem, filter; simpl.
generalize (mem_aux_iff (filter_aux f l) x (filter_ok f l Hl)).
generalize (mem_aux_iff l x Hl) (filter_aux_spec l x f).
case (f x), (mem_aux x l), (mem_aux x (filter_aux f l)); intuition.
Qed.

Fixpoint inter_aux (l : list S) : list S -> list S :=
  match l with
    | nil    => fun _ => nil
    | x :: l' =>
        (fix inter_aux_aux (l'' : list S) : list S :=
           match l'' with
             | nil      => nil
             | x' :: l''' =>
                  match compare x x' with
                    | Lt => inter_aux l' l''
                    | Eq => x :: inter_aux l' l'''
                    | Gt => inter_aux_aux l'''
                  end
           end)
  end.

Lemma inter_ok l l' : forall `(SSorted l, SSorted l'), SSorted (inter_aux l l').
Proof.
revert l'; elim l; clear l; [simpl; auto|].
intros a l IHl l'; elim l'; clear l'; [simpl; auto|intros a' l' IHl' Hl Hl'].
simpl; destruct (compare_spec a a') as [E|L|G]; simpl; [rewrite <- E in *|..].

  {
  constructor; [apply IHl|];[apply Sorted_inv in Hl|apply Sorted_inv in Hl'|];
  [intuition..|]; clear a' E IHl IHl'; revert dependent l'; revert dependent l.
  intro l; elim l; clear l; [simpl; auto|intros x l IHl Hl l'].
  elim l'; clear l'; [simpl; auto|intros x' l' IHl' Hl'].
  simpl; assert (H1 := Hl); assert (H2 := Hl').
  apply Sorted_inv in H1; apply Sorted_inv in H2.
  elim (compare x x'); simpl; intros;
  [constructor; destruct H1 as [_ Hlt]; apply HdRel_inv in Hlt; auto|..];
  [apply IHl|apply IHl']; [|intuition|];
  [apply lt_SSorted with x|apply lt_SSorted with x']; try solve [intuition];
  [destruct H1 as [_ Hlt]|destruct H2 as [_ Hlt]]; apply HdRel_inv in Hlt; auto.
  }

  {
  apply IHl; auto;  apply Sorted_inv in Hl; intuition.
  }

  {
  cut (SSorted (inter_aux (a :: l) l')); [simpl; auto|apply IHl'; auto].
  apply Sorted_inv in Hl'; intuition.
  }
Qed.

Definition inter (s1 s2 : set) : set :=
  exist _ (inter_aux (proj1_sig s1) (proj1_sig s2))
        (inter_ok (proj1_sig s1) (proj1_sig s2) (proj2_sig s1) (proj2_sig s2)).

Lemma In_list_In (x : S) (l : list S) : In_list x l <-> In x l.
Proof.
elim l; clear l; simpl; [intuition; invlist In_list|intros a l IHl].
split; [intro HIn; apply In_cons in HIn; intuition|].
intros [HEq|HIn]; apply In_cons; intuition.
Qed.

Lemma inter_aux_spec :
  forall (l l' : list S) (x : S) (Hl : SSorted l) (Hl' : SSorted l'),
    In_list x (inter_aux l l') <-> In_list x l /\ In_list x l'.
Proof.
intro l; elim l; clear l; simpl; [intuition; invlist In_list|].
intros a l IHl l'; elim l'; clear l'; [intuition; invlist In_list|].
intros a' l' IHl' x Hl Hl'; simpl; assert (Hal := Hl); assert (Ha'l' := Hl').
apply Sorted_inv in Hl; destruct Hl as [Hl _].
apply Sorted_inv in Hl'; destruct Hl' as [Hl' _].
elim (compare_spec a a'); intro HC; rewrite !In_cons, ?IHl, ?IHl'; auto;
[rewrite HC; intuition|split..]; try solve [intuition; apply In_cons; auto];
[intros [[HEq1|HIn1] [HEq2|HIn2]]|intros [[HEq1|HIn1] [HEq2|HIn2]]];
try solve [exfalso; apply (lt_irrefl x); intuition];
try solve [split; try apply In_cons; auto];
exfalso; apply (lt_irrefl a), lt_trans with a'; unfold eq in *; subst; auto;
[assert (Hlt := Ha'l')|assert (Hlt := Hal)]; apply Sorted_extends in Hlt;
try solve [intro; apply lt_trans]; eapply Forall_forall in Hlt;
try solve [apply In_list_In; eauto]; intuition.
Qed.

Lemma interP (x : S) (s1 s2 : set) :
  mem x (inter s1 s2) = mem x s1 && mem x s2.
Proof.
destruct s1 as [l1 Hl1]; destruct s2 as [l2 Hl2]; unfold mem, inter; simpl.
generalize (inter_aux_spec l1 l2 x Hl1 Hl2) (mem_aux_iff (inter_aux l1 l2) x).
generalize (mem_aux_iff l1 x) (mem_aux_iff l2 x) (inter_ok l1 l2).
case (mem_aux x l1), (mem_aux x l2), (mem_aux x (inter_aux l1 l2)); intuition.
Qed.

Fixpoint union_aux (l : list S) : list S -> list S :=
  match l with
    | nil    => fun l' => l'
    | x :: l' =>
        (fix union_aux_aux (l'' : list S) : list S :=
           match l'' with
             | nil      => l
             | x' :: l''' =>
                  match compare x x' with
                    | Lt => x  :: union_aux l' l''
                    | Eq => x  :: union_aux l' l'''
                    | Gt => x' :: union_aux_aux l'''
                  end
           end)
  end.

Lemma union_inf :
  forall (l l' : list S) (a : S) (Hl : SSorted l) (Hl' : SSorted l'),
    Inf a l -> Inf a l' -> Inf a (union_aux l l').
Proof.
intro l; elim l; clear l; [simpl; auto|intros x l IHl l'; simpl].
destruct l' as [|x' l']; [simpl; auto|intros a Hl Hl'].
elim (compare_spec x x'); intros; apply HdRel_cons; eapply HdRel_inv; eauto.
Qed.

Lemma union_ok l l' : forall `(SSorted l, SSorted l'), SSorted (union_aux l l').
Proof.
revert l'; elim l; clear l; [simpl; auto|intros x l IHl l'].
elim l'; clear l'; [simpl; auto|intros x' l' IHl' Hl Hl'].
simpl; destruct (compare_spec x x') as [E|L|G]; simpl.

  {
  constructor; [apply IHl|]; [apply Sorted_inv in Hl|apply Sorted_inv in Hl'|];
  [intuition..|apply union_inf; eapply Sorted_inv; eauto].
  rewrite E; auto.
  }

  {
  constructor; [apply IHl|]; [apply Sorted_inv in Hl|..]; [intuition..|].
  apply union_inf;  eapply Sorted_inv; eauto.
  }

  {
  cut (SSorted (x' :: union_aux (x :: l) l')); [simpl; auto|].
  constructor; [apply IHl'|apply union_inf]; try solve [constructor; auto];
  eapply Sorted_inv; eauto.
  }
Qed.

Definition union (s1 s2 : set) : set :=
  exist _ (union_aux (proj1_sig s1) (proj1_sig s2))
        (union_ok (proj1_sig s1) (proj1_sig s2) (proj2_sig s1) (proj2_sig s2)).

Lemma union_aux_spec :
  forall (l l' : list S) (x : S) (Hl : SSorted l) (Hl' : SSorted l'),
    In_list x (union_aux l l') <-> In_list x l \/ In_list x l'.
Proof.
intro l; elim l; clear l; simpl; [intuition; invlist In_list|].
intros a l IHl l'; elim l'; clear l'; [intuition; invlist In_list|].
intros a' l' IHl' x Hl Hl'; simpl; elim (compare_spec a a'); intro HC;
rewrite !In_cons, ?IHl, ?IHl'; try solve [eapply Sorted_inv; eauto];
rewrite ?In_cons; unfold eq in *; subst; intuition.
Qed.

Lemma unionP (x : S) (s1 s2 : set) :
  mem x (union s1 s2) = mem x s1 || mem x s2.
Proof.
destruct s1 as [l1 Hl1]; destruct s2 as [l2 Hl2]; unfold mem, union; simpl.
generalize (union_aux_spec l1 l2 x Hl1 Hl2) (mem_aux_iff (union_aux l1 l2) x).
generalize (mem_aux_iff l1 x) (mem_aux_iff l2 x) (union_ok l1 l2).
case (mem_aux x l1), (mem_aux x l2), (mem_aux x (union_aux l1 l2)); intuition.
Qed.

Definition select (test : S -> bool) (s : set) := filter test s.

(*
https://coq.inria.fr/refman/addendum/extraction.html
*)

(*
Extraction Language Haskell.
*)

Extraction filter_aux.

(*
Extract Constant S => "my_set".

Extract Inlined Constant compare => "my_compare".
*)

Recursive Extraction inter_aux.

Recursive Extraction inter union select.

(*
Extraction "test" select.
*)

Lemma select2 s p1 p2 x :
  Proper (eq ==> Logic.eq) p1 ->
  Proper (eq ==> Logic.eq) p2 ->
  mem x (select p1 (select p2 s)) =
  mem x (select (fun y => p1 y && p2 y) s).
Proof.
intros Hp1 Hp2; unfold select; repeat rewrite mem_filter; auto;
[|intros a b Hab; rewrite (Hp1 _ _ Hab), (Hp2 _ _ Hab); auto].
case (mem x s); simpl; [rewrite andb_comm|]; reflexivity.
Qed.