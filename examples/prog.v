Require Import List Arith.

Fixpoint lo (n : nat) :=
  match n with
  | 0   => nil
  | S m => (2 * m + 1) :: lo m
  end.

Lemma length_lo n : length (lo n) = n.
Proof.
induction n as [|n IH].
  simpl; easy.
change (length (lo (S n))) with (S (length (lo n))).
rewrite IH; easy.
Qed.

Definition sl (l : list nat) := fold_right (fun e i => e + i) 0 l.

Lemma sl_lo n : sl (lo n) = n * n.
Proof.
induction n as [|n IH].
  unfold sl; simpl; easy.
change (sl (lo (S n))) with ((2 * n + 1) + sl (lo n)).
rewrite IH, Nat.mul_succ_r.
replace (S n * n) with (n * n + n) by (rewrite Nat.mul_succ_l; easy).
simpl; rewrite Nat.add_0_r, Nat.add_comm.
replace (n + n + 1) with (n + (n + 1)) by (rewrite Nat.add_assoc; easy).
rewrite Nat.add_1_r, Nat.add_assoc; easy.
Qed.

Fixpoint add x y :=
  match x with
  | 0   => y
  | S p => add p (S y)
  end.

Lemma add_n_Sm : forall x y, add x (S y) = S (add x y).
Proof.
intro x; induction x as [|x IH]; intro y.
  simpl; easy.
simpl; rewrite IH; easy.
Qed.

Lemma add_n_0 : forall x, add x 0 = x.
Proof.
intro x; induction x as [|x IH].
  simpl; easy.
simpl; rewrite add_n_Sm, IH; easy.
Qed.

Lemma add_Sn_m : forall x y, add (S x) y = S (add x y).
Proof. intros x y; simpl; rewrite add_n_Sm; easy. Qed.

Lemma addA : forall x y z, add x (add y z) = add (add x y) z.
Proof.
intro x; induction x as [|x IH]; intros y z; [simpl; easy|].
rewrite add_Sn_m, IH, <- !add_Sn_m; easy.
Qed.

Lemma add_correct : forall x y, add x y = x + y.
Proof.
intro x; induction x as [|x IH]; intro y; simpl; [easy|].
rewrite IH, plus_n_Sm; easy.
Qed.