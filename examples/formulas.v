Variable H : Type -> Prop. (* H(x) signifie que x est un humain *)
Variable T : Type -> Prop. (* T(x) signifie que x est un tableau *)
Variable P : Type -> Type -> Prop. (* P x y signifie que x a peint y *)
Variable V : Type -> Type -> Prop. (* V x y signifie que x a vu y *)

Variable m : Type. (* m signifie Monet *)
Variable n : Type. (* n signifie les Nymphéas *)
Variable p : Type. (* p signifie Paul *)

(* Pour tout tableau, soit il a été vu par un humain soit il a été peint par Paul *)
Definition E := forall y, T y -> (exists x, H x /\ V x y) \/ P p y.

(* Paul a vu les Nymphéas *)
Definition Q1 := V p n.

(* Quelqu'un a vu les Nymphéas *)
Definition Q2 := exists x, H x /\ V x n.

(* Paul a vu tous les tableaux *)
Definition Q3 := forall y, T y -> V p y.

(* Tout le monde a vu les Nymphéas *)
Definition Q4 := forall x, H x -> V x n.

(* Personne n'a vu les Nymphéas *)
Definition Q5 := forall x, H x -> ~ V x n.

(* Tout le monde n'a pas vu les Nymphéas (attention il faut le comprendre dans un sens différent de la question précédente) *)
Definition Q6 := ~ (forall x, H x -> V x n).
(* Definition Q6 := exists x, H x /\ ~ V x n.
   serait aussi acceptable mais pas intuitionnistiquement équivalent *)

(* Paul a vu un tableau *)
Definition Q7 := exists y, T y /\ V p y.

(* Paul a vu exactement deux tableaux *)
Definition Q8 := exists y1 y2, T y1 /\ V p y1 /\ T y2 /\ V p y2 /\ y1 <> y2 /\
	(forall y3, (T y3 /\ V p y3) -> y1 = y3 \/ y2 = y3).

(* Monet a peint les Nymphéas *)
Definition Q9 := P m n.

(* Paul n'a pas peint de tableau *)
Definition Q10 := ~ (exists y, T y /\ P p y).

(* Paul a vu tous les tableaux de Monet *)
Definition Q11 := forall y, (T y  /\ P m y) -> V p y.

(* Paul n'a pas vu tous les tableaux de Monet *)
Definition Q12 := ~ (forall y, (T y  /\ P m y) -> V p y).
(* Definition Q12 := exists y, T y  /\ P m y /\ ~ V p y.
   serait aussi acceptable mais pas intuitionnistiquement équivalent *)

(* Tout le monde a vu un tableau de Monet *)
Definition Q13 := forall x, H x -> exists y, T y /\ P m y /\ V x y.

(* Quelqu'un a vu tous les tableaux de Monet *)
Definition Q14 := exists x, H x /\ forall y, (T y /\ P m y) -> V x y.

(* Tous ceux qui ont peint un tableau ont vu les Nymphéas *)
Definition Q15 := forall x, (H x /\ exists y, T y /\ P x y) -> V x n.

(* Tous ceux qui ont peint un tableau ont vu un tableau de Monet *)
Definition Q16 := forall x, (H x /\ exists y1, T y1 /\ P x y1) -> exists y2, T y2 /\ P m y2 /\ V x y2.

(* Tous les tableaux n'ont pas été peint par la même personne *)
Definition Q17 := ~ (exists x, H x /\ forall y, T y -> P x y).

(* Monet n'a pas peint tous les tableaux *)
Definition Q18 := ~ (forall y, T y -> P m y).
(* Definition Q18 := exists y, T y /\ ~ P m y.
   serait aussi acceptable mais pas intuitionnistiquement équivalent *)

(* Monet n'a pas peint que les Nymphéas *)
Definition Q19 := ~ (forall y, (T y /\ P m y) -> n = y).
(* Definition Q19 := exists y, T y /\ P m y /\ n <> y.
   serait aussi acceptable mais pas intuitionnistiquement équivalent *)

(* Parmi les tableaux de Monet, Paul n'a vu que les Nymphéas *)
Definition Q20 := forall y, (T y /\ P m y /\ V p y) -> n = y.

(* Parmi les tableaux de Monet, Paul n'a pas vu que les Nymphéas *)
Definition Q21 := V p n /\ ~ (forall y, T y /\ P m y /\ V p y -> n = y).
(* Definition Q21 := V p n /\ exists y, T y /\ P m y /\ V p y /\ n <> y.
   serait aussi acceptable mais pas intuitionnistiquement équivalent *)