Require Import ZArith Lia.
Require Import Arith.

Fixpoint fib (n : nat) :=
  match n with
  | S (S p as p') => fib p + fib p'
  | _ => 1
  end.

Fixpoint fibt (n : nat) (acc1 acc2 : nat) : nat :=
  match n with
  | S p => fibt p (acc2 + acc1) acc1
  | 0 => acc1
  end.

Fixpoint Zfibt (n : nat) (acc1 acc2 : Z) : Z :=
  match n with
  | S p => Zfibt p (acc2 + acc1) acc1
  | _ => acc1
  end.

Lemma fibt_aux (n k: nat) : fibt n (fib (S k)) (fib k) = fib (S (n + k)).
Proof.
Qed.

Lemma fibtP (n : nat) : fibt n 1 0 = fib n.
Proof. ... Qed.

Lemma fibt_wide (a b n : nat) :
  fibt (S n) a b = fib (S n) * a + fib n * b.
