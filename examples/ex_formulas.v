Variable H : Type -> Prop. (* H(x) signifie que x est un humain *)
Variable T : Type -> Prop. (* T(x) signifie que x est un tableau *)
Variable P : Type -> Type -> Prop. (* P x y signifie que x a peint y *)
Variable V : Type -> Type -> Prop. (* V x y signifie que x a vu y *)

Variable m : Type. (* m signifie Monet *)
Variable n : Type. (* n signifie les Nymphéas *)
Variable p : Type. (* p signifie Paul *)

(* Pour tout tableau, soit il a été vu par un humain soit il a été peint par Paul *)
Definition E := forall y, T y -> (exists x, H x /\ V x y) \/ P p y.

(* Paul a vu les Nymphéas *)
Definition Q1 := ...

(* Quelqu'un a vu les Nymphéas *)
Definition Q2 := ...

(* Paul a vu tous les tableaux *)
Definition Q3 := ...

(* Tout le monde a vu les Nymphéas *)
Definition Q4 := ...

(* Personne n'a vu les Nymphéas *)
Definition Q5 := ...

(* Tout le monde n'a pas vu les Nymphéas (attention il faut le comprendre
   dans un sens différent de la question précédente) *)
Definition Q6 := ...

(* Paul a vu un tableau *)
Definition Q7 := ...

(* Paul a vu exactement deux tableaux *)
Definition Q8 := ...

(* Monet a peint les Nymphéas *)
Definition Q9 := ...

(* Paul n'a pas peint de tableau *)
Definition Q10 := ...

(* Paul a vu tous les tableaux de Monet *)
Definition Q11 := ...

(* Paul n'a pas vu tous les tableaux de Monet *)
Definition Q12 := ...

(* Tout le monde a vu un tableau de Monet *)
Definition Q13 := ...

(* Quelqu'un a vu tous les tableaux de Monet *)
Definition Q14 := ...

(* Tous ceux qui ont peint un tableau ont vu les Nymphéas *)
Definition Q15 := ...

(* Tous ceux qui ont peint un tableau ont vu un tableau de Monet *)
Definition Q16 := ...

(* Tous les tableaux n'ont pas été peint par la même personne *)
Definition Q17 := ...

(* Monet n'a pas peint tous les tableaux *)
Definition Q18 := ...

(* Monet n'a pas peint que les Nymphéas *)
Definition Q19 := ...

(* Parmi les tableaux de Monet, Paul n'a vu que les Nymphéas *)
Definition Q20 := ...

(* Parmi les tableaux de Monet, Paul n'a pas vu que les Nymphéas *)
Definition Q21 := ...