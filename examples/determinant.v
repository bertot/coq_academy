Require Import List ZArith.

Import ListNotations.

Section ring_operations.

Variables (A : Type) (add mul : A -> A -> A) (opp : A -> A) (zero : A).

Fixpoint expand_col det_fun (sign : bool)
  (acc : list (list A)) (l : list (list A)) : A :=
match l with
  nil => zero
| nil :: tl => zero
| (a :: row) :: rows =>
    add (mul (if sign then a else opp a)
          (det_fun (rev acc ++ (map (@tl A) rows))))
        (expand_col det_fun (negb sign) (row :: acc) rows)
end.

Lemma expand_col_step det_fun (sign : bool) (acc : list (list A))
  (l : list (list A)) :
  expand_col det_fun sign acc l =
  match l with
    nil => zero
  | nil :: tl => zero
  | (a :: row) :: rows =>
      add (mul (if sign then a else opp a)
            (det_fun (rev acc ++ (map (@tl A) rows))))
          (expand_col det_fun (negb sign) (row :: acc) rows)
  end.
Proof. destruct l; auto. Qed.

Fixpoint determinant (n : nat) (l : list (list A)) : A :=
  match n with
  | O => zero
  | 1 => nth 0 (nth 0 l nil) zero
  | S p => expand_col (determinant p) true nil l
  end.


Lemma determinant_step (n : nat) (l : list (list A)) :
  determinant n l =
  match n with
  | O => zero
  | 1 => nth 0 (nth 0 l nil) zero
  | S p => expand_col (determinant p) true nil l
  end.
Proof. destruct n; auto. Qed.

End ring_operations.

Open Scope Z_scope.
(* determinant [[1;2;3];
                [4;5;6];
                [7;8;9]] = 0

   because 3rd line is - 1 time first line + 2 times second line *)

Compute determinant Z Z.add Z.mul Z.opp Z0 3
  [[1; 2; 3];
   [4; 5; 6];
   [7; 8; 9]].
