Require Import List Arith.

(* Define a function lo that takes a natural number n as input and returns
the list containing the first n odd natural numbers. For instance lo 3 =
5::3::1. *)

Fixpoint lo (n : nat) := ...

Lemma length_lo n : length (lo n) = n.
Proof. ... Qed.

(* Define a function sl that takes a list of natural numbers as input and
returns the sum of all the elements in the list. *)

Fixpoint sl (l : list nat) := ...

Lemma sl_lo n : sl (lo n) = n * n.
Proof. ... Qed.

Fixpoint add x y :=
  match x with
  | 0   => y
  | S p => add p (S y)
  end.

Lemma add_n_Sm : forall x y, add x (S y) = S (add x y).
Proof. ... Qed.

Lemma add_n_0 : forall x, add x 0 = x.
Proof. ... Qed.

Lemma add_Sn_m : forall x y, add (S x) y = S (add x y).
Proof. ... Qed.

Lemma addA : forall x y z, add x (add y z) = add (add x y) z.
Proof. ... Qed.

Lemma add_correct : forall x y, add x y = x + y.
Proof. ... Qed.
