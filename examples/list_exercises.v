Require Import List.

Import ListNotations.

Lemma in_1_3 (A : Type) (a b c : A) : In a [a; b; c].
Proof.
simpl.
left.
reflexivity.
Qed.

Lemma in_all_3 (A : Type) (a b c : A) :
   In a [a; b; c] /\
   In b [a; b; c] /\
   In c [a; b; c].
Proof.
split.
  apply in_1_3.
split.
  simpl.
  right.
  left.
  easy.
simpl.
right.
right.
left.
easy.
Qed.

Fixpoint pick_accumulate (A : Type) (pick2 : A -> A -> A) (acc : A) l : A :=
  match l with
  | b :: l' => pick_accumulate A pick2 (pick2 acc b) l'
  | nil => acc
  end.

Definition pick_list (A : Type) (pick2 : A -> A -> A) (default : A) l : A :=
  match l with
  | nil => default
  | a :: l' =>  pick_accumulate A pick2 a l'
  end.

Lemma pick_list_in (A : Type) (pick2 : A -> A -> A) default l:
   (forall a b, pick2 a b = a \/ pick2 a b = b) ->
   l <> nil ->
   In (pick_list A pick2 default l) l.
Proof.
intros cond_pick2.
assert (pick_accumulate_in : forall a ll, In (pick_accumulate A pick2 a ll) (a :: ll)).
  intros a ll; revert a.
  induction ll as [ | b ll' IH].
    intros a; simpl.
    left; easy.
  intros a; simpl.
  destruct (cond_pick2 a b) as [pa | pb].
    rewrite pa; destruct (IH a) as [eqa | inll'].
      rewrite <- eqa.
      left.
      easy.
    right.
    right.
    assumption.
  rewrite pb.
  right.
  exact (IH b).
destruct l as [ | a ll].
  intros abs; case abs; easy.
intros _.
exact (pick_accumulate_in a ll).
Qed.





