Require Import List ZArith Bool.
Import ListNotations.
Open Scope Z_scope.

(* Erroneous pointers and buffer overflow errors are well known causes of
  bugs and security breaches, for instance in network software. *)

(* A well known use case where one needs to build structures with pointers
  is the case of tree structures. *)

(* Des nombres entiers relatifs: Calcul exact, non borné. *)
Compute (2 ^ 100 - 2 ^ 99) / 2 ^ 97.

(* Des listes de nombres (ceci remplace les tableaux) *)

Check [1; 2; 3].

(* Des fonctions, par exemple des fonctions sur les nombres. *)

Check (fun x => 5 * x ^ 2 - 6 * x + 2).

(* Nous pouvons définir une fonction. *)

Definition my_poly := fun x => 5 * x ^ 2 - 6 * x + 2.

(* Nous pouvons appliquer une fonction à un argument. Les parenthèses
   ne sont pas nécessaires.  On écrit la fonction à gauche et l'argument
   à droite.*)

Compute my_poly 3.

Compute my_poly (3 + 5).

(* Nous pouvons appliquer une fonction à tous les éléments d'une liste.
   Dans la ligne qui suit, la fonction map est appliquée à deux arguments,
   le premier est la fonction my_poly, et le second est la liste [1; 2; 3]. *)

Compute map my_poly [1; 2; 3].

Fail Compute map (my_poly [1; 2; 3]).

Definition my_sequence := map my_poly [1; 2; 3].

(* Calculer n'est pas le point important du système Coq.  En fait, lorsque
  l'on définit une valeur, celle-ci reste non calculée par défaut. *)
Check my_sequence.

Print my_sequence.

(* A chaque valeur, on attache une information importante: son type.  Cette
   valeur est affichée par la commande "Check".  Nous pouvons également préciser
  le type auquel nous nous attendons pour une expression. *)

Check (fun x => x + 2) : Z -> Z.

(* Nous pouvons manipuler des types. *)

Check Z.

(* Le type Set qui apparait ici est un "type de types", qui est utilisé
  essentiellement pour indiquer que Coq a reconnu que le type utilisé 
  est vraiment simple, en un sens. *)

Check Z -> Z.

(* Nous avons des fonctions qui prennent des types en argument
   et retournent des types. *)

Check list.

Check list Z.

(* Par exemple, on peut écrire des tableaux bi-dimensionnels en faisant
   des listes de listes. *)

Check [[1; 2; 3]; [4; 5; 6]; [7; 8; 9]].

(* POLYMORPHISME *)

(* Certaines opérations sur les listes ne dépendent pas du type des
   élément. *)
Check length.

Check length my_sequence.

(* La fonction length met en lumière le fait qu'il y a au moins deux types
   de nombres en Coq. *)
Compute length my_sequence.

Fail Check (length my_sequence) + 2.

(* Rassurez-vous, il y a des fonctions de coercion comme dans les
   autres langages de programmation.  Par défaut, elles doivent être
   utilisées explicitement. *)
Check (Z.of_nat (length my_sequence) + 2).

(* Le polymorphisme joue aussi pour des fonctions à deux arguments:
   il doit y avoir de la cohérence. *)

Check map.

(* Ici on voit que map prend une fonction de type (A -> B) en argument
   et une liste.  Il faut vraiment s'assurer que la liste contient des
   éléments qui sont légitimes comme arguments de la fonction.  C'est pour
   cela que l'on fixe l'argument A qui sert à assurer la cohérence
   entre la fonction et la liste. *)

Check map (fun x => x + 3).

Check map (fun x => [x]).  (* Ici on produit un type avec un trou. *)

(* TYPES DEPENDANTS *)

(* Un autre exemple : un type indexé par des entiers. *)

Definition arrayZ (n : nat) :=
  {l : list Z | length l = n}.

Definition list_to_array (l : list Z) : arrayZ (length l) :=
    exist _ l eq_refl.

Check list_to_array [1; 2; 3] : arrayZ 3.

Fail Definition bad_arrayZ : arrayZ 3 := list_to_array [1; 2].

(* list_to_array s'utilise comme une fonction.  Mais le type de la valeur
  retournée n'est connu que lorsque l'on connait la *valeur* de l'argument. *)
Check list_to_array.

(* Une minute pour réfléchir: nous avons ici un langage dans lequel le type
   peut enregistrer des informations sur la taille des objets.  Un langage
   qui pourrait résoudre les problèmes de "buffer overflow"? *)

(* La même approche peut être utilisée pour décrire des nombres entiers
   bornés. *)

Definition boundZ (n : Z) := {x : Z | -1 < x < n}.

Definition example_boundZ : boundZ 3 :=
  exist _ 2 (conj eq_refl eq_refl).

(* Il serait intéressant d'élaborer sur cet exemple pour montrer que l'on
   peut développer un système de tableaux et d'accès dans ces tableaux,
   où l'absence de débordement est vérifiée au moment de la compilation,
   parce que l'on ne manipule jamais de nombres trop grand, garanti par
   typage.  Mais nous n'allons pas continuer dans cette direction.  Un
   exemple pourra être fourni dans les notes de cours. *)

(* Nous allons maintenant voir comment développer des fonctions qui
   travaillent sur les listes.  L'idée de base est que l'on décompose
   le problème en deux cas possible.

   1/ la liste que l'on considère est la liste vide.
   2/ la liste que l'on considère est composée d'un premier élément
      et d'une autre liste pour tout le reste.

   Donc, si l'on considère la liste [1; 2; 3], elle est dans le deuxième
   cas.  Le premier élement est 1 et le reste est la liste [ 2 ; 3].

   La fonction suivante prend un nombre entier en argument et une liste
   d'entiers, elle retourne la somme du premier élement de la liste et
   de l'entier.  Si la liste est vide elle retourne seulement l'entier. *)

Definition add_first_if_possible (n : Z) (l : list Z) :=
  match l with
  | a :: l' => n + a
  | [] => n
  end.

Compute add_first_if_possible 3 [2; 3].

Compute add_first_if_possible 3 [].

(* Si l'on veut extraire d'une liste tous les éléments qui satisfont
  un test, on va devoir continuer le calcul dans le reste de la liste.
  Ceci se fait avec la récursivité.  Notons ici, que la récursivité
  remplace pour nous ce que l'on ferait avec des boucles dans un autre
  langage de programmation. *)

(* Le premier mot-clef pour une fonction récursive est "Fixpoint" *)

Fixpoint select {A : Type} (p : A -> bool) (l : list A) : list A :=
(* In the next line, we explain that the function will work by analyzing
   in which case the input list is. *)
  match l with
(* If the list is non-empty, it has a first element noted called x
   and a tail list called l'.  The next line introduces these two names
   which can be used anywhere in the next line.  *)
  | x :: l' =>
(* In the next line, x is a name for the first element of the list.
   l' is a name for the rest of the list.  This sublist is allowed
   as an argument of the recursive call.  Note that the recursive call
   is written by calling the function select that we are defining just now. *)
   if p x then x :: select p l' else select p l'
(* In the next line, we explain what happens in the case where the list 
   is empty.  Actually no recursive call is possible. *)
  | nil => nil
  end.


