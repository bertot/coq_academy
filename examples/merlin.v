Require Import Arith.Even Bool List Omega.

(* Case blanche représentée par false et noire par true *)
Definition plateau : Type :=
  (bool * bool * bool * bool * bool * bool * bool * bool * bool)%type.

Inductive coord_aux : Type := A | B | C.

Definition coord : Type := (coord_aux * coord_aux)%type.

Definition partie := list coord.

Definition applique_clic (c : coord) (pl : plateau) : plateau :=
  match pl with (a11, a12, a13,
                 a21, a22, a23,
                 a31, a32, a33) =>
    match c with
      | (A, A) =>
        (negb a11, negb a12, a13,
         negb a21, negb a22, a23,
         a31     , a32     , a33)
      | (A, B) =>
        (negb a11, negb a12, negb a13,
         a21     , a22     , a23,
         a31     , a32     , a33)
      | (A, C) =>
        (a11     , negb a12, negb a13,
         a21     , negb a22, negb a23,
         a31     , a32     , a33)
      | (B, A) =>
        (negb a11 , a12     , a13,
         negb a21 , a22     , a23,
         negb a31 , a32     , a33)
      | (B, B) =>
        (a11      , negb a12, a13,
         negb a21 , negb a22, negb a23,
         a31      , negb a32, a33)
      | (B, C) =>
        (a11      , a12     , negb a13,
         a21      , a22     , negb a23,
         a31      , a32     , negb a33)
      | (C, A) =>
        (a11     , a12     , a13,
         negb a21, negb a22, a23,
         negb a31, negb a32, a33)
      | (C, B) =>
        (a11     , a12     , a13,
         a21     , a22     , a23,
         negb a31, negb a32, negb a33)
      | (C, C) =>
        (a11     , a12     , a13,
         a21     , negb a22, negb a23,
         a31     , negb a32, negb a33)
    end
  end.

Definition applique_partie (pa : partie) (pl : plateau) : plateau :=
  fold_left (fun p c => applique_clic c p) pa pl.

Definition plateau_init_test : plateau :=
  (false, false, false,
   false, false, false,
   false, false, false).

Definition plateau_gagnant (pl : plateau) : Prop :=
  pl = (true, true, true,
        true, true, true,
        true, true, true).

Definition partie_gagnante (pa : partie) (pl : plateau) : Prop :=
  plateau_gagnant (applique_partie pa pl).

Theorem exists_partie_gagnante_pour_init_test :
  exists pa, partie_gagnante pa plateau_init_test.
Proof.
exists ((A, A) :: (A, C) :: (B, B) :: (C, A) :: (C, C) :: nil).
unfold partie_gagnante, plateau_gagnant; simpl; reflexivity.
Qed.

Lemma clic_involutif (c : coord) (pl : plateau) :
  applique_clic c (applique_clic c pl) = pl.
Proof.
destruct c as [x y]; destruct x; destruct y;
repeat (destruct pl as [pl ?]); simpl; rewrite ?negb_involutive; reflexivity.
Qed.

Lemma simpl_clic_involutif (c : coord) (pa : partie) (pl : plateau) :
  applique_partie (c :: c:: pa) pl = applique_partie pa pl.
Proof. unfold applique_partie; simpl; rewrite clic_involutif; reflexivity. Qed.

Lemma clic_commutatif (c1 c2 :coord) (pl : plateau) :
  applique_clic c1 (applique_clic c2 pl) =
  applique_clic c2 (applique_clic c1 pl).
Proof.
destruct c1 as [x1 y1]; destruct x1; destruct y1;
destruct c2 as [x2 y2]; destruct x2; destruct y2;
repeat (destruct pl as [pl ?]); simpl; reflexivity.
Qed.

Lemma clic_commutatif_partie (c1 c2 : coord) (pa : partie) (pl : plateau) :
  applique_partie (c1 :: c2 :: pa) pl = applique_partie (c2 :: c1 :: pa) pl.
Proof. unfold applique_partie; simpl; rewrite clic_commutatif; reflexivity. Qed.

Definition change_une_coord (c : coord) : partie :=
  match c with
    | (A, A) =>
      (A, A) :: (A, B) :: (A, C) :: (B, A) :: (B, B) :: (C, A) :: nil
    | (A, B) =>
      (A, B) :: (B, A) :: (B, C) :: (C, A) :: (C, C) :: nil
    | (A, C) =>
      (A, A) :: (A, B) :: (A, C) :: (B, B) :: (B, C) :: (C, C) :: nil
    | (B, A) =>
      (A, B) :: (A, C) :: (B, A) :: (C, B) :: (C, C) :: nil
    | (B, B) =>
      (A, B) :: (B, A) :: (B, B) :: (B, C) :: (C, B) :: nil
    | (B, C) =>
      (A, A) :: (A, B) :: (B, C) :: (C, A) :: (C, B) :: nil
    | (C, A) =>
      (A, A) :: (B, A) :: (B, B) :: (C, A) :: (C, B) :: (C, C) :: nil
    | (C, B) =>
      (A, A) :: (A, C) :: (B, A) :: (B, C) :: (C, B) :: nil
    | (C, C) =>
      (A, C) :: (B, B) :: (B, C) :: (C, A) :: (C, B) :: (C, C) :: nil
  end.

Definition change_case (c : coord) (pl : plateau) : plateau :=
  match pl with (a11, a12, a13,
                 a21, a22, a23,
                 a31, a32, a33) =>
    match c with
      | (A, A) =>
        (negb a11, a12     , a13,
         a21     , a22     , a23,
         a31     , a32     , a33)
      | (A, B) =>
        (a11     , negb a12, a13,
         a21     , a22     , a23,
         a31     , a32     , a33)
      | (A, C) =>
        (a11     , a12     , negb a13,
         a21     , a22     , a23,
         a31     , a32     , a33)
      | (B, A) =>
        (a11     , a12     , a13,
         negb a21, a22     , a23,
         a31     , a32     , a33)
      | (B, B) =>
        (a11     , a12     , a13,
         a21     , negb a22, a23,
         a31     , a32     , a33)
      | (B, C) =>
        (a11     , a12     , a13,
         a21     , a22     , negb a23,
         a31     , a32     , a33)
      | (C, A) =>
        (a11     , a12     , a13,
         a21     , a22     , a23,
         negb a31, a32     , a33)
      | (C, B) =>
        (a11     , a12     , a13,
         a21     , a22     , a23,
         a31     , negb a32, a33)
      | (C, C) =>
        (a11     , a12     , a13,
         a21     , a22     , a23,
         a31     , a32     , negb a33)
    end
  end.

Lemma change_une_coord_correct (c : coord) (pl : plateau) :
   applique_partie (change_une_coord c) pl = change_case c pl.
Proof.
destruct c as [x y]; destruct x; destruct y;
repeat (destruct pl as [pl ?]); simpl; rewrite ?negb_involutive; reflexivity.
Qed.

Definition liste_false (pl : plateau) : partie :=
  match pl with (a11, a12, a13,
                 a21, a22, a23,
                 a31, a32, a33) =>
  (if a11 then nil else (A, A) :: nil) ++
  (if a12 then nil else (A, B) :: nil) ++
  (if a13 then nil else (A, C) :: nil) ++
  (if a21 then nil else (B, A) :: nil) ++
  (if a22 then nil else (B, B) :: nil) ++
  (if a23 then nil else (B, C) :: nil) ++
  (if a31 then nil else (C, A) :: nil) ++
  (if a32 then nil else (C, B) :: nil) ++
  (if a33 then nil else (C, C) :: nil)
end.

Definition strategie_gagnante (pl : plateau) : partie :=
  fold_left (@app (coord)) (map change_une_coord (liste_false pl)) nil.

Theorem strategie_gagnante_correcte (pl : plateau) :
  partie_gagnante (strategie_gagnante pl) pl.
Proof.
repeat (destruct pl as [pl b]; destruct b); destruct pl; simpl; reflexivity.
Qed.

Lemma coord_eq_dec : forall (x y : coord), {x = y} + {x <> y}.
Proof.
intros [a b] [c d]; destruct a, b, c, d;
(left; reflexivity) || (right; discriminate).
Defined.

Definition if_even_then_none_else_one (c : coord) (pa : partie) : list coord :=
   if even_odd_dec (count_occ coord_eq_dec pa c)
    then nil else c :: nil.

Definition simpl_partie (pa : partie) : partie :=
  (if_even_then_none_else_one (A, A) pa) ++
  (if_even_then_none_else_one (A, B) pa) ++
  (if_even_then_none_else_one (A, C) pa) ++
  (if_even_then_none_else_one (B, A) pa) ++
  (if_even_then_none_else_one (B, B) pa) ++
  (if_even_then_none_else_one (B, C) pa) ++
  (if_even_then_none_else_one (C, A) pa) ++
  (if_even_then_none_else_one (C, B) pa) ++
  (if_even_then_none_else_one (C, C) pa).

Definition simpl_strategie_gagnante (pl : plateau) : partie :=
  simpl_partie (strategie_gagnante pl).

Lemma strategie_optimale_correcte (pl : plateau) :
 partie_gagnante (simpl_strategie_gagnante pl) pl.
Proof.
repeat (destruct pl as [pl b]; destruct b); destruct pl;
unfold partie_gagnante, simpl_strategie_gagnante, strategie_gagnante;
unfold simpl_partie, if_even_then_none_else_one; simpl; reflexivity.
Qed.

Lemma if_even_then_none_else_one_max_one (c : coord) (pa : partie) :
  length (if_even_then_none_else_one c pa) <= 1.
intros. unfold if_even_then_none_else_one; case even_odd_dec; simpl; auto. Qed.

Lemma simpl_partiee_max_9 (pa : partie) :
  length (simpl_partie pa) <= 9.
Proof.
unfold simpl_partie; repeat (rewrite app_length).
assert (T1 := if_even_then_none_else_one_max_one (A, A) pa).
assert (T2 := if_even_then_none_else_one_max_one (A, B) pa).
assert (T3 := if_even_then_none_else_one_max_one (A, C) pa).
assert (T4 := if_even_then_none_else_one_max_one (B, A) pa).
assert (T5 := if_even_then_none_else_one_max_one (B, B) pa).
assert (T6 := if_even_then_none_else_one_max_one (B, C) pa).
assert (T7 := if_even_then_none_else_one_max_one (C, A) pa).
assert (T8 := if_even_then_none_else_one_max_one (C, B) pa).
assert (T9 := if_even_then_none_else_one_max_one (C, C) pa).
omega.
Qed.

Lemma strategie_max_9 (pl : plateau) :
 exists pa, partie_gagnante pa pl /\ length pa <= 9.
Proof.
exists (simpl_strategie_gagnante pl).
split; [apply strategie_optimale_correcte|apply simpl_partiee_max_9].
Qed.