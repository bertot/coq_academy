From SimpleIO Require Import SimpleIO.
Require Import ExtrOcamlIntConv.

Require Import ZArith List random_generator Strings.String.

Import IO.Notations.

Fixpoint print_list_Z (l : list Z) : IO unit :=
  match l with
  | a :: b :: tl =>
    print_int (int_of_z ((a / 4) mod 16000)) ;;
    print_string " "%string;;
    print_int (int_of_z ((b / 7) mod 9000)) ;;
    print_endline ""%string;;
    print_list_Z tl
  | _ => IO.ret tt
  end.

Definition main : io_unit := IO.unsafe_run (print_list_Z nums).

Separate Extraction main.
