Require Import List.

(* Two lists are equal when:
   1/ they have the same length
   2/ the function nth returns the same result on both lists
      for any index smaller than the length of both lists

   State and prove a theorem expressing this. *)

Lemma eq_from_nth_1 {A : Type} (l1 l2 : list A) :
  length l1 = length l2 ->
  (forall i e, i < length l1 -> nth i l1 e = nth i l2 e) -> l1 = l2.
Proof.
revert l2; induction l1 as [ | a l1 IH]; intros [ | b l2].
- easy.
- easy.
- easy.
- simpl.
  intros slen.
  assert (len : length l1 = length l2) by (injection slen; easy).
  intros cnd.
  assert (cnd0 := cnd 0 a); simpl in cnd0.
  rewrite cnd0; [ | now apply PeanoNat.Nat.lt_0_succ].
  rewrite (IH l2).
      easy.
    easy.
  intros i e iltl.
  assert (ci : S i < S (length l1)).
    now apply Lt.lt_n_S.     
  apply (cnd (S i) e).
  assumption.
Qed.

Lemma eq_from_nth {A : Type} (l1 l2 : list A) :
  (length l1 = length l2 /\ 
   forall i e, i < length l1 -> nth i l1 e = nth i l2 e) <-> l1 = l2.
Proof.
split.
  intros [len cnd].
  apply eq_from_nth_1; assumption.
now intros lq; rewrite lq; auto.
Qed.
