Require Import ZArith_base.
Require Import ZArithRing.
Require Import Zcomplements.
Require Import Zdiv.
Require Import Wf_nat.
Require Import Coq.ZArith.Znumtheory.

Open Scope Z_scope.

Definition prime p := 1 < p /\ (forall n, 1 < n < p -> ~ (n | p)).

Definition prime' p :=
  1 < p /\ (forall n, (1 < n /\ (n * n) <= p) -> ~ (n | p)).

Theorem prime_alt : forall p, prime' p <-> prime p.
Proof.
intro p; split; intros [Hp H]; split; auto; intros n [HLn HGn] HF.

  {
  destruct HF as [m Hm]; assert (HLm: 0 < m)
    by (apply Zmult_lt_0_reg_r with n; omega).
  apply Z.le_succ_l in HLm; rewrite <- Z.one_succ in HLm.
  apply Zle_lt_or_eq in HLm; destruct HLm as [HLm|HF];
  [|apply (Z.lt_irrefl n); revert HGn; rewrite Hm;
    rewrite <- HF; rewrite Z.mul_1_l; auto].
  cut (m * m <= p \/ n * n <= p);
  [intros [|]; [apply (H m)|apply (H n)];
   [|exists n; rewrite Z.mul_comm| |exists m]; auto; split; auto|].
  destruct (Z.lt_ge_cases p (n * n)) as [|]; [|auto].
  destruct (Z.lt_ge_cases p (m * m)) as [|]; [exfalso|auto].
  assert (HF : p * p < m * m * (n * n))
    by (apply Zmult_lt_compat; split; auto; apply Z.lt_le_incl; omega).
  apply (Z.lt_irrefl (p * p)); revert HF; cut (m * m * (n * n) = p * p);
  [intro HE; rewrite <- HE; auto|cut (m * m * (n * n) = (m * n) * (m * n))];
  [intro HE; rewrite HE; rewrite <- Hm; auto|ring].
  }

  {
  apply (H n); [split; [auto|]|auto].
  apply Z.square_lt_simpl_nonneg; try omega.
  cut ((n * n) * 1 < p * p); [rewrite Z.mul_1_r; auto|].
  apply Zmult_lt_compat2; split; auto; [|apply Z.lt_0_1].
  apply Z.lt_trans with (1 * 1); [omega|].
  apply Z.square_lt_mono_nonneg; [apply Z.le_0_1|auto].
  }
Qed.

Definition prime'' p :=
  1 < p /\ (forall n, (1 < n /\ (n * n) < p) -> ~ (n | p)).

Theorem prime_alt_wrong : ~ (forall p, prime'' p <-> prime p).
Proof.
intro HF. cut (~ (prime'' 4 <-> prime 4)); [intro H; apply H, HF|clear HF].
cut (prime'' 4); [cut (~ prime 4); [intuition|]|];
[intros H; apply Znumtheory.prime_alt in H; case (prime_divisors _ H 2);
 auto with zarith; exists 2; ring|].
split; [omega|intros n [L G]]; exfalso; cut (~ (1 < n /\ n * n < 4)); [auto|].
clear L G; cut (forall n, 0 < n -> n * n < 4 -> n = 1);
[|intros; apply Z.le_antisymm; [apply Zlt_succ_le, Zlt_square_simpl|]; omega].
intros H [HF HE]; apply H in HE; omega.
Qed.