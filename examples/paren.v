Require Import String Ascii.

Open Scope string_scope.

Inductive parenthesis_free : string -> Prop :=
  pf_nil : parenthesis_free EmptyString
| pf_char : forall (a : Ascii.ascii) s,
      ~(a = "("%char \/ a = ")"%char) -> parenthesis_free s ->
      parenthesis_free (String a s).

Inductive parenthesized : string -> Prop :=
  noparen : forall s,
        parenthesis_free s -> parenthesized s
| paren_wrap : forall s,
    parenthesized s ->
    parenthesized ("(" ++ s ++ ")")
| paren_concat : forall s1 s2,
    parenthesized s1 -> parenthesized s2 ->
    parenthesized (s1 ++ s2).

Lemma parenthesized_example : parenthesized "(alice)((bob))".
Proof.
apply (paren_concat "(alice)" "((bob))").
  apply (paren_wrap "alice").
  apply noparen.
  repeat constructor; intros [|]; discriminate.
apply (paren_wrap "(bob)").
apply (paren_wrap "bob").
repeat constructor; intros [|]; discriminate.
Qed.

Lemma not_paren_example : ~ parenthesized "(".
Proof.
assert (forall s, parenthesized s -> ~ s = "(").
  induction 1 as [s | s IH | s1 s2 s1p IH1 s2p IH2].
      intros so; rewrite so in H; inversion H.
      apply H2; left; easy.
    destruct s; discriminate.
  destruct s1 as [ | a [ | b  s1]]; simpl; [easy | | ].
  destruct s2 as [ | c s2]; [ easy | discriminate].
  discriminate.
intros abs; apply (H _ abs); easy.
Qed.
