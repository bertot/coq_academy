Lemma Q1 : forall P : Prop, P -> P.
Proof. ... Qed.

Lemma Q2 : forall P Q : Prop, P -> Q -> P /\ Q.
Proof. ... Qed.

Lemma Q3 : forall P Q : Prop, P -> P \/ Q.
Proof. ... Qed.

Lemma Q4 : forall P Q : Prop, Q -> P \/ Q.
Proof. ... Qed.

Lemma Q5 : forall P Q : Prop, P /\ Q -> Q /\ P.
Proof. ... Qed.

Lemma Q6 : forall P Q : Prop, P \/ Q -> Q \/ P.
Proof. ... Qed.

Lemma Q7 : forall P Q R : Prop, (P -> Q) -> (Q -> R) -> (P -> R).
Proof. ... Qed.

Lemma Q8 : forall P Q : Prop, ~ ((P -> Q) -> Q) -> ~ (P \/ Q).
Proof. ... Qed.

Lemma Q9 : forall P Q R : Prop, (P -> Q -> R) <-> ((P /\ Q) -> R).
Proof. ... Qed.

Require Import Classical.

Lemma Q10 : forall P Q : Prop, ((P -> Q) -> P) -> P.
Proof. 
(* destruct (classic A). permet de raisonner
   en supposant A dans un premier sous but et ~ A dans un second *)
...
Qed.