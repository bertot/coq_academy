Lemma Q1 : forall P : Prop, P -> P.
Proof. intro; easy. Qed.

Lemma Q2 : forall P Q : Prop, P -> Q -> P /\ Q.
Proof. intros P Q HP HQ; split; easy. Qed.

Lemma Q3 : forall P Q : Prop, P -> P \/ Q.
Proof. intros P Q HP; left; easy. Qed.

Lemma Q4 : forall P Q : Prop, Q -> P \/ Q.
Proof. intros P Q HQ; right; easy. Qed.

Lemma Q5 : forall P Q : Prop, P /\ Q -> Q /\ P.
Proof. intros P Q [HP HQ]; split; easy. Qed.

Lemma Q6 : forall P Q : Prop, P \/ Q -> Q \/ P.
Proof. intros P Q [HP|HQ]; [right|left]; easy. Qed.

Lemma Q7 : forall P Q R : Prop, (P -> Q) -> (Q -> R) -> (P -> R).
Proof. intros P Q R HPQ HQR HP; exact (HQR (HPQ HP)). Qed.

Lemma Q8 : forall P Q : Prop, ~ ((P -> Q) -> Q) -> ~ (P \/ Q).
Proof.
assert (HOr : forall P Q, P \/ Q -> ((P -> Q) -> Q))
  by (intros P Q [HP|HQ] HPQ; [exact (HPQ HP)|exact HQ]).
assert (HC : forall P Q : Prop, (P -> Q) -> (~ Q -> ~ P))
  by (intros P Q HPQ HNQ HP; apply HNQ; exact (HPQ HP)).
intros P Q; apply HC, HOr.
Qed.

Lemma Q9 : forall P Q R : Prop, (P -> Q -> R) <-> ((P /\ Q) -> R).
Proof.
intros P Q R; split; [intros HPQR [HP HQ]; apply HPQR; easy|].
intros HPQR HP HQ; apply HPQR; split; easy.
Qed.

Require Import Classical.

Lemma Q10 : forall P Q : Prop, ((P -> Q) -> P) -> P.
Proof.
intros P Q HPQ; elim (classic P); [easy|intro HNP].
apply HPQ; intro HP; exfalso; apply HNP; easy.
Qed.