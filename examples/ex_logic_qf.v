Lemma E1 : forall (T : Type) (A B C : T -> Prop),
  (exists x, A x /\ (B x \/ C x)) -> exists x, (A x /\ B x) \/ (A x /\ C x).
Proof. ... Qed.

Lemma E2 : forall (T : Type) (A B : T -> Prop),
  (exists x, A x /\ B x) -> ~ (forall x, ~ A x \/ ~ B x).
Proof.
...
assert (HDMQ : forall P : T -> Prop, (exists x, P x) -> ~ (forall x, ~ P x)).
...
assert (HNFx : ~ (forall x, ~ (A x /\ B x))).
...
assert (HDMO : ~ A x \/ ~ B x -> ~ (A x /\ B x)).
...
Qed.

Require Import Classical.

Lemma E3 : forall (T : Type) (P : T -> Prop), ~ (forall x, P x) -> exists x, ~ P x.
Proof.
(* destruct (classic P). permet de raisonner
   en supposant P dans un premier sous but et ~ P dans un second *)
...
Qed.