Require Import List Arith.

Fixpoint mulsl (n : nat) (l : list nat) :=
  match l with
  | a :: l1 => a * (n + 1) :: mulsl (n + 1) l1
  | nil => nil
  end.

Lemma mulsl_injective n l1 l2 : mulsl n l1 = mulsl n l2 -> l1 = l2.
Proof.
(*
unfold mulsl.
(fix mulsl (n : ℕ) (l : list ℕ) {struct l} : list ℕ := match l with
| nil => nil
| a :: l1 => a * (n + 1) :: mulsl (n + 1) l1
end) n l1 =
(fix mulsl (n : ℕ) (l : list ℕ) {struct l} : list ℕ := match l with
| nil => nil
| a :: l1 => a * (n + 1) :: mulsl (n + 1) l1
end) n l2 →
l1 = l2
*)
(*
destruct l1 as [|a l1].
mulsl n nil = mulsl n l2 → nil = l2
mulsl n (a :: l1) = mulsl n l2 → a :: l1 = l2
*)
(*
induction l1 as [|a l1 IH].
IHmulsl n l1 = mulsl n l2 → l1 = l2
*)
revert n l2; induction l1 as [|a l1 IH].
  intros n [| b l2].
    intros; reflexivity.
  simpl; discriminate.
intros n [|b l2]; simpl.
  discriminate.
intros Heq; injection Heq as heads tails.
replace l2 with l1.
  Search (_ * ?x = _ * ?x).
  rewrite Nat.mul_cancel_r in heads.
    rewrite heads; easy.
  rewrite Nat.add_1_r; easy.
apply IH with (n := n + 1).
exact tails.
Qed.