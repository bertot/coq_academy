Require Import Arith List Bool.

Definition bool_prime n : bool :=
  (2 <=? n) && forallb (fun k => negb (n mod k =? 0)) (seq 2 (n - 2)).

Definition prime n : Prop :=
  (2 <= n) /\ forall k l,  (k <> 1 /\ k <> n) -> n <> k * l.

Lemma bool_primeP n : bool_prime n = true <-> prime n.
Proof.
split.
  unfold bool_prime.
  rewrite andb_true_iff, forallb_forall; intros [nge2 nodiv].
  split.
    now apply leb_complete.
  intros k l [not1 notk] abs.
  assert (kn0 : k <> 0).
    now intros absk; revert nge2; rewrite abs, absk.
  assert (abs' : negb (n mod k =? 0) = false).
    now rewrite abs, Nat.mul_comm, Nat.mod_mul, Nat.eqb_refl.
  revert abs'; rewrite nodiv;[easy | ].
  rewrite in_seq.
  rewrite le_plus_minus_r;[ | rewrite leb_complete; auto].
  assert (lgt1 : 2 <= l).
    destruct l as [ | [ | l']].
        revert nge2; rewrite abs, Nat.mul_0_r; simpl; discriminate.
      destruct notk.
      now rewrite abs, Nat.mul_1_r.
    now repeat apply le_n_S; apply Nat.le_0_l.
  split.
    rewrite Nat.le_succ_l, Nat.le_neq; split;[ | apply not_eq_sym; auto].
    rewrite Nat.le_succ_l, Nat.le_neq; split; [ | apply not_eq_sym; auto].
    now auto with arith.          
  rewrite abs; apply Nat.lt_le_trans with (k * 2).
    replace k with (k * 1) at 1 by ring.
    apply Nat.mul_lt_mono_pos_l.
      now apply Nat.neq_0_lt_0.
    now auto with arith.
  now apply Nat.mul_le_mono_l.
intros [nge2 nodiv].
unfold bool_prime; rewrite andb_true_iff, forallb_forall.
split.
  now apply leb_correct.
intros x; rewrite in_seq, le_plus_minus_r; auto.
intros intx.
rewrite negb_true_iff, Nat.eqb_neq; intros abs.
assert (xneq : x <> 1 /\ x <> n).
  split;[ apply not_eq_sym | ]; apply Nat.lt_neq; [ | tauto].
  apply Nat.lt_le_trans with (2 := proj1 intx).  
  now auto with arith.
apply (nodiv x (n / x) xneq).
rewrite (Nat.div_mod  n x) at 1.
  now rewrite abs, Nat.add_0_r.
intros abs'; destruct intx as [it _]; revert it; rewrite <- Nat.leb_le.
now rewrite abs'.
Qed.

Check proj1 (bool_primeP 233) eq_refl.

Fail Check proj1 (bool_primeP 237) eq_refl.

Check proj1 (bool_primeP 237).
