Lemma E1 : forall (T : Type) (A B C : T -> Prop),
  (exists x, A x /\ (B x \/ C x)) -> exists x, (A x /\ B x) \/ (A x /\ C x).
Proof.
intros T A B C Hx.
destruct Hx as [x Hx].
exists x.
destruct Hx as [HAx HBxoCx].
destruct HBxoCx as [HBx|HCx].
  left.
  split.
    assumption.
  assumption.
right.
split.
  assumption.
assumption.
Qed.

Lemma E2 : forall (T : Type) (A B : T -> Prop),
  (exists x, A x /\ B x) -> ~ (forall x, ~ A x \/ ~ B x).
Proof.
intros T A B.
assert (HDMQ : forall P : T -> Prop, (exists x, P x) -> ~ (forall x, ~ P x)).
  intros P Hx.
  destruct Hx as [x HPx].
  intro HF.
  assert (HNPx : ~ P x) by apply HF.
  case HNPx.
  assumption.
intro HEx.
assert (HNFx : ~ (forall x, ~ (A x /\ B x))).
  apply HDMQ.
  assumption.
intro HF.
case HNFx.
intro x.
assert (HDMO : ~ A x \/ ~ B x -> ~ (A x /\ B x)).
  intro HNAoNB.
  intro HAaB.
  destruct HAaB as [HA HB].
  destruct HNAoNB as [HNA|HNB].
    case HNA.
    assumption.
  case HNB; assumption.
apply HDMO.
apply HF.
Qed.

Require Import Classical.

Lemma E3 : forall (T : Type) (P : T -> Prop), ~ (forall x, P x) -> exists x, ~ P x.
Proof.
intros T P HNFPx.
elim (classic (exists x, ~ P x)); [easy|intro HENPx].
case HNFPx; intro x.
elim (classic (P x)); [easy|intro HNPx].
case HENPx; exists x; assumption.
Qed.