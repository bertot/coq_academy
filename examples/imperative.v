Require Import ZArith String Recdef Lia.
Require Import OrderedTypeEx.
Require Import FMapAVL.

Open Scope string_scope.
Open Scope Z_scope.

Function loop (p : Z * Z) {measure (fun x => Z.to_nat (fst x))} :=
  let '(i, s) := p in
  if (0 <? i) then loop (i - 1, s + (i - 1))
  else p.
intros [i s] i0 s0 h; injection h; intros; subst i0 s0.
rewrite Z.ltb_lt in teq0.
simpl; apply Z2Nat.inj_lt; lia.
Qed.

Module maps := FMapAVL.Make(String_as_OT).

Section state_map_example.

(* This example is both simpler and more complex than monad examples
  available in general purpose libraries.

  The added complexity comes from the fact that we introduce a specific
  function for assigning one variable

  I used coq-ext-lib as an guideline, but monads can also be found
  in CompCert or Iris. *)

(* This function introduces a little level of dynamic error catching, to
  make the explanation easier.  In a serious development, we could rather
  exploit the option type to work with a state-and-error monad. *)
Definition catch {A} (def : A) (s : option A) :=
  match s with None => def | Some v => v end.

Definition ret : forall {t : Type}, t -> maps.t Z -> (t * maps.t Z) :=
  fun t v s => (v, s).

Definition bind {t u : Type} (s1 : maps.t Z -> t * maps.t Z)
  (f : t -> maps.t Z -> u * maps.t Z) : maps.t Z -> u * maps.t Z :=
  fun m => let (v, m') := s1 m in f v m'.

Definition assign (s : string) (v : Z) : maps.t Z -> unit * maps.t Z :=
  fun m => (tt, maps.add s v m).

Notation "X := ! s ;; e" :=
   (bind (fun m => (catch 0 (maps.find s m), m))
     (fun X => e))
   (at level 61, s at next level, right associativity).

Notation "s <<= v ;; e" :=
   (bind (assign s v) (fun _ => e))
  (at level 61, v at next level, right associativity).
  
(* In this treatment of the loop body, the read access to variables
  is done before executing the expression.  Actually, we did not
  introduce (! s) as a regular expression. *)
Definition loop_body (s : maps.t Z) : (bool * maps.t Z) :=
    (v := ! "i" ;;
    if 0 <? v then
       "i" <<= v - 1 ;;
       v1 := ! "i" ;;
       vs := ! "s" ;;
       "s" <<= vs + v1 ;; ret true
    else
      ret false) s.

Function loop1 (s : maps.t Z) 
  {measure (fun s' => Z.to_nat (catch 0 (maps.find "i" s')))} : maps.t Z :=
  match loop_body s with
  | (true, s') => loop1 s'
  | (false, s') => s'
  end.
Proof.
intros s b s' _; unfold loop_body, bind, assign, ret.
set (v := catch 0 (maps.find "i" s)).
destruct (0 <? v) eqn:test_eq;[ | discriminate].
rewrite Z.ltb_lt in test_eq.
intros h; injection h; clear h; intros new_mem; subst s'.
set (new_v := maps.find _ _).
assert (new_v_eq : new_v = Some (v - 1)).
  unfold new_v; apply maps.find_1.
  apply maps.add_2;[discriminate | now apply maps.add_1].
rewrite new_v_eq; simpl; apply Z2Nat.inj_lt; lia.
Qed.

Section state_monad_example.

Lemma example_property_of_loop :
  forall n m, 0 <= n -> 2 * (snd (loop (n, m)) - m) = n * (n - 1).
Proof.
assert (main : forall p, 
          0 <= fst p -> 2 * snd (loop p) = (fst p) * (fst p - 1) + 2 * snd p).
  intros p; functional induction (loop p); simpl fst; simpl snd; intros ige0.
    rewrite Z.ltb_lt in e0.
    rewrite IHp0;simpl fst; simpl snd;[ring | lia].
  rewrite Z.ltb_ge in e0; assert (i0 : i = 0) by lia.
  rewrite i0; ring.
intros n m nge0.
rewrite Z.mul_sub_distr_l, main;[simpl fst; simpl snd; ring | exact nge0].
Qed.
