\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{multirow}
\usepackage{alltt}
\newcommand{\coqand}{{\tt/\char'134}}
\newcommand{\coqor}{{\tt\char'134/}}
\newcommand{\coqnot}{\mbox{\~{}}}
\usepackage{color}
\setbeamertemplate{footline}[frame number]
\title{Reasoning about imperative programs}
\author{Yves Bertot and Pierre Boutry}
\date{March 2024}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Bridging the divide between imperative and functional programming}
\begin{itemize}
\item Imperative programs have assignment and loops
\item Functional programs have local variables and recursion
\begin{itemize}
\item This covers some of the distance
\item Remaining difficulty is termination
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Imperative-functional correspondance (take 1)}
\begin{columns}
\column{0.25\textwidth}
\begin{small}
\begin{alltt}
while (0 < i) \{
  i = i - 1;
  s = s + i
\}
\end{alltt}
\end{small}
\column{0.7\textwidth}
\begin{small}
\begin{alltt}
Function loop (p : Z * Z)
   \{measure 
       (fun x => Z.to_nat (fst x))\} :=
  let '(i, s) := p in
  if (0 <? i) then
    loop (i - 1, s + (i - 1))
  else p.
Proof. (termination needs a proof. *)
\end{alltt}
\end{small}
\end{columns}
\end{frame}
\begin{frame}[fragile]
\frametitle{Proof of termination}
The termination proof is simple in this case
\begin{itemize}
\item The test must be transformed into an arithmetic property
\item A change of number type is needed
\end{itemize}
\begin{alltt}
Proof.
intros [i s] i0 s0 h; injection h; intros; subst i0 s0.
rewrite Z.ltb_lt in teq0.
simpl; apply Z2Nat.inj_lt; lia.
Qed.
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Lessons learned}
\begin{itemize}
\item Make the state of the program explicit
\begin{itemize}
\item  Tuple of all variables modified in the loop
\end{itemize}
\item Use arguments in recursive calls to represent update
\item Note that compilation of terminal recursion leads to imperative code
\item The Coq code includes a proof of termination that was not present
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Monadic reasoning (take 2)}
\begin{itemize}
\item Reasoning with side-effects is a well studied topic
\item Notations can be included to hide the way the state is handled
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example notation for the loop body}
\begin{alltt}
    (v := ! "i" ;;
    if 0 <? v then
       "i" <<= v - 1 ;;
       v1 := ! "i" ;;
       vs := ! "s" ;;
       "s" <<= vs + v1 ;; ret true
    else
      ret false)
\end{alltt}
\begin{itemize}
\item Notation {\tt v := !~"i" ;;} is used to represent read access
\item Notation {\tt "i" <<= v - 1 ;;} is used to represent write access
\item The value {\tt ret true} or {\tt ret false} is used to
      tell when code should repeat the loop
\item These notations were invented when preparing these slides
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{More systematic approach: CompCert + VST (take 3)}
\begin{itemize}
\item Previous approaches rely on an approximate understanding of C
\item CompCert has a more comprehensive description of C
\item With Hoare logic, we prove correspondence between C program and Coq function
\item Other extensions also make it possible to address concurrent and
distributed programming
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Coq verification of C programs}
\begin{itemize}
\item Abstract models of algorithm make unrealistic assumptions
\begin{itemize}
\item Infinite size of memory
\item Exact real arithmetic
\end{itemize}
\item More precise models are possible, safeguarding against more bugs
\begin{itemize}
\item Precise description of C programming language semantics
\item Consistency with C compiler choices
\item Precise description of memory usage
\item Precise description of floating point computation
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{An example : floating point computation of square roots}
\begin{verbatim}
float sqrt_newton(float x)
{
  float y, z;

  if (x <= 0)
    return 0;
  y = x >= 1 ? x : 1;
  do {
    z = y;
    y = (z + x/z)/2;
  } while (y < z);
  return y;
}
\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq model of the main loop behavior}
\begin{verbatim}
Definition main_loop_measure (p : float * float) : nat :=
  float_to_nat (snd p).

Function main_loop (xy : float * float) 
   {measure main_loop_measure} : float :=
    let (x,y) := xy in 
   let z := float_div 
         (float_plus y (float_div x y)) (float_of_Z 2) in 
    if float_cmp Clt z y then  main_loop (x, z) else z.
Proof.
intros; apply float_to_nat_lt; auto.
Qed.
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{Comments on the main function}
\begin{itemize}
\item Most numeric values here are floating point numbers
\item The floating point numbers are in a finite set
\begin{itemize}
\item {\tt float\_to\_nat} counts them in order
\item {\tt float\_to\_nat\_lt} says that {\tt float\_to\_nat} is increasing
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq model of the main loop (second part)}
\begin{verbatim}
Definition fsqrt (x: float) : float :=
  if float_cmp Cle x (float_of_Z 0) 
  then (float_of_Z 0) 
  else
  let y := if float_cmp Cge x (float_of_Z 1)
               then x else float_of_Z 1  in
  main_loop (x,y).
\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{Main mathematical statement about the Coq model}
\begin{verbatim}
Lemma fsqrt_correct:
 forall x, 
  powerRZ 2 (6 - es) <= f2real x < powerRZ 2 (es-3) ->
  Rabs (f2real (fsqrt x) - sqrt (f2real x)) <=
       3 / (powerRZ 2 (ms-1)) * sqrt (f2real x).
\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{Main theorem about {\tt sqrt\_newton}}
\begin{small}
\begin{verbatim}
Definition sqrt_newton_spec2 :=
 DECLARE _sqrt_newton
 WITH x: float32
 PRE [ tfloat ]
     PROP (powerRZ 2 (-122) <= f2real x < powerRZ 2 125)
     PARAMS (Vsingle x)
     SEP ()
  POST [ tfloat ]
     PROP
       (Rabs (f2real (fsqrt x) - sqrt (f2real x)) <=
                 3 / (powerRZ 2 23) * sqrt (f2real x))
     RETURN (Vsingle (fsqrt x))
     SEP ().
...
Lemma body_sqrt_newton2:  semax_body Vprog Gprog f_sqrt_newton
    sqrt_newton_spec2.
Proof.
...
\end{verbatim}
\end{small}
\end{frame}
\begin{frame}
\frametitle{Connecting bits together}
\begin{itemize}
\item {\tt semax\_body} links together a Coq description of
the C program syntax tree and the specification
\end{itemize}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
