\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{alltt}
\newcommand{\coqand}{{\tt/\char'134}}
\newcommand{\coqor}{{\tt\char'134/}}
\usepackage{color}
\setbeamertemplate{footline}[frame number]
\title{Using type theory to develop programs}
\author{Yves Bertot}
\date{March 2024}
\mode<presentation>

\begin{document}

\maketitle

\begin{frame}
\frametitle{Basic principles}

\begin{itemize}
\item Coq provides a language with guaranteed termination
\item Close to OCaml, but recursion is limited
\item Easy to define new data-types
\item Non-terminating programs can also be modeled, but not executed inside Coq
\end{itemize}

\end{frame}

%\begin{frame}
%\frametitle{Types �num�r�s}
%
%\begin{alltt}
%Inductive bool : Set := true | false.
%\end{alltt}
%
%\end{frame}

\begin{frame}
\frametitle{The working environment}
\includegraphics[width=1\textwidth, trim=0 0cm 0 0cm, clip]{../images/Emacs.png}
\end{frame}

\begin{frame}[fragile]
\frametitle{Using pre-defined objects}

\begin{itemize}
\item Coq users have developed libraries about data structures and proof
\item You can use them by simply calling to them
\item For some libraries, you have to download them from a shared package
repository
\end{itemize}
\begin{verbatim}
Require Import ZArith.

Open Scope Z_scope.
\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{Defining data-types}
\begin{itemize}
\item Enumerated types
\item Simple agregates
\item Types with self-reference
\end{itemize}
\begin{alltt}
Inductive season := Spring | Summer | Fall | Winter.

Inductive three_fields :=
  agregate (s : season; size : Z; other : bool).

Inductive tree : Type :=
  leaf (value : Z)
| node (first_child second_child : tree).
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Construct values}
The {\tt Check} command makes it possible to verify that a term is well
formed.  It returns information about its type.
\begin{alltt}
Check season.

Check agregate.

Check agregate Spring.

Check agregate Spring 25.

Check agregate Spring 25 true.

Check node (leaf 25)
\end{alltt}
\end{frame}

\begin{frame}[fragile]
\frametitle{Pattern matching}
Pattern matching makes it possible at the same time:
\begin{itemize}
\item to have different behaviors depending on the data case
\item to access sub-fields of constructs
\end{itemize}
\begin{alltt}
Definition next_season (s : season) : season :=
  match s with
  | String => Summer
  | Summer => Fall
  | Fall => Winter
  | Winter => Spring
  end.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Pattern matching (continued)}
\begin{alltt}
Definition new_three_fields (a : three_fields) :=
  match a with
  agregate s n b => 
  agregate (next_season s) (n + 1) (negb b)
  end.

Definition is_singleton (t : tree) : bool :=
  match t with
  | leaf _   => true
  | node _ _ => false
  end.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Self reference of types and recursion}
Types with self reference can contain vast amounts of information

Programs operating on these types need some form of repetition

Recursion is the solution, but it is limited to subterms
\begin{verbatim}
Fixpoint sum_tree (t : tree) :=
  match t with
  | leaf v => v
  | node t1 t2 => sum_tree t1 + sum_tree t2
  end.
\end{verbatim}
Here recursive calls are on {\tt t1} and {\tt t2},  obtained in the pattern,
thus subterms of {\tt t}.

The limitation to subterms can be worked around (later in the course)
\end{frame}

\begin{frame}[fragile]
\frametitle{�valuation des fonctions}

\begin{alltt}
Compute next_season Summer.
\textcolor{blue}{     = Fall
     : season}
Compute is_singleton (node (leaf 1) (leaf 2)).
\textcolor{blue}{     = false
     : bool}
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Types polymorphes}

There exists a generic type of lists, with the type of elements
unspecified.

\begin{alltt}\textcolor{blue}{
Inductive list {A : Type} : Type :=
  nil : list A | cons : A -> list A -> list A.}
\end{alltt}
A function computing the number of elements in a list:
\begin{alltt}
Definition len \{A : Type\} : list A -> nat :=
  fix len l :=
    match l with
      | nil _       => O
      | cons _ _ tl => len tl + 1
    end.
\end{alltt}
Note that the type of arguments in the list is irrelevant
\end{frame}

\begin{frame}
\frametitle{Additions to the list definition}
\begin{itemize}
\item Notation {\tt 1 :: 2 :: 3 :: nil}
\item Implicit arguments
\begin{itemize}
\item {\tt nil} is a function that appears to be a constant
\item {\tt cons} is really a 3 argument function, but we give only 2
\item {\tt cons nat 1 (cons nat 2 (cons nat 3 (nil nat)))}
\end{itemize}
\item The command {\tt About} makes it possible to know the implicit arguments
of a function
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Higher order functions}
%\begin{itemize}
%\item
Functions can take functions as arguments

%\item 
This combines very well with polymorphism
%\end{itemize}

For instance, the function {\tt iter} can repeat a function without knowing
its type
\begin{alltt}
About Z.iter.
\textcolor{blue}{Z.iter : Z -> forall A : Type, (A -> A) -> A -> A.
Arguments Z.iter n%Z_scope {A}%type_scope
   f%function_scope x}
Definition fact (x : Z) :=
  snd (Z.iter x (fun p => (fst p + 1, fst p * snd p))
   (1, 1)).
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Creating a higher order function}
\begin{verbatim}
Fixpoint fold {A B : Type}
  (f : A -> B -> A) (a : A) (l : list B) :=
  match l with
    nil => a
  | cons hd tl => fold f (f a hd) tl
  end.

Definition Zlen {B : Type} (l : list B) :=
  fold (fun n v => n + 1) 0 l.
\end{verbatim}

\end{frame}
\begin{frame}
\frametitle{Import et Search}
\begin{itemize}
\item By default, Coq only provides a minimal set of pre-defined functions
\item To work with lists, it is better to import a large library of
predefined functions on lists
\item For some features, more libraries may be needed.
\item Once a library is loaded, {\tt Search} can be used to explore its
 contents
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Example uses of Search}
\begin{itemize}
\item The results of {\tt Search} include functions on a concept and associated
theorems
\item {\tt Search} can receive patterns, strings, and more constraints
\item Without loading the {\tt List} library, {\tt Search list} only gives
induction principles, {\tt length} and {\tt app}
\item After loading, it returns 421 elements
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Search demo}
\begin{alltt}
Require Import List.

(* Find all the functions that return a list *)
Search headconcl:list. 

(* Find all theorems that mention app, but not nat *)
Search app -nat.
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{A naive model of finite sets}
\begin{itemize}
\item We will construct a small library to manipulate sets of integers
\item Each set will be represented by a list of numbers,
\item we will provide 5 basic operations:
\begin{enumerate}
\item Constructing singleton sets
\item Checking whether an integer is in a set (as boolean function)
\item Computing the subset of a set of elements satisfying a certain property
\item Making the intersection of two sets
\item Making the union of two sets
\end{enumerate}
\item We will attempt to guarantee that the lists used to represent set have
no duplications
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\begin{verbatim}
Definition Zfset := list Z.

Definition singleton (x : Z) : Zfset :=
  x :: nil.

Definition mem (x : Z) (S : Zfset) := 
  existsb (fun y => Z.eqb x y) S.

Definition select (S : Zfset) (p : Z -> bool) :=
  filter p S.
  
Definition intersection S1 S2 : Zfset) :=
  filter (fun x => mem x S1) S2.

Definition union (S1 S2 : Zfset) :=
  S1 ++ filter (fun x => negb (mem x S1)) S2.
\end{verbatim}
\end{frame}
\end{document}
