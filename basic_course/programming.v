(* The programming.v file for coq_academy_courses *)

Require Import ZArith List.

Open Scope Z_scope.

Inductive season :=
  Spring | Summer | Fall | Winter.
  
Inductive three_fields :=
  agregate (s : season) (size : Z) (other : bool).
  
Inductive tree : Type :=
  leaf (value : Z) | node (first_child second_child : tree).

Definition next_season (s : season) : season :=
  match s with
  | Spring => Summer
  | Summer => Fall
  | Fall => Winter
  | Winter => Spring
  end.
  
Definition new_three_fields (a : three_fields) :=
  match a with
  agregate s n b => 
  agregate (next_season s) (n + 1) (negb b)
  end.

Definition is_singleton (t : tree) : bool :=
  match t with
  | leaf _   => true
  | node _ _ => false
  end.
Compute new_three_fields (agregate Summer 24 true).

Compute is_singleton (node (leaf 1) (leaf 2)).

Definition fact (x : Z) :=
  snd (Z.iter x (fun p => (fst p + 1, fst p * snd p)) (1, 1)).

Compute fact 15.

Fixpoint fold {A B : Type} (f : A -> B -> A) (a : A) (l : list B) :=
  match l with nil => a | cons hd tl => fold f (f a hd) tl end.

Definition Zlen {B : Type} (l : list B) := fold (fun n v => n + 1) 0 l.

Compute Zlen (1 :: 2 :: 3 :: nil).

Require Import List.

Search app.

Definition Zfset := list Z.

Definition singleton (x : Z) : Zfset :=
  x :: nil.

Definition mem (x : Z) (S : Zfset) := 
  existsb (fun y => Z.eqb x y) S.

Definition select (S : Zfset) (p : Z -> bool) :=
  filter p S.
  
Definition intersection (S1 S2 : Zfset) :=
  filter (fun x => mem x S1) S2.

Definition union (S1 S2 : Zfset) :=
  S1 ++ filter (fun x => negb (mem x S1)) S2.

Compute union (singleton 1) (union (singleton 3) (singleton 1)).

Compute intersection (union (singleton 1) (singleton 2))
           (union (singleton 1) (singleton 3)).
