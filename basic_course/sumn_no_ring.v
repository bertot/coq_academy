Require Import Arith.

Fixpoint sumn (n : nat) : nat :=
  match n with
  | O => 0
  | S p => sumn p + n
  end.
  
Compute (sumn 5, sumn 10).

Lemma sumn_prop : forall n, 2 * sumn n = n * (n + 1).
Proof.
induction n as [ | n Ih].
  easy.
simpl sumn.
rewrite Nat.mul_add_distr_l.
rewrite Ih.
simpl; rewrite !Nat.add_1_r; simpl.
rewrite (Nat.add_comm n (n * S (S n))).
rewrite !Nat.mul_succ_r.
rewrite !Nat.add_succ_r, Nat.add_0_r, !Nat.add_assoc.
easy.
Qed.
