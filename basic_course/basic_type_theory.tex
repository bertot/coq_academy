\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{color}
\setbeamertemplate{footline}[frame number]
\title{Basic principles of type theory}
\author{Yves Bertot}
\date{March 2024}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Safe programming}
We use examples of bugs to illustrate safeguards provided by type theory
\begin{itemize}
\item Two frequent causes of bugs in programming
  \begin{itemize}
  \item Inconsistent manipulation of variants in tree structures
  \item buffer and array overflows
  \end{itemize}
\item Similarity between proofs and programs
\begin{itemize}
\item Inputs and outputs of theorems
\item Construction of proofs as programs
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Tree-like structures}
Trees are a major example of structures using pointers
  \begin{itemize}
  \item A recurrent pattern throughout computer technology
    \begin{itemize}
    \item libraries, books, sentences,
    \item classification, phylogenetics,
    \item computer programs (after parsing)
    \end{itemize}
  \item Even linear arrays can be modeled as tree structures (lists)
  \end{itemize}
The language of Coq supports safe programming with trees
\end{frame}
\begin{frame}
  \frametitle{Tree structures}
  % trim left bottom right top
%  \framebox{
    \includegraphics[width=\textwidth, trim=3cm 15cm 3cm 3cm, clip]{tree.pdf}
%  }
\end{frame}
\begin{frame}
  \frametitle{Tree structures: two cases}
  % trim left bottom right top
%  \framebox{
    \includegraphics[width=\textwidth, trim=3cm 20cm 3cm 3cm, clip]{tree-with-cases.pdf}
%  }
\begin{itemize}
\item source of bugs: act on a leaf as if it was a binary node
\end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Tree structures in memory}
  % trim left bottom right top
%  \framebox{
    \includegraphics[width=\textwidth, trim=0 17cm 0 2cm, clip]{tree_in_memory.pdf}
%  }
\begin{itemize}
\item case information is stored in memory
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Low level access in a tree}
Example function to compute the sum of all numbers in a tree
\begin{small}
\begin{alltt}
/*  1 */ int sum_tree (tree t) \{
/*  2 */   if (key_two_subtrees == t->key) \{ \textcolor{blue}{blue case}
/*  3 */     tree t1 = t->ts.first_child;    \textcolor{blue}{node operations}
/*  4 */     tree t2 = t->ts.second_child;
/*  5 */     return (sum_tree(t1) + sum_tree(t2));
/*  6 */   \} else \{                          \textcolor{red}{red case}
/*  7 */     int value = t->val;             \textcolor{red}{leaf operation}
/*  8 */     return(value);
/*  9 */   \}
/* 10 */ \}
\end{alltt}
\end{small}
\end{frame}
\begin{frame}
\frametitle{Unsafe language}
\begin{itemize}
\item The programming language (here C), does not protect the programmer
\item The compiler does not warn against omitting the test on the {\tt key}
\item Or using {\tt t->ts.first\_child} in the {\tt else} branch
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Pattern-matching solution}
\begin{itemize}
\item Pack test and unsafe operations together
\end{itemize}
\begin{small}
\begin{alltt}
/*  1 */ int sum_tree (tree t) \{
/*  2 */   if (key_two_subtrees == t->key) \{  \textcolor{blue}{/* blue case */}
/*  3 */     tree \textcolor{blue}{t1} = t->ts.first_child;
/*  4 */     tree \textcolor{blue}{t2} = t->ts.second_child;
/*  5 */     return (\textcolor{blue}{sum_tree(t1) + sum_tree(t2)});
/*  6 */   \} else \{
/*  7 */     int \textcolor{red}{value} = t->val;
/*  8 */     return(\textcolor{red}{value});
/*  9 */   \}
/* 10 */ \}
\end{alltt}
\end{small}
\begin{itemize}
\item Operations between lines 3-5 represented as a function
\item This function acts on values provided in variables \textcolor{blue}{\tt t1} and \textcolor{blue}{\tt t2}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Coq syntax solution: accessing content}
\begin{small}
\begin{verbatim}
Inductive tree :=
  node (first_child second_child : tree) | leaf (value : Z).

Fixpoint sum_tree (t : tree) :=
  match t with
  | node t1 t2 => sum_tree(t1) + sum_tree(t2)
  | leaf value => value
  end.
\end{verbatim}
\end{small}
\begin{itemize}
\item Parentheses around {\tt t1} and {\tt t2} are not idiomatic
\end{itemize}
\end{frame}
\begin{frame}
\begin{large}
\begin{center}
Array overflow
\end{center}
\end{large}
\end{frame}
\begin{frame}
\frametitle{Array overflow and remedies}
\begin{itemize}
\item Array overflow is a known cause of bugs
\item Also a source of exploits by hackers
\item Bound checking is costly and programmers avoid it
\item Coq solution: compile-time user-controlled bound checking
\item Use type discipline for this
\end{itemize}
\end{frame}
\begin{frame}[fragile]
  \frametitle{Example use of arrays and bounds}
\begin{small}
\begin{verbatim}
some_type table1[10], table2[10];

if (0 < i < 10) {
  ... table1[i] ...  table2[i]
}
\end{verbatim}
\end{small}
\begin{itemize}
\item A language like Java inserts an extra check when handling {\tt table1[i]}
\item Potentially six tests, only 2 are useful
  \begin{itemize}
  \item compare to 0, then to 10, 3 times
  \end{itemize}
\item Inside braces, {\tt i} is known to be in bounds
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Coq solution: indexed families of types}
\begin{itemize}
\item Use types to register the information
\item {\tt array} types are indexed by their size
\item Programmers can define a type of bounded integers
  {\tt boundZ n} where {\tt n} is an integer,
\item After a test, some values can be cast in type {\tt boundZ n}.
\item Some tests can be made at compile time
\item Some tests are simply not needed
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Programming with integers and bounded integers}
An example on purpose for this course
\begin{small}
\begin{verbatim}
Definition multiple_safe_access {n}
           (table1 table2 : array Z n) (k : Z) : Z :=
  match check_in_bounds n k with
  | Some bk =>  (access table1 bk) + (access table2 bk)
  | None => 0
  end.
\end{verbatim}
\end{small}
The {\tt access} function does not need to perform any tests\\
The {\tt check\_in\_bounds} function returns more than ``yes'' or ``no''\\
{\tt bk} is the same integer but marked as within bounds\\
The failure case must still be covered
\end{frame}
\begin{frame}
\frametitle{The type of functions}
\begin{itemize}
\item A function that consumes arguments of type {\tt T} and produces arguments of type {\tt U} has type\\
\begin{center}
   \tt T -> U
\end{center}
\item A function that consumes two arguments of type {\tt T} has type
\begin{center}
   \tt T -> T -> U
\end{center}
\item Read {\tt a :~T} as ``{\tt a} has type {\tt T}''
\item if {\tt a :~T} and {\tt f :~T -> T -> U} then {\tt f a :~T -> U}
\item With parentheses : {\tt f : T -> (T -> U)} and\\
 {\tt f a a = (f a) a}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
  \frametitle{The type of {\tt multiple\_safe\_access}}
\begin{verbatim}
multiple_safe_access 
  : forall n : nat, array Z n -> array Z n -> Z -> Z
\end{verbatim}
\begin{itemize}
\item A function of 4 arguments,
\item The type of the 2nd and 3rd depend on the value of the first one
\item A {\em dependently typed function}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{First steps in logic}
\begin{large}
\begin{center}
Use of dependent types in logic
\end{center}
\end{large}
\end{frame}
\begin{frame}
  \frametitle{Two basic rules about comparison}
Ad hoc rules for this example
  \begin{itemize}
    \item gt1 : for any {\tt n}, {\tt n < n + 1}
    \item gtadd1 : for any {\tt n} and {\tt m}, if {\tt n < m} then {\tt n < m + 1}
  \end{itemize}
\begin{itemize}
\item {\tt gt1} consumes an integer and produces a {\em proof} that
this number is less than its successor
\item {\tt gtadd1} consumes two integers and a {\em proof} and produces
another proof
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{A tree-like proof that {\tt 3 < 6}}
  % trim left bottom right top
  %\framebox{
    \includegraphics[width=\textwidth, trim=0 8cm 0 13cm, clip]{proof-tree.pdf}
  %}

  In Coq syntax: {\tt gtadd1 3 5 (gtadd1 3 4 (gt1 3))}
\end{frame}
\begin{frame}
\frametitle{The nature of {\tt gtadd1}}
{\tt gtadd1} is several things at the same time
\begin{itemize}
\item Coq function
\begin{itemize}
\item it can be applied to numbers 3, 4, and to a proof of {\tt 3 < 4}
\item it returns a proof of {\tt 3 < 5}
\end{itemize}
\item A theorem, a proof
\begin{itemize}
\item the existence of {\tt gtadd1} shows that the statement
\hbox{\(\forall n~m, n < m \Rightarrow n < m + 1\)} holds
\end{itemize}
\item  the symbols \(\forall\) and \(\Rightarrow\) have a meaning as type
operations
\item The statement {\tt 3 < 5} is the type of proofs
\begin{itemize}
\item Proofs for this statement
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Main lessons}
\begin{enumerate}
\item The syntax of function application
\item The use of pattern-matching and tree-like structures
\item Types can be expressions like {\tt array Z 10} (containing numbers, e.g.)
\item Proofs themselves are tree-like structures
\item Theorems are function consuming data and proofs, and producing proofs
\end{enumerate}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
