Require Import List ZArith Bool Lia.
Require Import Extraction.
Import ListNotations.

Open Scope Z_scope.

(* In this file we observe how the types available in Coq can be used to
   provide safe programming with respect to array bounds. *)

(* We define an array as a list plus some information on its size. *)

Definition array (A : Type) (n : Z) := {l : list A | length l = Z.to_nat n}.

(* For instance, all elements of the type (array Z 10) represent tables of
   length 10, whose elements are in Z.  We wish to show a programming
   discipline that forces us to check that the array is only accessed within
   bounds. *)

(* We define a bounded integer as an integer, plus information that it
   respects bounds. We use natural numbers for the upper bound, to be
   consistent with the fact that array size are naturally expressed with
   natural numbers. *)

Definition boundZ (n : Z) := {k : Z | -1 < k < n}.

(* An interesting property is that if a number is within bound, then its
  half is also within bounds.  There is no need to perform a runtime test
  for this.  *)
Definition b_div2 {n : Z} (k : boundZ n) : boundZ n.
  destruct k as [k bk].
  exists (k / 2).
  assert (tmp1 := Z_div_mod_eq_full k 2).
  assert (tmp2 := Z.mod_pos_bound k 2 eq_refl).
  lia.
Defined.

Check b_div2.

(* Here is an example of a bounded number. *)
Definition sample_1024 : boundZ 1024 := exist _ 873 (conj eq_refl eq_refl).

(* We can check that the division by two on bounded numbers is consistent
  with division by 2 on the integer part of the information. *)
Compute proj1_sig (b_div2 sample_1024).

(* Now if we access an array with an index that is within bound, we are
  sure to be able to return a value. *)
Definition access {A} {n} (table : array A n) (i : boundZ n) : A.
  destruct table as [seq size_seq].
  destruct i as [k bound_k].
  (* we use nth_error, which returns an error condition when there
    is an access beyond array size. *)
  destruct (nth_error seq (Z.to_nat k)) as [a | ] eqn:nth_prop.
    exact a.
  (* The next two lines are the ones expressing that there cannot be
    any error because the integer is within bounds. *)
  rewrite nth_error_None in nth_prop.
  rewrite size_seq in nth_prop; lia.
Defined.

Check access.
(* So if we want to acces an unprotected array with an unprotected
  index, we perform all the checks and record the information that
  checks have been performed using array and boundZ types, we can
  then perform the operations without further run-time checks.

  Of course, the first check may return an error, so an option type
  is used. *)
Definition simple_safe_access (l : list Z) (k : Z) : option Z :=
  match Z_lt_dec (-1) k, Z_lt_dec k (Z.of_nat (length l)) with
    left kpos, left kltl =>
    let bk := exist _ k (conj kpos kltl) : boundZ (Z.of_nat (length l)) in
    let table := exist _ l (eq_sym (Nat2Z.id _)) :
          array Z (Z.of_nat (length l)) in
      Some (access table bk)
  | _, _ => None
  end.

Compute simple_safe_access [0; 1; 2; 3; 4; 5; 6; 7; 8] 8.

(* What is really a plus is that we don't need to pay the cost of
  testing the index for every access.  If we compute new indexes
  that are guaranteed by construction to also be within bounds, then
  only one test on the integer needs to be done, even for 3 different
  array access. *)
Definition multiple_safe_access_in_a_list
   (f : Z -> Z -> Z -> Z) (l : list Z) (k : Z) : option Z :=
  match Z_lt_dec (-1) k, Z_lt_dec k (Z.of_nat (length l)) with
    left kpos, left kltl =>
    let bk := exist _ k (conj kpos kltl) : boundZ (Z.of_nat (length l)) in
    let table := exist _ l (eq_sym (Nat2Z.id _)) : array Z _ in
      Some (f (access table bk) (access table (b_div2 bk))
            (access table (b_div2 (b_div2 bk))))
  | _, _ => None
  end.

Compute multiple_safe_access_in_a_list (fun x y z => x + y + z)
  [0; 1; 2; 3; 4; 5; 6; 7; 8] 8.

Compute multiple_safe_access_in_a_list (fun x y z => x + y + z)
  [0; 1; 2; 3; 4; 5; 6; 7; 8] 6.

Definition mkbound {n} (k : Z) : option (boundZ n) :=
  match Z_lt_dec (-1) k, Z_lt_dec k n with
    left kpos, left kltn => Some (exist _ k (conj kpos kltn))
  | _, _ => None
  end.

Definition check_in_bounds {n} (k : Z) : option (boundZ n) :=
  mkbound k.

Check check_in_bounds.

Definition multiple_safe_access {n}
           (table1 table2 : array Z n) (k : Z) : Z :=
  match check_in_bounds k with
  | Some bk =>  (access table1 bk) + (access table2 bk)
  | None => 0
  end.

Check @multiple_safe_access.

Lemma gt1 : forall n : Z, n < n + 1.
Proof. lia. Qed.

Lemma gtadd1 : forall n m : Z, n < m -> n < m + 1.
Proof. lia. Qed.

Definition bound_2_4 : boundZ 4 :=
  exist _ 2 (conj (rule2 _ _ (rule2 _ _ (rule1 _))) (rule2 _ _ (rule1 _))).

Lemma half_sum_proof {n} (k1 k2 : Z) :
   -1 < k1 < n -> -1 < k2 < n -> -1 < (k1 + k2) / 2 < n.
Proof.
simpl.
assert (tq1 := Z_div_mod_eq (k1 + k2) 2 eq_refl).
assert (tmp := Z.mod_pos_bound (k1 + k2) 2 eq_refl).
lia.
Qed.

Definition half_sum_bound {n} (k1 k2 : boundZ n) : boundZ n :=
  match k1, k2 with
    (exist _ k1 h1), (exist _ k2 h2) =>
    exist _ ((k1 + k2) / 2) (half_sum_proof k1 k2 h1 h2)
  end.

Definition triple_safe_access {n} (table : array Z n) (k1 k2 : boundZ n) : Z :=
  access table k1 +
  access table k2 +
  access table (half_sum_bound k1 k2).

Extraction Inline half_sum_bound.

Extraction triple_safe_access.
