Require Import ZArith Lia.

Lemma example x y :
 2 * x + 1 < y -> 3 * y + 1 < x -> x < 0.
Proof.
intros c1 c2.
rewrite (Nat.mul_lt_mono_pos_l 2); cycle 1.
  apply Nat.lt_0_succ.
rewrite Nat.mul_0_r.
enough (tmp : 2 * x + 1 < 0).
  apply Nat.add_neg_cases in tmp.
  destruct tmp as [it | abs].
    easy.
  case (Nat.nlt_0_r 1); exact abs.
apply (Nat.lt_trans _ _ _ c1).
rewrite (Nat.mul_lt_mono_pos_l 5); cycle 1.
  apply Nat.lt_0_succ.
rewrite Nat.mul_0_r.
apply (Plus.plus_lt_reg_l_stt _ _ y).
rewrite Nat.add_0_r.
replace y with (1 * y) at 1; cycle 1.
  rewrite Nat.mul_1_l.
  easy.
rewrite <- Nat.mul_add_distr_r.
change (1 + 5) with (2 * 3).
apply Nat.lt_trans with (2 * x + 1); cycle 1.
  exact c1.
apply Nat.lt_trans with (2 * (3 * y + 1) + 1); cycle 1.
  apply Nat.add_lt_mono_r.
  rewrite <- Nat.mul_lt_mono_pos_l.
    assumption.
  apply Nat.lt_0_succ.
rewrite Nat.mul_add_distr_l.
rewrite <- Nat.add_assoc.
rewrite <- Nat.mul_assoc.
apply Nat.lt_add_pos_r.
apply Nat.lt_0_succ.
Qed.

Lemma example' x y :
  2 * x + 1 < y -> 3 * y + 1 < x -> x < 0.
Proof.
lia.
Qed.
