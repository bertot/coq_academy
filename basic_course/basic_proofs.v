Require Import ZArith Lia.

Open Scope Z_scope.

Lemma rule1 : 0 < 1.
Proof.  lia.  Qed.

Lemma rule2 : forall x y : Z, 0 < x -> 0 < y -> 0 < x + y.
Proof. lia.  Qed.

Check   (fun x h => rule2 x (x + x) h (rule2 x x h h))  : forall x : Z, 0 < x -> 0 < x + (x + x).
Check   (fun x h => rule2 x (x + 1) h (rule2 x 1 h rule1)) 
   : forall x : Z, 0 < x -> 0 < x + (x + 1).
