\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{multirow}
\usepackage{alltt}
\newcommand{\coqand}{{\tt/\char'134}}
\newcommand{\coqor}{{\tt\char'134/}}
\newcommand{\coqnot}{\mbox{\~{}}}
\usepackage{color}
\setbeamertemplate{footline}[frame number]
\title{Specifications in Coq}
\author{Yves Bertot}
\date{June 2020}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Correctness of programs}
\begin{itemize}
\item Intrinsic guarantees of Coq programs
\begin{itemize}
\item Programs are guaranteed to terminate
\item Results are guaranteed to have the right type
\end{itemize}
\item Adding extra properties
\begin{itemize}
\item Document usage and restrictions
\item Add redundancy: provide several points of view
\item Have all claims verified thoroughly
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Multiple points of view}
% trim left bottom right top
%\framebox{
\begin{center}
\begin{tabular}{ccc}
\raisebox{5pt}{
% trim left bottom right top
%\framebox{
\includegraphics[width=0.2\textwidth, trim=0 0cm 0 0cm, clip]{../images/Mieusement_-_Cathedrale_de_Rouen_1886.jpg}
% }
}&
\includegraphics[width=0.2\textwidth, trim=0 0cm 0 0cm, clip]{../images/Claude_Monet_-_Cathedrale_de_Rouen_W1356.jpg}&
\includegraphics[width=0.2\textwidth, trim=0 0cm 0 0cm, clip]{../images/Claude_Monet_-_Cathedrale_de_Rouen_Harmonie_bleue.jpg}\\
\raisebox{1pt}{\includegraphics[width=0.2\textwidth, trim=0 0cm 0 0cm, clip]{../images/Claude_Monet_-_Cathedrale_de_Rouen_Morning_effect.jpg}}&
\includegraphics[width=0.2\textwidth, trim=0 0cm 0 0cm, clip]{../images/Claude_Monet_-_Cathedrale_de_Rouen_W1353.jpg}&
\raisebox{2pt}{\includegraphics[width=0.2\textwidth, trim=0 0cm 0 0cm, clip]{../images/Claude_Monet_-_Cathedrale_de_Rouen_Facade_Ouest.jpg}}\\
\multicolumn{3}{c}{
\begin{tiny}
Credits: S\'eraphin M\'ed\'eric Mieusement (1886), Claude Monet (1882-1885)
\end{tiny}
}
\end{tabular}
\end{center}
\end{frame}
\begin{frame}
\frametitle{What should a sorting function do?}
\begin{itemize}
\item The output should be sorted
\item All elements of the input should be present
\item No elements should be added
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Missing properties}
\begin{itemize}
\item What about the number of occurrences of each element?
\item What about the behavior when the list is already sorted?
\begin{itemize}
\item Think about sorting a table along the first colum
\item Suppose the order is not antisymmetric\\
\begin{center}
  \(r_1 \leq r_2 \wedge r_2 \leq r_1 \not \Rightarrow r_1 = r_2\)
\end{center}
\item One often wishes the elements to not move in this case
\item Sobering thought: absolute correctness is elusive
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Simple approach to specification}
\begin{itemize}
\item A Property can often be expressed by a program (a test)
\item Add universal quantification to say ``the test always succeeds''
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example for sorting}
\begin{alltt}
Fixpoint sorted \{A : Type\} (cmp : A -> A -> bool)
  (l : list A) : bool :=
match l with
  nil => true
| a :: tl =>
    match tl with
    | nil => true
    | b :: _ => cmp a b && sorted cmp tl
    end
end.

Definition sorting_function_first_prop \{A : Type\}
  (cmp : A -> A -> bool) (f : list A -> list A) :=
\textcolor{blue}{  forall l : list A, sorted cmp (f l) = true.}
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Limits to the simple approach}
\begin{itemize}
\item Not all interesting properties can be represented by the
return value of a program
\item This only works for {\em decidable} properties
\item Coq has two types for this distinction
\begin{itemize}
\item {\tt bool} : boolean values for decidable properties
\item {\tt Prop} : propositional values for non-decidable properties
\end{itemize}
\item Bridge between {\tt bool} and {\tt Prop} : {\tt b = true}
\item This bridge {\em Injects} {\tt bool} into {\tt Prop}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Logical connectives for Prop}
\begin{itemize}
\item {\tt True} (with a capital {\tt T})
\item {\tt False} (with a capital {\tt F})
\item Conjunction : \coqand{}
\item disjunction : \coqor{}
\item implication : {\tt ->} when the right-hand side is in {\tt Prop}
\item Negation : \coqnot{}
\item Universal quantification (if the body is in {\tt Prop})
\item Existential quantification
\item Equality {\tt x = y} is a proposition
\item Negation of equality is written {\tt x <> y}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{More about equality}
\begin{itemize}
\item Equality between numbers is decidable, usable in programs
\item Equality between functions of type {\tt Z -> Z} is not decidable
\item Coq provides a function {\tt x =?~y} for comparing two numbers
\item {\tt x = y} is more general, applicable when the type does
not support decidable equality
\item The {\sc Mathematical Components} library has a systematic treatment
of types with decidable equality
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Logical connectives for {\tt bool}}
\begin{itemize}
\item atomic values : {\tt true}, {\tt false} (with lower case {\tt t} and {\tt f})
\item Conjunction : {\tt \&\&}
\item disjunction : {\tt ||}
\item Negation : {\tt negb}
\item No universal or existential quantification
\begin{itemize}
\item But bounded quantification over the elements of a list, e.g.\\
{\tt forallb existsb~:~\\forall A~:~Type, (A -> bool) -> list A -> bool}
\end{itemize}
\item Equality {\tt x~=?~y} only for certain types
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example : primality}
A prime number is larger than 2 and has no divisor different from
1 and itself.
\begin{alltt}
Definition primality n : Prop :=
  (2 <= n) \coqand{}
  forall k l,  (k <> 1 \coqand{} k <> n) -> n <> k * l.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Curiosity : primality as a boolean predicate}
We already have to include some mathematical knowledge:
\begin{itemize}
\item Use of remainder computation to get rid of universal quantification on
 \(l\)
\item Use a bounded list of potential divisors instead of all natural numbers
\end{itemize}
\begin{alltt}
Definition bool_primality n : bool  :=
  (2 <=? n) &&
  forallb (fun k => negb (n mod k =? 0)) (seq 2 (n - 2)).
\end{alltt}
\begin{itemize}
\item Note difference between {\tt 2 <=? n} and {\tt 2 <= n}
\item Historical feature of Coq
\item {\sc Mathematical Components} advocates using a the boolean
concept whenever possible
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Equivalences: nested implication : and}
\begin{alltt}
 (A -> (B -> C)) <-> ((A \coqand{} B) -> C).
\end{alltt}
\begin{itemize}
\item parentheses convention:\\
 {\tt (A -> B -> C) <-> (A \coqand{} B -> C)}
\item For theorems with several hypotheses, the tradition is to
use nested implications
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Equivalences: existential and universal quantification}
\begin{alltt}
 (forall x, P x -> C) <-> ((exists x, P x) -> C)
\end{alltt}
\begin{itemize}
\item As long as {\tt x} does not occur in {\tt C}
\item Similar to the previous equivalence
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Negation and other connectives}
\begin{alltt}
  ~(A \coqor{} B) <-> ~A \coqand{} ~B

   ~(exists x, P x) <-> (forall x, ~ P x)
\end{alltt}
\begin{itemize}
\item Parallel between existential quantification and disjunction
\item negated choice is constructively reachable
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Constructive limitations}
In a constructive setting
\begin{itemize}
\item This implication holds, but not the other way around
\begin{alltt}
   (~ A \coqor{} ~ B) -> ~(A \coqand{} B)
\end{alltt}
\item Similarly with quantifiers
\begin{alltt}
   (exists x, ~ P x) -> ~(forall x, P x)
\end{alltt}
\item Not false, but not provable
\begin{alltt}
   ~(A \coqand{} B) -> (~ A \coqor{} ~ B)
   ~(forall x, P x) -> exists x, ~ P x
\end{alltt}
\end{itemize}
Users have the choice to work in a non-constructive setting
\begin{itemize}
\item These become provable if one adds the following axiom:
\begin{alltt}
    classic : forall P : Prop, P \coqor{} ~ P
\end{alltt}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Inductive predicate}
\begin{itemize}
\item The smallest set stable by a set of rules
\item Suitable to describe recursive thinking
\item No terminating algorithm required
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Inductive predicate example: well-parenthesized formula}
\begin{itemize}
\item A character string with no parenthesis is well-parenthesized
\item If a character string \(s\) is well parenthesized, then \((s)\) is
well-parenthesized
\item If two character strings are well-parenthesized, then their
concatenation is well-parenthesized
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Example with no known algorithm: \(3n+1\) conjecture}
\begin{itemize}
\item 1 is Collatz-reachable,
\item If \(n\) is odd and \(3n+1\) is Collatz-reachable, then \(n\) is 
Collatz-reachable
\item If \(n\) is reachable, then \(2n\) is Collatz-reachable
\end{itemize}
It is not known whether all numbers larger than 1 are Collatz-reachable\\
This is also known as the Syracuse conjecture
\end{frame}
\begin{frame}[fragile]
\frametitle{Inductive predicates in Coq}
\begin{alltt}
Inductive parenthesized : string -> Prop :=
  noparen : forall s,
        parenthesis_free s -> parenthesized s
| paren_wrap : forall s,
    parenthesized s ->
    parenthesized ("(" ++ s ++ ")")
| paren_concat : forall s1 s2,
    parenthesized s1 -> parenthesized s2 ->
    parenthesized (s1 ++ s2).
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Inductive connectives}
\begin{itemize}
\item conjunction, existential quantification, disjunction, True, and False
\end{itemize}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
