/* this file is meant to be compiled by the command. */
/* cc -o hello example.c (if the file is named example.c) */

#include "stdio.h"
#include "malloc.h"

/* The following type declarations describe a type of trees,
   where each node is either a pair of two subtrees or an integer */
struct tree_struct;

struct two_subtrees {
  struct tree_struct *first_child;
  struct tree_struct *second_child;
} two_subtrees;

struct tree_struct {
int key;
  union { struct two_subtrees ts; int val;};
} tree_struct;

#define key_node 0
#define key_leaf 1

typedef struct tree_struct * tree;
  
/* The following procedure traverses a tree and prints its content
   using parentheses to describe the tree structure. */
void print_tree (tree t) {
  if (key_node == t->key) {
    printf("(");
    print_tree(t->ts.first_child);
    printf(" ");
    print_tree(t->ts.second_child);
    printf(")");
  } else {
    printf("%d", t->val);
  }
}

/* In this example we only construct a three-node tree and
   display it. */
void main () {

  tree t, t1, t2, t3, t4;

  t = (tree)malloc(sizeof(struct tree_struct));
  t->key = key_leaf;
  t->val = 42;

  t1 = (tree)malloc(sizeof(struct tree_struct));
  t1->key = key_leaf;
  t1->val = 57;

  t2 = (tree)malloc(sizeof(struct tree_struct));
  t2->key = key_leaf;
  t2->val = 25;

  t3 = (tree)malloc(sizeof(struct tree_struct));
  t3->key = key_node;
  t3->ts.first_child = t1;
  t3->ts.second_child = t2;

  t4 = (tree)malloc(sizeof(struct tree_struct));
  t4->key = key_node;
  t4->ts.first_child = t;
  t4->ts.second_child = t3;

  /* Uncommenting the following line would lead into a bug. */
  /* t2->val = 127; */

  printf("hello world!\nthe tree is ");
  print_tree(t4);
  printf("\n");
}
