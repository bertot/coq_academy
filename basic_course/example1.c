/* this file is meant to be compiled by the command. */
/* cc -o hello example.c (if the file is named example.c) */

#include "stdio.h"
#include "malloc.h"

/* The following type declarations describe a type of trees,
   where each node is either a pair of two subtrees or an integer */
struct tree_struct;

struct two_subtrees {
  struct tree_struct *first_child;
  struct tree_struct *second_child;
} two_subtrees;

struct tree_struct {
int key;
  union { struct two_subtrees ts; int val;};
} tree_struct;

#define key_node 0
#define key_leaf 1

typedef struct tree_struct * tree;

/* for safer programming, we can use constructor functions. */
tree node(tree t1, tree t2) {
  tree t;
  t = (tree)malloc(sizeof(struct tree_struct));
  t->key = key_node;
  t->ts.first_child = t1;
  t->ts.second_child = t2;
}

tree leaf(int value) {
  tree t;
  t = (tree)malloc(sizeof(struct tree_struct));
  t->key = key_leaf;
  t->val = value;
}

/* The following procedure traverses a tree and adds the inter values
   in the leaves. */
int sum_tree (tree t) {
  if (key_node == t->key) {
    tree t1 = t->ts.first_child;
    tree t2 = t->ts.second_child;
    return (sum_tree(t1) + sum_tree(t2));
  } else {
    int value = t->val;
    return(value);
  }
}

/* In this example we only construct a three-node tree and
   display its sum. */
void main () {

  tree t;

  t = node(leaf(42), node(leaf(57), leaf(25)));

  printf("hello world!\nthe sum of values in the tree is ");
  printf ("%d\n", sum_tree(t));
}
