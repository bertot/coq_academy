Require Import Arith.

Fixpoint sum (n : nat) :=
  match n with S p => n + sum p | 0 => 0 end.

Compute (sum 10, sum 100).

Lemma sum_identity n : 2 * sum n = n * (n + 1).
Proof.
induction n as [ | p Ih].
  easy.
replace (2 * sum (S p)) with (2 * (S p) + 2 * sum p) by (simpl; ring).
rewrite Ih; ring.
Qed.
